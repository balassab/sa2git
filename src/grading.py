#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2013 by Balint Balassa                                  *
#*                                                                         *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************


import numpy as np
from scipy.interpolate import interp1d



def optimize_nCells(nCells, startCellLength=0, endCellLength=0, 
                           midCellLength=0, midCellPosition=0.5):
    if nCells == 1:
        return [0,1]
    else:
        assert startCellLength + midCellLength + endCellLength < 1.0
        x = range(nCells)
        xInt = [0,x[-1]]
        yInt = [startCellLength, endCellLength]
        if midCellLength > 0 :
            xInt.insert(1, int(round(nCells*midCellPosition)))
            yInt.insert(1, midCellLength)
        func = interp1d(xInt, yInt) 
        while sum(func(range(nCells))) > 1.0:
            nCells -= 1


def test_grading(points, reverse=False):
    tmp = np.logspace(1, 2, points, base=10)
    tmp -= tmp.min()
    tmp /= tmp.max()
    if reverse:
        tmp = 1 - tmp
        tmp = tmp[::-1]
    return list(tmp)


def hyperbolic_tangent(x, alpha):
    # from handbook of grid generation, free pdf on the interwebz
    y = 1.0 - (np.tanh(alpha*(1.0-x))) / (np.tanh(alpha))
    return y


def ht_grading(nCells, alpha, reverse=False):
    xArray = np.linspace(0, 1, nCells+1)
    yArray = hyperbolic_tangent(xArray, alpha)
    if reverse:
        yArray = 1.0 - yArray
        yArray = yArray[::-1]
    return yArray




