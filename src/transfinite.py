#!/usr/bin/env python
# -*- coding: utf-8 -*-



#***************************************************************************
# *   Copyright (C) 2013 by Balint Balassa                                  *
# *                                                                         *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 3 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import numpy as np
from pythonnurbs import *


def resample(nurbsSurface, interpPoints, sampleRate=100, dim=3, switchUV=False):
    '''Resampling nurbssurface within the given interpolation points'''
    interpCurves = []
    for edge in interpPoints:
        if len(edge) < 2:
            raise Exception('Each set of interpolation point must have at \
                            least 2 sets of coordinates!')
        # doing the nurbs interpolation of the u-v-values for the quadrilateral:
        else:
            edgeU = np.linspace(edge[0][0], edge[-1][0], sampleRate)
            edgeV = np.linspace(edge[0][1], edge[-1][1], sampleRate)

        if dim == 3:
            array = np.zeros((len(edgeU), 3))
            for i, (u, v) in enumerate(zip(edgeU, edgeV)):
                if switchUV:
                    nurbsPoint = nurbsSurface.pointAt(v, u)
                else:
                    nurbsPoint = nurbsSurface.pointAt(u, v)
                x = nurbsPoint.getx()
                y = nurbsPoint.gety()
                z = nurbsPoint.getz()
                array[i, :] = x, y, z
            interpCurves.append(interpolateNurbs3D(array, dim=3))
        elif dim == 4:
            array = np.zeros((len(edgeU), 4))
            for i, (u, v) in enumerate(zip(edgeU, edgeV)):
                if switchUV:
                    nurbsPoint = nurbsSurface.pointAt(v, u)
                else:
                    nurbsPoint = nurbsSurface.pointAt(u, v)
                x = nurbsPoint.getx()
                y = nurbsPoint.gety()
                z = nurbsPoint.getz()
                w = nurbsPoint.getw()
                array[i, :] = x, y, z, w
            interpCurves.append(interpolateNurbs3D(array, dim=4))
    return interpCurves


def get_uv_boundary_curves_from_uv_points(uvPoints, nurbsSurface, 
                                          bridgeU=False, bridgeV=False,
                                          sampleRate=9, switchUV=False):
    edge = uvPoints
    if len(edge) == 2:
        npEdge = np.array(uvPoints)
        npNewEdge = np.zeros((2, sampleRate))
        npEdge = np.swapaxes(npEdge, 0, 1)
        if not bridgeU and not bridgeV:
            npNewEdge[0] = np.linspace(npEdge[0, 0], npEdge[0, -1], sampleRate)
            npNewEdge[1] = np.linspace(npEdge[1, 0], npEdge[1, -1], sampleRate)
        if bridgeU:
            if npEdge[0, 0] > npEdge[0, -1]:
                npNewEdge[0] = np.linspace(npEdge[0, 0], npEdge[0, -1]+1, 
                                           sampleRate)
                npNewEdge[0] = np.where(npNewEdge[0]>1, npNewEdge[0]-1, 
                                        npNewEdge[0])
            else:
                npNewEdge[0] = np.linspace(npEdge[0, 0], npEdge[0, -1], 
                                           sampleRate)
            npNewEdge[1] = np.linspace(npEdge[1, 0], npEdge[1, -1], sampleRate)
            #npNewEdge = np.swapaxes(npNewEdge, 0, 1)
        if bridgeV:
            if npEdge[1, 0] > npEdge[1, -1]:
                npNewEdge[1] = np.linspace(npEdge[1, 0], npEdge[1, -1]+1, 
                                           sampleRate)
                npNewEdge[1] = np.where(npNewEdge[1]>1, npNewEdge[1]-1, 
                                        npNewEdge[1])
            else:
                npNewEdge[1] = np.linspace(npEdge[1, 0], npEdge[1, -1], 
                                           sampleRate)
            npNewEdge[0] = np.linspace(npEdge[0, 0], npEdge[0, -1], sampleRate)
            #npNewEdge = np.swapaxes(npNewEdge, 0, 1)
        
        npNewEdge = np.swapaxes(npNewEdge, 0, 1)
        edge = npNewEdge.tolist()

        uArray = np.zeros((len(edge), 4))
        vArray = np.zeros((len(edge), 4))
        for i, uvPair in enumerate(edge):
            if switchUV:
                nurbsPoint = nurbsSurface.pointAt(uvPair[1], uvPair[0])
            else:
                nurbsPoint = nurbsSurface.pointAt(uvPair[0], uvPair[1])
            uw = 1.0 + uvPair[0]
            vw = 1.0 + uvPair[1]
            if bridgeU:
                if uvPair[0] > 0.5:
                    uw = 0.5 + uvPair[0]
                else:
                    uw = 1.5 + uvPair[0]
            if bridgeV:
                if uvPair[1] > 0.5:
                    uw = 0.5 + uvPair[1]
                else:
                    uw = 1.5 + uvPair[1]
            
            x, y, z = nurbsPoint.getx(), nurbsPoint.gety(), nurbsPoint.getz()
            uArray[i, :] = x, y, z, uw
            vArray[i, :] = x, y, z, vw
        uBoundaryCurve = interpolateNurbs3D(uArray, dim=4)
        vBoundaryCurve = interpolateNurbs3D(vArray, dim=4)
        return uBoundaryCurve, vBoundaryCurve

    elif len(edge) < 2:
        raise Exception('Each set of interpolation point must have at \
                        least 2 sets of coordinates!')


def quadrilateral_nurbs_surface(u, v, nurbsSurface, dim=3, switchUV=False,
                                interpPointsU0=None, interpPointsU1=None,
                                interpPointsV0=None, interpPointsV1=None,
                                uInterpCurves=None, vInterpCurves=None,
                                bridgeUArg=False, bridgeVArg=False):
    '''Interpolate on quadrilateral nurbs surface with given boundaries'''
    if not 0 <= u <= 1:
        raise ValueError('u must be inside interval [0,1]')
    if not 0 <= v <= 1:
        raise ValueError('v must be inside interval [0,1]')

    if uInterpCurves == None or vInterpCurves == None:
        uInterpCurves = []
        vInterpCurves=  []
        for edge in [interpPointsU0, interpPointsU1, interpPointsV0,
                     interpPointsV1]:
            uCurve, vCurve = get_uv_boundary_curves_from_uv_points(edge,
                                            nurbsSurface, switchUV=switchUV,
                                            bridgeU=bridgeUArg,
                                            bridgeV=bridgeVArg)
            uInterpCurves.append(uCurve)
            vInterpCurves.append(vCurve)
    # TODO: return the itnerpolation curves, so that they will not be 
    # computed more than once
    interpU = quadrilateral(u, v, uInterpCurves[0], uInterpCurves[1],
                        uInterpCurves[2], uInterpCurves[3], dim=4)[3] - 1.0
    interpV = quadrilateral(u, v, vInterpCurves[0], vInterpCurves[1],
                        vInterpCurves[2], vInterpCurves[3], dim=4)[3] - 1.0
    if bridgeUArg:
        if u > 0.5:
            subtract = 1.5
        else:
            subtract = 0.5
        interpU = quadrilateral(u, v, uInterpCurves[0], uInterpCurves[1],
                            uInterpCurves[2], uInterpCurves[3], dim=4)[3] - subtract
    if bridgeVArg:
        if v > 0.5:
            subtract = 1.5
        else:
            subtract = 0.5
        interpV = quadrilateral(u, v, vInterpCurves[0], vInterpCurves[1],
                            vInterpCurves[2], vInterpCurves[3], dim=4)[3] - subtract
    if interpU < 0:
        #print interpU, '\traising interpU!'
        interpU = 0.0
    if interpV < 0:
        #print interpV, '\traising interpV!'
        interpV = 0.0
    if interpU > 1:
        #print interpU, '\treducing interpU!'
        interpU = 1.0
    if interpV > 1:
        #print interpV, '\treducing interpV!'
        interpV = 1.0
    if dim == 3:
        # Mind the switch of u-v for bladedesigner blade:
        if switchUV:
            nurbsPoint = nurbsSurface.pointAt(interpV, interpU)
        else:
            nurbsPoint = nurbsSurface.pointAt(interpU, interpV)
        x = nurbsPoint.getx()
        y = nurbsPoint.gety()
        z = nurbsPoint.getz()
        return np.array((x, y, z))
    elif dim == 4:
        # Mind the switch of u-v for bladedesigner blade:
        if switchUV:
            nurbsHPoint = nurbsSurface.hpointAt(interpV, interpU)
        else:
            nurbsHPoint = nurbsSurface.hpointAt(interpU, interpV)
        x = nurbsHPoint.getx()
        y = nurbsHPoint.gety()
        z = nurbsHPoint.getz()
        w = nurbsHPoint.getw()
        return np.array((x, y, z, w))




def quadrilateral(u, v, curveU0, curveU1, curveV0, curveV1, dim=3,
                  mappingDictArg={}):
    '''Interpolation on quadrilateral'''
    mappingDict = {'U0':lambda x:x,
                   'U1':lambda x:x,
                   'V0':lambda x:x,
                   'V1':lambda x:x, }
    mappingDict.update(mappingDictArg)
    if not 0 <= u <= 1:
        raise ValueError('u must be inside interval [0,1]')
    if not 0 <= v <= 1:
        raise ValueError('v must be inside interval [0,1]')

    # mapping the u and v values with the given function.
    # the standard lambda function returns u and v without mapping:
    U0Start = mappingDict['U0'](0)
    U0v = mappingDict['U0'](v)
    U0Stop = mappingDict['U0'](1)

    U1Start = mappingDict['U1'](0)
    U1v = mappingDict['U1'](v)
    U1Stop = mappingDict['U1'](1)

    V0Start = mappingDict['V0'](0)
    V0u = mappingDict['V0'](u)
    V0End = mappingDict['V0'](1)

    V1Start = mappingDict['V1'](0)
    V1u = mappingDict['V1'](u)
    V1End = mappingDict['V1'](1)

    lengthWise = False
    xTol = 1e-6

    if dim == 3:
        pointU0Start = getPoint(curveU0.pointAt(U0Start))
        if lengthWise:
            U0v = brentq(root_lengthwise, U0Start, U0Stop,
                       args=(U0Start, U0Stop, v, curveU0), xtol=xTol)
        pointU0v = getPoint(curveU0.pointAt(U0v))
        pointU0Stop = getPoint(curveU0.pointAt(U0Stop))

        pointU1Start = getPoint(curveU1.pointAt(U1Start))
        if lengthWise:
            U1v = brentq(root_lengthwise, U1Start, U1Stop,
                       args=(U1Start, U1Stop, v, curveU1), xtol=xTol)
        pointU1v = getPoint(curveU1.pointAt(U1v))
        pointU1Stop = getPoint(curveU1.pointAt(U1Stop))

        if lengthWise:
            V0u = brentq(root_lengthwise, V0Start, V0End,
                       args=(V0Start, V0End, u, curveV0), xtol=xTol)
        pointV0u = getPoint(curveV0.pointAt(V0u))

        if lengthWise:
            V1u = brentq(root_lengthwise, V1Start, V1End,
                       args=(V1Start, V1End, u, curveV1), xtol=xTol)
        pointV1u = getPoint(curveV1.pointAt(V1u))

        xyz = ((1.0 - u) * pointU0v + u * pointU1v
                + (1.0 - v) * pointV0u + v * pointV1u
                - ((1.0 - u) * (1.0 - v) * pointU0Start
                  + u * (1.0 - v) * pointU1Start
                  + v * (1.0 - u) * pointU0Stop
                  + u * v * pointU1Stop))
        return xyz

    elif dim == 4:
        pointU0Start = getHPoint(curveU0.hpointAt(U0Start))
        if lengthWise:
            U0v = brentq(root_lengthwise, U0Start, U0Stop,
                       args=(U0Start, U0Stop, v, curveU0), xtol=xTol)
        pointU0v = getHPoint(curveU0.hpointAt(U0v))
        pointU0Stop = getHPoint(curveU0.hpointAt(U0Stop))

        pointU1Start = getHPoint(curveU1.hpointAt(U1Start))
        if lengthWise:
            U1v = brentq(root_lengthwise, U1Start, U1Stop,
                       args=(U1Start, U1Stop, v, curveU1), xtol=xTol)
        pointU1v = getHPoint(curveU1.hpointAt(U1v))
        pointU1Stop = getHPoint(curveU1.hpointAt(U1Stop))

        if lengthWise:
            V0u = brentq(root_lengthwise, V0Start, V0End,
                       args=(V0Start, V0End, u, curveV0), xtol=xTol)
        pointV0u = getHPoint(curveV0.hpointAt(V0u))

        if lengthWise:
            V1u = brentq(root_lengthwise, V1Start, V1End,
                       args=(V1Start, V1End, u, curveV1), xtol=xTol)
        pointV1u = getHPoint(curveV1.hpointAt(V1u))

        xyzw = ((1.0 - u) * pointU0v + u * pointU1v
                + (1.0 - v) * pointV0u + v * pointV1u
                - ((1.0 - u) * (1.0 - v) * pointU0Start
                  + u * (1.0 - v) * pointU1Start
                  + v * (1.0 - u) * pointU0Stop
                  + u * v * pointU1Stop))
        return xyzw




def hexahedron_nurbs_surfaces(u, v, w, curvesArg={}, surfacesArg={},
                              master=None, quadBoundaries={}, switchUVArg={},
                              bridgeUDict={}, bridgeVDict={},
                              mappingDictArg={}, dim=3, paraTol=1e-12):
    '''Interpolation on hexahedron with given surface(s) and curves'''

    if not 0 <= u <= 1:
        raise ValueError('u must be inside interval [0,1]')
    if not 0 <= v <= 1:
        raise ValueError('v must be inside interval [0,1]')
    if not 0 <= w <= 1:
        raise ValueError('w must be inside interval [0,1]')

    curves = {'U0V0':None,
                'U0V1':None,
                'U0W0':None,
                'U0W1':None,
                'U1V0':None,
                'U1V1':None,
                'U1W0':None,
                'U1W1':None,
                'V0W0':None,
                'V0W1':None,
                'V1W0':None,
                'V1W1':None}
    surfaces = {'U0':None,
                'U1':None,
                'V0':None,
                'V1':None,
                'W0':None,
                'W1':None}
    bridgeU = {'U0':False,
                'U1':False,
                'V0':False,
                'V1':False,
                'W0':False,
                'W1':False}
    bridgeV = {'U0':False,
                'U1':False,
                'V0':False,
                'V1':False,
                'W0':False,
                'W1':False}
    mappingDict = {'U0V0':lambda x:x,
                    'U0V1':lambda x:x,
                    'U0W0':lambda x:x,
                    'U0W1':lambda x:x,
                    'U1V0':lambda x:x,
                    'U1V1':lambda x:x,
                    'U1W0':lambda x:x,
                    'U1W1':lambda x:x,
                    'V0W0':lambda x:x,
                    'V0W1':lambda x:x,
                    'V1W0':lambda x:x,
                    'V1W1':lambda x:x}
    switchUV = {'U0':False,
                'U1':False,
                'V0':False,
                'V1':False,
                'W0':False,
                'W1':False}

    curves.update(curvesArg)
    surfaces.update(surfacesArg)
    mappingDict.update(mappingDictArg)
    switchUV.update(switchUVArg)
    bridgeU.update(bridgeUDict)
    bridgeV.update(bridgeVDict)

    # mapping the u and v values with the given function.
    # the standard lambda function returns u and v without mapping:
    U0V0Start = mappingDict['U0V0'](0)
    U0V0w = mappingDict['U0V0'](w)
    U0V0Stop = mappingDict['U0V0'](1)

    U0V1Start = mappingDict['U0V1'](0)
    U0V1w = mappingDict['U0V1'](w)
    U0V1Stop = mappingDict['U0V1'](1)

    # U0W0Start = mappingDict['U0W0'](0)
    U0W0v = mappingDict['U0W0'](v)
    # U0W0Stop = mappingDict['U0W0'](1)

    # U0W1Start = mappingDict['U0W1'](0)
    U0W1v = mappingDict['U0W1'](v)
    # U0W1Stop = mappingDict['U0W1'](1)

    U1V0Start = mappingDict['U1V0'](0)
    U1V0w = mappingDict['U1V0'](w)
    U1V0Stop = mappingDict['U1V0'](1)

    U1V1Start = mappingDict['U1V1'](0)
    U1V1w = mappingDict['U1V1'](w)
    U1V1Stop = mappingDict['U1V1'](1)

    # U1W0Start = mappingDict['U1W0'](0)
    U1W0v = mappingDict['U1W0'](v)
    # U1W0Stop = mappingDict['U1W0'](1)

    # U1W1Start = mappingDict['U1W1'](0)
    U1W1v = mappingDict['U1W1'](v)
    # U1W1Stop = mappingDict['U1W1'](1)

    # V0W0Start = mappingDict['V0W0'](0)
    V0W0u = mappingDict['V0W0'](u)
    # V0W0Stop = mappingDict['V0W0'](1)

    # V0W1Start = mappingDict['V0W1'](0)
    V0W1u = mappingDict['V0W1'](u)
    # V0W1Stop = mappingDict['V0W1'](1)

    # V1W0Start = mappingDict['V1W0'](0)
    V1W0u = mappingDict['V1W0'](u)
    # V1W0Stop = mappingDict['V1W0'](1)

    # V1W1Start = mappingDict['V1W1'](0)
    V1W1u = mappingDict['V1W1'](u)
    # V1W1Stop = mappingDict['V1W1'](1)

    pointU0V0W0 = None
    pointU0V0w = None
    pointU0V0W1 = None

    pointU0V1W0 = None
    pointU0V1w = None
    pointU0V1W1 = None

    pointU1V0W0 = None
    pointU1V0w = None
    pointU1V0W1 = None

    pointU1V1W0 = None
    pointU1V1w = None
    pointU1V1W1 = None

    pointV0W0u = None

    pointV0W1u = None

    pointV1W0u = None

    pointV1W1u =None 

    pointU0W0v = None

    pointU1W0v = None

    pointU0W1v = None

    pointU1W1v = None

    if surfaces['U0'] != None:
        faceU0 = quadrilateral_nurbs_surface(v, w, surfaces['U0'], dim=dim,
                                              switchUV=switchUV['U0'],
                                interpPointsU0=quadBoundaries['U0'][0],
                                interpPointsU1=quadBoundaries['U0'][1],
                                interpPointsV0=quadBoundaries['U0'][2],
                                interpPointsV1=quadBoundaries['U0'][3],
                                bridgeUArg=bridgeU['U0'],
                                bridgeVArg=bridgeV['U0'])
        if abs(u - 0) < paraTol:
            return faceU0
        else:
            if master == 'U0':
                pointU0V0w = quadrilateral_nurbs_surface(0, w, surfaces['U0'],
                                            dim=dim, switchUV=switchUV['U0'],
                                interpPointsU0=quadBoundaries['U0'][0],
                                interpPointsU1=quadBoundaries['U0'][1],
                                interpPointsV0=quadBoundaries['U0'][2],
                                interpPointsV1=quadBoundaries['U0'][3],
                                bridgeUArg=bridgeU['U0'],
                                bridgeVArg=bridgeV['U0'])
                pointU0V1w = quadrilateral_nurbs_surface(1, w, surfaces['U0'],
                                            dim=dim, switchUV=switchUV['U0'],
                                interpPointsU0=quadBoundaries['U0'][0],
                                interpPointsU1=quadBoundaries['U0'][1],
                                interpPointsV0=quadBoundaries['U0'][2],
                                interpPointsV1=quadBoundaries['U0'][3],
                                bridgeUArg=bridgeU['U0'],
                                bridgeVArg=bridgeV['U0'])
                pointU0W0v = quadrilateral_nurbs_surface(v, 0, surfaces['U0'],
                                            dim=dim, switchUV=switchUV['U0'],
                                interpPointsU0=quadBoundaries['U0'][0],
                                interpPointsU1=quadBoundaries['U0'][1],
                                interpPointsV0=quadBoundaries['U0'][2],
                                interpPointsV1=quadBoundaries['U0'][3],
                                bridgeUArg=bridgeU['U0'],
                                bridgeVArg=bridgeV['U0'])
                pointU0W1v = quadrilateral_nurbs_surface(v, 1, surfaces['U0'],
                                            dim=dim, switchUV=switchUV['U0'],
                                interpPointsU0=quadBoundaries['U0'][0],
                                interpPointsU1=quadBoundaries['U0'][1],
                                interpPointsV0=quadBoundaries['U0'][2],
                                interpPointsV1=quadBoundaries['U0'][3],
                                bridgeUArg=bridgeU['U0'],
                                bridgeVArg=bridgeV['U0'])

                
    else:
        faceU0 = quadrilateral(v, w, curves['U0V0'], curves['U0V1'],
                               curves['U0W0'], curves['U0W1'], dim=dim,
                               mappingDictArg={'U0':mappingDict['U0V0'],
                                               'U1':mappingDict['U0V1'],
                                               'V0':mappingDict['U0W0'],
                                               'V1':mappingDict['U0W1']})

    if surfaces['U1'] != None:
        faceU1 = quadrilateral_nurbs_surface(v, w, surfaces['U1'], dim=dim,
                                              switchUV=switchUV['U1'],
                                interpPointsU0=quadBoundaries['U1'][0],
                                interpPointsU1=quadBoundaries['U1'][1],
                                interpPointsV0=quadBoundaries['U1'][2],
                                interpPointsV1=quadBoundaries['U1'][3],
                                bridgeUArg=bridgeU['U1'],
                                bridgeVArg=bridgeV['U1'])
        if abs(u - 1) < paraTol:
            return faceU1
        else:
            if master == 'U1':
                pointU1V0w = quadrilateral_nurbs_surface(0, w, surfaces['U1'],
                                            dim=dim, switchUV=switchUV['U1'],
                                interpPointsU0=quadBoundaries['U1'][0],
                                interpPointsU1=quadBoundaries['U1'][1],
                                interpPointsV0=quadBoundaries['U1'][2],
                                interpPointsV1=quadBoundaries['U1'][3],
                                bridgeUArg=bridgeU['U1'],
                                bridgeVArg=bridgeV['U1'])
                pointU1V1w = quadrilateral_nurbs_surface(1, w, surfaces['U1'],
                                            dim=dim, switchUV=switchUV['U1'],
                                interpPointsU0=quadBoundaries['U1'][0],
                                interpPointsU1=quadBoundaries['U1'][1],
                                interpPointsV0=quadBoundaries['U1'][2],
                                interpPointsV1=quadBoundaries['U1'][3],
                                bridgeUArg=bridgeU['U1'],
                                bridgeVArg=bridgeV['U1'])
                pointU1W0v = quadrilateral_nurbs_surface(v, 0, surfaces['U1'],
                                            dim=dim, switchUV=switchUV['U1'],
                                interpPointsU0=quadBoundaries['U1'][0],
                                interpPointsU1=quadBoundaries['U1'][1],
                                interpPointsV0=quadBoundaries['U1'][2],
                                interpPointsV1=quadBoundaries['U1'][3],
                                bridgeUArg=bridgeU['U1'],
                                bridgeVArg=bridgeV['U1'])
                pointU1W1v = quadrilateral_nurbs_surface(v, 1, surfaces['U1'],
                                            dim=dim, switchUV=switchUV['U1'],
                                interpPointsU0=quadBoundaries['U1'][0],
                                interpPointsU1=quadBoundaries['U1'][1],
                                interpPointsV0=quadBoundaries['U1'][2],
                                interpPointsV1=quadBoundaries['U1'][3],
                                bridgeUArg=bridgeU['U1'],
                                bridgeVArg=bridgeV['U1'])

    else:
        faceU1 = quadrilateral(v, w, curves['U1V0'], curves['U1V1'],
                               curves['U1W0'], curves['U1W1'], dim=dim,
                               mappingDictArg={'U0':mappingDict['U1V0'],
                                               'U1':mappingDict['U1V1'],
                                               'V0':mappingDict['U1W0'],
                                               'V1':mappingDict['U1W1']})

    if surfaces['V0'] != None:
        faceV0 = quadrilateral_nurbs_surface(u, w, surfaces['V0'], dim=dim,
                                              switchUV=switchUV['V0'],
                                interpPointsU0=quadBoundaries['V0'][0],
                                interpPointsU1=quadBoundaries['V0'][1],
                                interpPointsV0=quadBoundaries['V0'][2],
                                interpPointsV1=quadBoundaries['V0'][3],
                                bridgeUArg=bridgeU['V0'],
                                bridgeVArg=bridgeV['V0'])
        if abs(v - 0) < paraTol:
            return faceV0
        else:
            if master == 'V0':
                pointU0V0w = quadrilateral_nurbs_surface(0, w, surfaces['V0'],
                                            dim=dim, switchUV=switchUV['V0'],
                                interpPointsU0=quadBoundaries['V0'][0],
                                interpPointsU1=quadBoundaries['V0'][1],
                                interpPointsV0=quadBoundaries['V0'][2],
                                interpPointsV1=quadBoundaries['V0'][3],
                                bridgeUArg=bridgeU['V0'],
                                bridgeVArg=bridgeV['V0'])
                pointU1V0w = quadrilateral_nurbs_surface(1, w, surfaces['V0'],
                                            dim=dim, switchUV=switchUV['V0'],
                                interpPointsU0=quadBoundaries['V0'][0],
                                interpPointsU1=quadBoundaries['V0'][1],
                                interpPointsV0=quadBoundaries['V0'][2],
                                interpPointsV1=quadBoundaries['V0'][3],
                                bridgeUArg=bridgeU['V0'],
                                bridgeVArg=bridgeV['V0'])
                pointV0W0u = quadrilateral_nurbs_surface(u, 0, surfaces['V0'],
                                            dim=dim, switchUV=switchUV['V0'],
                                interpPointsU0=quadBoundaries['V0'][0],
                                interpPointsU1=quadBoundaries['V0'][1],
                                interpPointsV0=quadBoundaries['V0'][2],
                                interpPointsV1=quadBoundaries['V0'][3],
                                bridgeUArg=bridgeU['V0'],
                                bridgeVArg=bridgeV['V0'])
                pointV0W1u = quadrilateral_nurbs_surface(u, 1, surfaces['V0'],
                                            dim=dim, switchUV=switchUV['V0'],
                                interpPointsU0=quadBoundaries['V0'][0],
                                interpPointsU1=quadBoundaries['V0'][1],
                                interpPointsV0=quadBoundaries['V0'][2],
                                interpPointsV1=quadBoundaries['V0'][3],
                                bridgeUArg=bridgeU['V0'],
                                bridgeVArg=bridgeV['V0'])

    else:
        faceV0 = quadrilateral(u, w, curves['U0V0'], curves['U1V0'],
                               curves['V0W0'], curves['V0W1'], dim=dim,
                               mappingDictArg={'U0':mappingDict['U0V0'],
                                               'U1':mappingDict['U1V0'],
                                               'V0':mappingDict['V0W0'],
                                               'V1':mappingDict['V0W1']})

    if surfaces['V1'] != None:
        faceV1 = quadrilateral_nurbs_surface(u, w, surfaces['V1'], dim=dim,
                                              switchUV=switchUV['V1'],
                                interpPointsU0=quadBoundaries['V1'][0],
                                interpPointsU1=quadBoundaries['V1'][1],
                                interpPointsV0=quadBoundaries['V1'][2],
                                interpPointsV1=quadBoundaries['V1'][3],
                                bridgeUArg=bridgeU['V1'],
                                bridgeVArg=bridgeV['V1'])
        if abs(v - 1) < paraTol:
            return faceV1
        else:
            if master == 'V1':
                pointU0V1w = quadrilateral_nurbs_surface(0, w, surfaces['V1'],
                                            dim=dim, switchUV=switchUV['V1'],
                                interpPointsU0=quadBoundaries['V1'][0],
                                interpPointsU1=quadBoundaries['V1'][1],
                                interpPointsV0=quadBoundaries['V1'][2],
                                interpPointsV1=quadBoundaries['V1'][3],
                                bridgeUArg=bridgeU['V1'],
                                bridgeVArg=bridgeV['V1'])
                pointU1V1w = quadrilateral_nurbs_surface(1, w, surfaces['V1'],
                                            dim=dim, switchUV=switchUV['V1'],
                                interpPointsU0=quadBoundaries['V1'][0],
                                interpPointsU1=quadBoundaries['V1'][1],
                                interpPointsV0=quadBoundaries['V1'][2],
                                interpPointsV1=quadBoundaries['V1'][3],
                                bridgeUArg=bridgeU['V1'],
                                bridgeVArg=bridgeV['V1'])
                pointV1W0u = quadrilateral_nurbs_surface(u, 0, surfaces['V1'],
                                            dim=dim, switchUV=switchUV['V1'],
                                interpPointsU0=quadBoundaries['V1'][0],
                                interpPointsU1=quadBoundaries['V1'][1],
                                interpPointsV0=quadBoundaries['V1'][2],
                                interpPointsV1=quadBoundaries['V1'][3],
                                bridgeUArg=bridgeU['V1'],
                                bridgeVArg=bridgeV['V1'])
                pointV1W1u = quadrilateral_nurbs_surface(u, 1, surfaces['V1'],
                                            dim=dim, switchUV=switchUV['V1'],
                                interpPointsU0=quadBoundaries['V1'][0],
                                interpPointsU1=quadBoundaries['V1'][1],
                                interpPointsV0=quadBoundaries['V1'][2],
                                interpPointsV1=quadBoundaries['V1'][3],
                                bridgeUArg=bridgeU['V1'],
                                bridgeVArg=bridgeV['V1'])

    else:
        faceV1 = quadrilateral(u, w, curves['U0V1'], curves['U1V1'],
                               curves['V1W0'], curves['V1W1'], dim=dim,
                               mappingDictArg={'U0':mappingDict['U0V1'],
                                               'U1':mappingDict['U1V1'],
                                               'V0':mappingDict['V1W0'],
                                               'V1':mappingDict['V1W1']})

    if surfaces['W0'] != None:
        faceW0 = quadrilateral_nurbs_surface(u, v, surfaces['W0'], dim=dim,
                                              switchUV=switchUV['W0'],
                                interpPointsU0=quadBoundaries['W0'][0],
                                interpPointsU1=quadBoundaries['W0'][1],
                                interpPointsV0=quadBoundaries['W0'][2],
                                interpPointsV1=quadBoundaries['W0'][3],
                                bridgeUArg=bridgeU['W0'],
                                bridgeVArg=bridgeV['W0'])
        if abs(w - 0) < paraTol:
            return faceW0
        else:
            if master == 'W0':
                pointU0W0v = quadrilateral_nurbs_surface(0, v, surfaces['W0'],
                                            dim=dim, switchUV=switchUV['W0'],
                                interpPointsU0=quadBoundaries['W0'][0],
                                interpPointsU1=quadBoundaries['W0'][1],
                                interpPointsV0=quadBoundaries['W0'][2],
                                interpPointsV1=quadBoundaries['W0'][3],
                                bridgeUArg=bridgeU['W0'],
                                bridgeVArg=bridgeV['W0'])
                pointU1W0v = quadrilateral_nurbs_surface(1, v, surfaces['W0'],
                                            dim=dim, switchUV=switchUV['W0'],
                                interpPointsU0=quadBoundaries['W0'][0],
                                interpPointsU1=quadBoundaries['W0'][1],
                                interpPointsV0=quadBoundaries['W0'][2],
                                interpPointsV1=quadBoundaries['W0'][3],
                                bridgeUArg=bridgeU['W0'],
                                bridgeVArg=bridgeV['W0'])
                pointV0W0u = quadrilateral_nurbs_surface(u, 0, surfaces['W0'],
                                            dim=dim, switchUV=switchUV['W0'],
                                interpPointsU0=quadBoundaries['W0'][0],
                                interpPointsU1=quadBoundaries['W0'][1],
                                interpPointsV0=quadBoundaries['W0'][2],
                                interpPointsV1=quadBoundaries['W0'][3],
                                bridgeUArg=bridgeU['W0'],
                                bridgeVArg=bridgeV['W0'])
                pointV1W0u = quadrilateral_nurbs_surface(u, 1, surfaces['W0'],
                                            dim=dim, switchUV=switchUV['W0'],
                                interpPointsU0=quadBoundaries['W0'][0],
                                interpPointsU1=quadBoundaries['W0'][1],
                                interpPointsV0=quadBoundaries['W0'][2],
                                interpPointsV1=quadBoundaries['W0'][3],
                                bridgeUArg=bridgeU['W0'],
                                bridgeVArg=bridgeV['W0'])

    else:
        faceW0 = quadrilateral(u, v, curves['U0W0'], curves['U1W0'],
                               curves['V0W0'], curves['V1W0'], dim=dim,
                               mappingDictArg={'U0':mappingDict['U0W0'],
                                               'U1':mappingDict['U1W0'],
                                               'V0':mappingDict['V0W0'],
                                               'V1':mappingDict['V1W0']})

    if surfaces['W1'] != None:
        faceW1 = quadrilateral_nurbs_surface(u, v, surfaces['W1'], dim=dim,
                                              switchUV=switchUV['W1'],
                                interpPointsU0=quadBoundaries['W1'][0],
                                interpPointsU1=quadBoundaries['W1'][1],
                                interpPointsV0=quadBoundaries['W1'][2],
                                interpPointsV1=quadBoundaries['W1'][3],
                                bridgeUArg=bridgeU['W1'],
                                bridgeVArg=bridgeV['W1'])
        if abs(w - 1) < paraTol:
            return faceW1
        else:
            if master == 'W1':
                pointU0W1v = quadrilateral_nurbs_surface(0, v, surfaces['W1'],
                                            dim=dim, switchUV=switchUV['W1'],
                                interpPointsU0=quadBoundaries['W1'][0],
                                interpPointsU1=quadBoundaries['W1'][1],
                                interpPointsV0=quadBoundaries['W1'][2],
                                interpPointsV1=quadBoundaries['W1'][3],
                                bridgeUArg=bridgeU['W1'],
                                bridgeVArg=bridgeV['W1'])
                pointU1W1v = quadrilateral_nurbs_surface(1, v, surfaces['W1'],
                                            dim=dim, switchUV=switchUV['W1'],
                                interpPointsU0=quadBoundaries['W1'][0],
                                interpPointsU1=quadBoundaries['W1'][1],
                                interpPointsV0=quadBoundaries['W1'][2],
                                interpPointsV1=quadBoundaries['W1'][3],
                                bridgeUArg=bridgeU['W1'],
                                bridgeVArg=bridgeV['W1'])
                pointV0W1u = quadrilateral_nurbs_surface(u, 0, surfaces['W1'],
                                            dim=dim, switchUV=switchUV['W1'],
                                interpPointsU0=quadBoundaries['W1'][0],
                                interpPointsU1=quadBoundaries['W1'][1],
                                interpPointsV0=quadBoundaries['W1'][2],
                                interpPointsV1=quadBoundaries['W1'][3],
                                bridgeUArg=bridgeU['W1'],
                                bridgeVArg=bridgeV['W1'])
                pointV1W1u = quadrilateral_nurbs_surface(u, 1, surfaces['W1'],
                                            dim=dim, switchUV=switchUV['W1'],
                                interpPointsU0=quadBoundaries['W1'][0],
                                interpPointsU1=quadBoundaries['W1'][1],
                                interpPointsV0=quadBoundaries['W1'][2],
                                interpPointsV1=quadBoundaries['W1'][3],
                                bridgeUArg=bridgeU['W1'],
                                bridgeVArg=bridgeV['W1'])

    else:
        faceW1 = quadrilateral(u, v, curves['U0W1'], curves['U1W1'],
                               curves['V0W1'], curves['V1W1'], dim=dim,
                               mappingDictArg={'U0':mappingDict['U0W1'],
                                               'U1':mappingDict['U1W1'],
                                               'V0':mappingDict['V0W1'],
                                               'V1':mappingDict['V1W1']})

    if dim == 3:
        if pointU0V0W0 == None:
            pointU0V0W0 = getPoint(curves['U0V0'].pointAt(U0V0Start))
        if pointU0V0w == None:
            pointU0V0w = getPoint(curves['U0V0'].pointAt(U0V0w))
        if pointU0V0W1 == None:
            pointU0V0W1 = getPoint(curves['U0V0'].pointAt(U0V0Stop))

        if pointU0V1W0 == None:
            pointU0V1W0 = getPoint(curves['U0V1'].pointAt(U0V1Start))
        if pointU0V1w == None:
            pointU0V1w = getPoint(curves['U0V1'].pointAt(U0V1w))
        if pointU0V1W1 == None:
            pointU0V1W1 = getPoint(curves['U0V1'].pointAt(U0V1Stop))

        if pointU1V0W0 == None:
            pointU1V0W0 = getPoint(curves['U1V0'].pointAt(U1V0Start))
        if pointU1V0w == None:
            pointU1V0w = getPoint(curves['U1V0'].pointAt(U1V0w))
        if pointU1V0W1 == None:
            pointU1V0W1 = getPoint(curves['U1V0'].pointAt(U1V0Stop))

        if pointU1V1W0 == None:
            pointU1V1W0 = getPoint(curves['U1V1'].pointAt(U1V1Start))
        if pointU1V1w == None:
            pointU1V1w = getPoint(curves['U1V1'].pointAt(U1V1w))
        if pointU1V1W1 == None:
            pointU1V1W1 = getPoint(curves['U1V1'].pointAt(U1V1Stop))

        if pointV0W0u == None:
            pointV0W0u = getPoint(curves['V0W0'].pointAt(V0W0u))

        if pointV0W1u == None:
            pointV0W1u = getPoint(curves['V0W1'].pointAt(V0W1u))

        if pointV1W0u == None:
            pointV1W0u = getPoint(curves['V1W0'].pointAt(V1W0u))

        if pointV1W1u == None:
            pointV1W1u = getPoint(curves['V1W1'].pointAt(V1W1u))

        if pointU0W0v == None:
            pointU0W0v = getPoint(curves['U0W0'].pointAt(U0W0v))

        if pointU1W0v == None:
            pointU1W0v = getPoint(curves['U1W0'].pointAt(U1W0v))

        if pointU0W1v == None:
            pointU0W1v = getPoint(curves['U0W1'].pointAt(U0W1v))

        if pointU1W1v == None:
            pointU1W1v = getPoint(curves['U1W1'].pointAt(U1W1v))

    elif dim == 4:
        if pointU0V0W0 == None:
            pointU0V0W0 = getHPoint(curves['U0V0'].hpointAt(U0V0Start))
        if pointU0V0w == None:
            pointU0V0w = getHPoint(curves['U0V0'].hpointAt(U0V0w))
        if pointU0V0W1 == None:
            pointU0V0W1 = getHPoint(curves['U0V0'].hpointAt(U0V0Stop))

        if pointU0V1W0 == None:
            pointU0V1W0 = getHPoint(curves['U0V1'].hpointAt(U0V1Start))
        if pointU0V1w == None:
            pointU0V1w = getHPoint(curves['U0V1'].hpointAt(U0V1w))
        if pointU0V1W1 == None:
            pointU0V1W1 = getHPoint(curves['U0V1'].hpointAt(U0V1Stop))

        if pointU1V0W0 == None:
            pointU1V0W0 = getHPoint(curves['U1V0'].hpointAt(U1V0Start))
        if pointU1V0w == None:
            pointU1V0w = getHPoint(curves['U1V0'].hpointAt(U1V0w))
        if pointU1V0W1 == None:
            pointU1V0W1 = getHPoint(curves['U1V0'].hpointAt(U1V0Stop))

        if pointU1V1W0 == None:
            pointU1V1W0 = getHPoint(curves['U1V1'].hpointAt(U1V1Start))
        if pointU1V1w == None:
            pointU1V1w = getHPoint(curves['U1V1'].hpointAt(U1V1w))
        if pointU1V1W1 == None:
            pointU1V1W1 = getHPoint(curves['U1V1'].hpointAt(U1V1Stop))

        if pointV0W0u == None:
            pointV0W0u = getHPoint(curves['V0W0'].hpointAt(V0W0u))

        if pointV0W1u == None:
            pointV0W1u = getHPoint(curves['V0W1'].hpointAt(V0W1u))

        if pointV1W0u == None:
            pointV1W0u = getHPoint(curves['V1W0'].hpointAt(V1W0u))

        if pointV1W1u == None:
            pointV1W1u = getHPoint(curves['V1W1'].hpointAt(V1W1u))

        if pointU0W0v == None:
            pointU0W0v = getHPoint(curves['U0W0'].hpointAt(U0W0v))

        if pointU1W0v == None:
            pointU1W0v = getHPoint(curves['U1W0'].hpointAt(U1W0v))

        if pointU0W1v == None:
            pointU0W1v = getHPoint(curves['U0W1'].hpointAt(U0W1v))

        if pointU1W1v == None:
            pointU1W1v = getHPoint(curves['U1W1'].hpointAt(U1W1v))

    result = ((1.0 - u) * faceU0 + u * faceU1
            + (1.0 - v) * faceV0 + v * faceV1
            + (1.0 - w) * faceW0 + w * faceW1
            - (1.0 - u) * ((1.0 - v) * pointU0V0w + v * pointU0V1w)
            - u * ((1.0 - v) * pointU1V0w + v * pointU1V1w)
            - (1.0 - v) * ((1.0 - w) * pointV0W0u + w * pointV0W1u)
            - v * ((1.0 - w) * pointV1W0u + w * pointV1W1u)
            - (1.0 - w) * ((1.0 - u) * pointU0W0v + u * pointU1W0v)
            - w * ((1.0 - u) * pointU0W1v + u * pointU1W1v)
            + (1.0 - u) * ((1.0 - v) * ((1.0 - w) *
                                        pointU0V0W0 + w * pointU0V0W1)
                      + v * ((1.0 - w) * pointU0V1W0 + w * pointU0V1W1))
            + u * ((1.0 - v) * ((1.0 - w) * pointU1V0W0 + w * pointU1V0W1)
                    + v * ((1.0 - w) * pointU1V1W0 + w * pointU1V1W1))
            )
    if dim == 3:
        return result
    elif dim == 4:
        return result[3]



def hexahedron(u, v, w,
               curveU0V0=None,
               curveU0V1=None,
               curveU0W0=None,
               curveU0W1=None,
               curveU1V0=None,
               curveU1V1=None,
               curveU1W0=None,
               curveU1W1=None,
               curveV0W0=None,
               curveV0W1=None,
               curveV1W0=None,
               curveV1W1=None,
               dim=3,
               mappingDictArg={}):
    '''interpolating on a hexahedron given by boundary curves'''
    mappingDict = {'U0V0':lambda x:x,
                    'U0V1':lambda x:x,
                    'U0W0':lambda x:x,
                    'U0W1':lambda x:x,
                    'U1V0':lambda x:x,
                    'U1V1':lambda x:x,
                    'U1W0':lambda x:x,
                    'U1W1':lambda x:x,
                    'V0W0':lambda x:x,
                    'V0W1':lambda x:x,
                    'V1W0':lambda x:x,
                    'V1W1':lambda x:x}
    mappingDict.update(mappingDictArg)
    if not 0 <= u <= 1:
        raise ValueError('u must be inside interval [0,1]')
    if not 0 <= v <= 1:
        raise ValueError('v must be inside interval [0,1]')
    if not 0 <= w <= 1:
        raise ValueError('w must be inside interval [0,1]')

    # mapping the u and v values with the given function.
    # the standard lambda function returns u and v without mapping:
    U0V0Start = mappingDict['U0V0'](0)
    U0V0w = mappingDict['U0V0'](w)
    U0V0Stop = mappingDict['U0V0'](1)

    U0V1Start = mappingDict['U0V1'](0)
    U0V1w = mappingDict['U0V1'](w)
    U0V1Stop = mappingDict['U0V1'](1)

    U0W0v = mappingDict['U0W0'](v)
    U0W1v = mappingDict['U0W1'](v)

    U1V0Start = mappingDict['U1V0'](0)
    U1V0w = mappingDict['U1V0'](w)
    U1V0Stop = mappingDict['U1V0'](1)

    U1V1Start = mappingDict['U1V1'](0)
    U1V1w = mappingDict['U1V1'](w)
    U1V1Stop = mappingDict['U1V1'](1)

    U1W0v = mappingDict['U1W0'](v)
    U1W1v = mappingDict['U1W1'](v)
    V0W0u = mappingDict['V0W0'](u)
    V0W1u = mappingDict['V0W1'](u)
    V1W0u = mappingDict['V1W0'](u)
    V1W1u = mappingDict['V1W1'](u)

    lengthWise = True
    xTol = 1e-12

    if dim == 3:
        pointU0V0W0 = getPoint(curveU0V0.pointAt(U0V0Start))
        if lengthWise:
            U0V0w = brentq(root_lengthwise, U0V0Start, U0V0Stop,
                       args=(U0V0Start, U0V0Stop, U0V0w, curveU0V0), xtol=xTol)
        pointU0V0w = getPoint(curveU0V0.pointAt(U0V0w))
        pointU0V0W1 = getPoint(curveU0V0.pointAt(U0V0Stop))

        pointU0V1W0 = getPoint(curveU0V1.pointAt(U0V1Start))
        if lengthWise:
            U0V1w = brentq(root_lengthwise, U0V1Start, U0V1Stop,
                       args=(U0V1Start, U0V1Stop, U0V1w, curveU0V1), xtol=xTol)
        pointU0V1w = getPoint(curveU0V1.pointAt(U0V1w))
        pointU0V1W1 = getPoint(curveU0V1.pointAt(U0V1Stop))

        pointU1V0W0 = getPoint(curveU1V0.pointAt(U1V0Start))
        if lengthWise:
            U1V0w = brentq(root_lengthwise, U1V0Start, U1V0Stop,
                       args=(U1V0Start, U1V0Stop, U1V0w, curveU1V0), xtol=xTol)
        pointU1V0w = getPoint(curveU1V0.pointAt(U1V0w))
        pointU1V0W1 = getPoint(curveU1V0.pointAt(U1V0Stop))

        pointU1V1W0 = getPoint(curveU1V1.pointAt(U1V1Start))
        if lengthWise:
            U1V1w = brentq(root_lengthwise, U1V1Start, U1V1Stop,
                       args=(U1V1Start, U1V1Stop, U1V1w, curveU1V1), xtol=xTol)
        pointU1V1w = getPoint(curveU1V1.pointAt(U1V1w))
        pointU1V1W1 = getPoint(curveU1V1.pointAt(U1V1Stop))

        pointV0W0u = getPoint(curveV0W0.pointAt(V0W0u))
        pointV0W1u = getPoint(curveV0W1.pointAt(V0W1u))
        pointV1W0u = getPoint(curveV1W0.pointAt(V1W0u))
        pointV1W1u = getPoint(curveV1W1.pointAt(V1W1u))
        pointU0W0v = getPoint(curveU0W0.pointAt(U0W0v))
        pointU1W0v = getPoint(curveU1W0.pointAt(U1W0v))
        pointU0W1v = getPoint(curveU0W1.pointAt(U0W1v))
        pointU1W1v = getPoint(curveU1W1.pointAt(U1W1v))

    elif dim == 4:
        pointU0V0W0 = getHPoint(curveU0V0.hpointAt(U0V0Start))
        if lengthWise:
            U0V0w = brentq(root_lengthwise, U0V0Start, U0V0Stop,
                       args=(U0V0Start, U0V0Stop, U0V0w, curveU0V0), xtol=xTol)
        pointU0V0w = getHPoint(curveU0V0.hpointAt(U0V0w))
        pointU0V0W1 = getHPoint(curveU0V0.hpointAt(U0V0Stop))

        pointU0V1W0 = getHPoint(curveU0V1.hpointAt(U0V1Start))
        if lengthWise:
            U0V1w = brentq(root_lengthwise, U0V1Start, U0V1Stop,
                       args=(U0V1Start, U0V1Stop, U0V1w, curveU0V1), xtol=xTol)
        pointU0V1w = getHPoint(curveU0V1.hpointAt(U0V1w))
        pointU0V1W1 = getHPoint(curveU0V1.hpointAt(U0V1Stop))

        pointU1V0W0 = getHPoint(curveU1V0.hpointAt(U1V0Start))
        if lengthWise:
            U1V0w = brentq(root_lengthwise, U1V0Start, U1V0Stop,
                       args=(U1V0Start, U1V0Stop, U1V0w, curveU1V0), xtol=xTol)
        pointU1V0w = getHPoint(curveU1V0.hpointAt(U1V0w))
        pointU1V0W1 = getHPoint(curveU1V0.hpointAt(U1V0Stop))

        pointU1V1W0 = getHPoint(curveU1V1.hpointAt(U1V1Start))
        if lengthWise:
            U1V1w = brentq(root_lengthwise, U1V1Start, U1V1Stop,
                       args=(U1V1Start, U1V1Stop, U1V1w, curveU1V1), xtol=xTol)
        pointU1V1w = getHPoint(curveU1V1.hpointAt(U1V1w))
        pointU1V1W1 = getHPoint(curveU1V1.hpointAt(U1V1Stop))

        pointV0W0u = getHPoint(curveV0W0.hpointAt(V0W0u))
        pointV0W1u = getHPoint(curveV0W1.hpointAt(V0W1u))
        pointV1W0u = getHPoint(curveV1W0.hpointAt(V1W0u))
        pointV1W1u = getHPoint(curveV1W1.hpointAt(V1W1u))
        pointU0W0v = getHPoint(curveU0W0.hpointAt(U0W0v))
        pointU1W0v = getHPoint(curveU1W0.hpointAt(U1W0v))
        pointU0W1v = getHPoint(curveU0W1.hpointAt(U0W1v))
        pointU1W1v = getHPoint(curveU1W1.hpointAt(U1W1v))

    faceU0 = quadrilateral(v, w, curveU0V0, curveU0V1, curveU0W0, curveU0W1,
                           dim=dim,
                           mappingDictArg={'U0':mappingDict['U0V0'],
                                       'U1':mappingDict['U0V1'],
                                       'V0':mappingDict['U0W0'],
                                       'V1':mappingDict['U0W1'], })
    faceU1 = quadrilateral(v, w, curveU1V0, curveU1V1, curveU1W0, curveU1W1,
                           dim=dim,
                           mappingDictArg={'U0':mappingDict['U1V0'],
                                       'U1':mappingDict['U1V1'],
                                       'V0':mappingDict['U1W0'],
                                       'V1':mappingDict['U1W1'], })
    faceV0 = quadrilateral(u, w, curveU0V0, curveU1V0, curveV0W0, curveV0W1,
                           dim=dim,
                           mappingDictArg={'U0':mappingDict['U0V0'],
                                       'U1':mappingDict['U1V0'],
                                       'V0':mappingDict['V0W0'],
                                       'V1':mappingDict['V0W1'], })  ####
    faceV1 = quadrilateral(u, w, curveU0V1, curveU1V1, curveV1W0, curveV1W1,
                           dim=dim,
                           mappingDictArg={'U0':mappingDict['U0V1'],
                                       'U1':mappingDict['U1V1'],
                                       'V0':mappingDict['V1W0'],
                                       'V1':mappingDict['V1W1'], })
    faceW0 = quadrilateral(u, v, curveU0W0, curveU1W0, curveV0W0, curveV1W0,
                           dim=dim,
                           mappingDictArg={'U0':mappingDict['U0W0'],
                                       'U1':mappingDict['U1W0'],
                                       'V0':mappingDict['V0W0'],
                                       'V1':mappingDict['V1W0'], })
    faceW1 = quadrilateral(u, v, curveU0W1, curveU1W1, curveV0W1, curveV1W1,
                           dim=dim,
                           mappingDictArg={'U0':mappingDict['U0W1'],
                                       'U1':mappingDict['U1W1'],
                                       'V0':mappingDict['V0W1'],
                                       'V1':mappingDict['V1W1'], })
    result = ((1.0 - u) * faceU0 + u * faceU1
            + (1.0 - v) * faceV0 + v * faceV1
            + (1.0 - w) * faceW0 + w * faceW1
            - (1.0 - u) * ((1.0 - v) * pointU0V0w + v * pointU0V1w)
            - u * ((1.0 - v) * pointU1V0w + v * pointU1V1w)
            - (1.0 - v) * ((1.0 - w) * pointV0W0u + w * pointV0W1u)
            - v * ((1.0 - w) * pointV1W0u + w * pointV1W1u)
            - (1.0 - w) * ((1.0 - u) * pointU0W0v + u * pointU1W0v)
            - w * ((1.0 - u) * pointU0W1v + u * pointU1W1v)
            + (1.0 - u) * ((1.0 - v) * ((1.0 - w) *
                                        pointU0V0W0 + w * pointU0V0W1)
                      + v * ((1.0 - w) * pointU0V1W0 + w * pointU0V1W1))
            + u * ((1.0 - v) * ((1.0 - w) * pointU1V0W0 + w * pointU1V0W1)
                    + v * ((1.0 - w) * pointU1V1W0 + w * pointU1V1W1))
            )
    if dim == 3:
        return result
    elif dim == 4:
        return result[3]




def interpolate_block(shape, curvesDict, blockMapping,
                      gradingBlock=None):
    '''Interpolating a block of given shape with given curves and mapping'''

    block = np.zeros(shape)
    print 'interpolating %i points for hexahedral.' % (shape[0] *
                                                       shape[1] * shape[2])
    print 'This may take a while...'

    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                if gradingBlock == None:
                    u = float(i) / (shape[0] - 1.0)
                    v = float(j) / (shape[1] - 1.0)
                    w = float(k) / (shape[2] - 1.0)
                else:
                    u, v, w = gradingBlock[i, j, k]
                if 'curveU0V0' in curvesDict:
                    xyz = hexahedron(u, v, w, curvesDict['curveU0V0'],
                                     curvesDict['curveU0V1'],
                                     curvesDict['curveU0W0'],
                                     curvesDict['curveU0W1'],
                                     curvesDict['curveU1V0'],
                                     curvesDict['curveU1V1'],
                                     curvesDict['curveU1W0'],
                                     curvesDict['curveU1W1'],
                                     curvesDict['curveV0W0'],
                                     curvesDict['curveV0W1'],
                                     curvesDict['curveV1W0'],
                                     curvesDict['curveV1W1'], dim=3,
                                     mappingDictArg=blockMapping)
                else:
                    xyz = hexahedron(u, v, w, curvesDict['U0V0'],
                                     curvesDict['U0V1'],
                                     curvesDict['U0W0'],
                                     curvesDict['U0W1'],
                                     curvesDict['U1V0'],
                                     curvesDict['U1V1'],
                                     curvesDict['U1W0'],
                                     curvesDict['U1W1'],
                                     curvesDict['V0W0'],
                                     curvesDict['V0W1'],
                                     curvesDict['V1W0'],
                                     curvesDict['V1W1'], dim=3,
                                     mappingDictArg=blockMapping)
                block[i, j, k, :] = xyz
    print 'done interpolating!'

    return block


def interpolate_block_nurbs_surface(shape, curvesDict, blockMapping,
                                    surfacesDict, master, quadBoundaries,
                                    gradingBlock=None, switchUV={},
                                    bridgeU={}, bridgeV={}):
    '''Interpolating a block of given shape with given curves and mapping'''

    block = np.zeros(shape)
    print 'interpolating %i points for hexahedral.' % (shape[0] *
                                                       shape[1] * shape[2])
    print 'This may take a while...'

    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                if gradingBlock == None:
                    u = float(i) / (shape[0] - 1.0)
                    v = float(j) / (shape[1] - 1.0)
                    w = float(k) / (shape[2] - 1.0)
                else:
                    u, v, w = gradingBlock[i, j, k]
                xyz = hexahedron_nurbs_surfaces(u, v, w, curvesDict, 
                                                surfacesDict, master, 
                                                quadBoundaries, 
                                                bridgeUDict=bridgeU,
                                                bridgeVDict=bridgeV,
                                                mappingDictArg=blockMapping, 
                                                switchUVArg=switchUV,
                                                dim=3)
                
                block[i, j, k, :] = xyz
    print 'done interpolating!'

    return block



# For testing only
# import nurbsToolSet as nts
import random
#from pythonnurbs import NurbsVector, NurbsCurve, NurbsPoint, NurbsHPoint
from scipy.optimize import brentq#, bisect

def edge(start, end, nurbsintres, parallel=None, factor=0.0):
    '''Creating straight edges'''
    dim = 3
    parray = np.zeros((nurbsintres, dim))
    for i in range(nurbsintres):
        xyz = np.ones([dim], 'd')
        x = start[0] + float(i) / (nurbsintres - 1.0) * (end[0] - start[0])
        y = start[1] + float(i) / (nurbsintres - 1.0) * (end[1] - start[1])
        z = start[2] + float(i) / (nurbsintres - 1.0) * (end[2] - start[2])
        if 0 < i < nurbsintres - 1:
            if parallel == 'x':
                y += factor * (random.random() - 0.5)
                z += factor * (random.random() - 0.5)
            elif parallel == 'y':
                x += factor * (random.random() - 0.5)
                z += factor * (random.random() - 0.5)
            elif parallel == 'z':
                x += factor * (random.random() - 0.5)
                y += factor * (random.random() - 0.5)
        xyz = [x, y, z]
        parray[i] = xyz
    return interpolateNurbs3D(parray)


def root_lengthwise(u, start, end, targetNormLength, curve):
    eps = 1e-6
    n = 500
    reverse = False
    if start > end:
        reverse = True
        start, end = end, start
    totalSegmentLength = curve.lengthIn(start, end, eps, n)
    currentSegmentLength = curve.lengthIn(start, u, eps, n)
    if not reverse:
        diff = targetNormLength - currentSegmentLength / totalSegmentLength
    else:
        diff = targetNormLength - (1.0 - currentSegmentLength / totalSegmentLength)
    return diff

def root_finding_function(u, normLength, curve):
    '''function to find the root for'''
    assert 0 <= normLength <= 1
    return curve.lengthIn(0, u) - curve.length() * normLength

def discretize_edge(edge, discretizationList, precise=False):
    '''discretize the given edge with the given discretization list'''
    dim = 4
    parray = np.zeros((len(discretizationList), dim))
    for i, uValue in enumerate(discretizationList):
        u = float(i) / (len(discretizationList) - 1.0)
        if precise:
            # Info: u stands for normLength!
            #preciseU = bisect(root_finding_function, 0, 1, args=(u, edge))
            u = brentq(root_finding_function, 0, 1, args=(u, edge))
        oldPoint = edge.pointAt(u)

        x = oldPoint.getx()
        y = oldPoint.gety()
        z = oldPoint.getz()
        # adding 1.0 so that weight=0 does not occur:
        w = uValue + 1.0
        xyzw = [x, y, z, w]
        parray[i] = xyzw
    return interpolateNurbs3D(parray, dim=4)


def getPoint(point):
    '''Cponverting nurbs++ point to numpy point'''
    return np.array([point.getx(), point.gety(), point.getz()])


def getHPoint(point):
    '''Cponverting nurbs++ hpoint to numpy point'''
    return np.array([point.getx(), point.gety(), point.getz(), point.getw()])


def interpolateNurbs3D(parray, kps=0, degree=3, dim=3, closed=False):
    '''method from bladedesigner package to itnerpolate nurbs'''
    if kps == 0:
        pts = len(parray)
    else:
        pts = kps

    if pts <= degree:
        degree = pts - 1

    if dim == 3:
        ptsVector = NurbsVector.Vector_Point3Dd(len(parray))
        vector = NurbsVector.Vector_Point3Dd(1)
        for n in range(len(parray)):
            vector.reset(NurbsPoint.Point3Dd(parray[n, 0], parray[n, 1],
                                             parray[n, 2]))
            ptsVector.as_func(n, vector)
        nurbsCurve = NurbsCurve.NurbsCurved()
        if not closed:
            nurbsCurve.leastSquares(ptsVector, degree, pts)
        else:
            nurbsCurve.leastSquaresClosed(ptsVector, degree, pts)
    elif dim == 4:
        ptsVector = NurbsVector.Vector_HPoint3Dd(len(parray))
        for n in range(len(parray)):
            pnt = NurbsHPoint.HPoint3Dd(parray[n, 0], parray[n, 1],
                                        parray[n, 2], parray[n, 3])
            ptsVector[n] = pnt
        nurbsCurve = NurbsCurve.NurbsCurved()
        if not closed:
            nurbsCurve.globalInterpH(ptsVector, degree)
        else:
            nurbsCurve.globalInterpHClosed(ptsVector, degree)

    return nurbsCurve


def create_grading_block(shape, gradingDictArg={}):
    nurbsintres = 3

    # removing entries with None as values:
    gradingDictArg = {k:v for (k, v) in gradingDictArg.items() if v != None}

    gradingDict = {'U0V0':list(np.linspace(0, 1, shape[2])),
                   'U0V1':list(np.linspace(0, 1, shape[2])),
                   'U0W0':list(np.linspace(0, 1, shape[1])),
                   'U0W1':list(np.linspace(0, 1, shape[1])),
                   'U1V0':list(np.linspace(0, 1, shape[2])),
                   'U1V1':list(np.linspace(0, 1, shape[2])),
                   'U1W0':list(np.linspace(0, 1, shape[1])),
                   'U1W1':list(np.linspace(0, 1, shape[1])),
                   'V0W0':list(np.linspace(0, 1, shape[0])),
                   'V0W1':list(np.linspace(0, 1, shape[0])),
                   'V1W0':list(np.linspace(0, 1, shape[0])),
                   'V1W1':list(np.linspace(0, 1, shape[0]))}
    gradingDict.update(gradingDictArg)

    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres)
    curveU0W0 = discretize_edge(curveU0W0, [0] * shape[1])


    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres)
    curveU1W0 = discretize_edge(curveU1W0, [1] * shape[1])


    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres)
    curveV0W0 = discretize_edge(curveV0W0, gradingDict['V0W0'])

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres)
    curveV1W0 = discretize_edge(curveV1W0, gradingDict['V1W0'])

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres)
    curveU0W1 = discretize_edge(curveU0W1, [0] * shape[1])

    start = (1, 0, 1)
    end = (1, 1, 1)
    curveU1W1 = edge(start, end, nurbsintres)
    curveU1W1 = discretize_edge(curveU1W1, [1] * shape[1])

    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres)
    curveV0W1 = discretize_edge(curveV0W1, gradingDict['V0W1'])

    start = (0, 1, 1)
    end = (1, 1, 1)
    curveV1W1 = edge(start, end, nurbsintres)
    curveV1W1 = discretize_edge(curveV1W1, gradingDict['V1W1'])

    # ============

    start = (0, 0, 0)
    end = (0, 0, 1)
    curveU0V0 = edge(start, end, nurbsintres)
    curveU0V0 = discretize_edge(curveU0V0, [0] * shape[2])

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres)
    curveU1V0 = discretize_edge(curveU1V0, [1] * shape[2])


    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres)
    curveU0V1 = discretize_edge(curveU0V1, [0] * shape[2])

    start = (1, 1, 0)
    end = (1, 1, 1)
    curveU1V1 = edge(start, end, nurbsintres)
    curveU1V1 = discretize_edge(curveU1V1, [1] * shape[2])

    # ==============

    gradingBlock = np.zeros((shape[0], shape[1], shape[2], 3))
    #print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
    #                                                   shape[2])
    #print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                u = float(i) / (shape[0] - 1.0)
                v = float(j) / (shape[1] - 1.0)
                w = float(k) / (shape[2] - 1.0)
                interpolU = hexahedron(u, v, w,
                                       curveU0V0, curveU0V1, curveU0W0,
                                       curveU0W1, curveU1V0, curveU1V1,
                                       curveU1W0, curveU1W1, curveV0W0,
                                       curveV0W1, curveV1W0, curveV1W1, dim=4)
                # Subtracting 1 to account for adding 1 in nts.discretize_edge!
                gradingBlock[i, j, k, 0] = interpolU - 1.0
    #print 'done interpolating for U!'


    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres)
    curveU0W0 = discretize_edge(curveU0W0, gradingDict['U0W0'])


    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres)
    curveU1W0 = discretize_edge(curveU1W0, gradingDict['U1W0'])


    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres)
    curveV0W0 = discretize_edge(curveV0W0, [0] * shape[0])

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres)
    curveV1W0 = discretize_edge(curveV1W0, [1] * shape[0])

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres)
    curveU0W1 = discretize_edge(curveU0W1, gradingDict['U0W1'])

    start = (1, 0, 1)
    end = (1, 1, 1)
    curveU1W1 = edge(start, end, nurbsintres)
    curveU1W1 = discretize_edge(curveU1W1, gradingDict['U1W1'])


    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres)
    curveV0W1 = discretize_edge(curveV0W1, [0] * shape[0])

    start = (0, 1, 1)
    end = (1, 1, 1)
    curveV1W1 = edge(start, end, nurbsintres)
    curveV1W1 = discretize_edge(curveV1W1, [1] * shape[0])

    # ============

    start = (0, 0, 0)
    end = (0, 0, 1)
    curveU0V0 = edge(start, end, nurbsintres)
    curveU0V0 = discretize_edge(curveU0V0, [0] * shape[2])

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres)
    curveU1V0 = discretize_edge(curveU1V0, [0] * shape[2])


    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres)
    curveU0V1 = discretize_edge(curveU0V1, [1] * shape[2])

    start = (1, 1, 0)
    end = (1, 1, 1)
    curveU1V1 = edge(start, end, nurbsintres)
    curveU1V1 = discretize_edge(curveU1V1, [1] * shape[2])

    # ==============

    #print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
    #                                                   shape[2])
    #print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                u = float(i) / (shape[0] - 1.0)
                v = float(j) / (shape[1] - 1.0)
                w = float(k) / (shape[2] - 1.0)
                interpolV = hexahedron(u, v, w,
                                       curveU0V0, curveU0V1, curveU0W0,
                                       curveU0W1, curveU1V0, curveU1V1,
                                       curveU1W0, curveU1W1, curveV0W0,
                                       curveV0W1, curveV1W0, curveV1W1, dim=4)
                # Subtracting 1 to account for adding 1 in nts.discretize_edge!
                gradingBlock[i, j, k, 1] = interpolV - 1.0
    #print 'done interpolating for V!'


    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres)
    curveU0W0 = discretize_edge(curveU0W0, [0] * shape[1])


    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres)
    curveU1W0 = discretize_edge(curveU1W0, [0] * shape[1])


    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres)
    curveV0W0 = discretize_edge(curveV0W0, [0] * shape[0])

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres)
    curveV1W0 = discretize_edge(curveV1W0, [0] * shape[0])

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres)
    curveU0W1 = discretize_edge(curveU0W1, [1] * shape[1])

    start = (1, 0, 1)
    end = (1, 1, 1)
    curveU1W1 = edge(start, end, nurbsintres)
    curveU1W1 = discretize_edge(curveU1W1, [1] * shape[1])


    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres)
    curveV0W1 = discretize_edge(curveV0W1, [1] * shape[0])

    start = (0, 1, 1)
    end = (1, 1, 1)
    curveV1W1 = edge(start, end, nurbsintres)
    curveV1W1 = discretize_edge(curveV1W1, [1] * shape[0])

    # ============

    start = (0, 0, 0)
    end = (0, 0, 1)
    curveU0V0 = edge(start, end, nurbsintres)
    curveU0V0 = discretize_edge(curveU0V0, gradingDict['U0V0'])

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres)
    curveU1V0 = discretize_edge(curveU1V0, gradingDict['U1V0'])


    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres)
    curveU0V1 = discretize_edge(curveU0V1, gradingDict['U0V1'])

    start = (1, 1, 0)
    end = (1, 1, 1)
    curveU1V1 = edge(start, end, nurbsintres)
    curveU1V1 = discretize_edge(curveU1V1, gradingDict['U1V1'])

    # ==============

    #print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
    #                                                   shape[2])
    #print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                u = float(i) / (shape[0] - 1.0)
                v = float(j) / (shape[1] - 1.0)
                w = float(k) / (shape[2] - 1.0)
                interpolW = hexahedron(u, v, w,
                                       curveU0V0, curveU0V1, curveU0W0,
                                       curveU0W1, curveU1V0, curveU1V1,
                                       curveU1W0, curveU1W1, curveV0W0,
                                       curveV0W1, curveV1W0, curveV1W1, dim=4)
                # Subtracting 1 to account for adding 1 in nts.discretize_edge!
                gradingBlock[i, j, k, 2] = interpolW - 1.0
    #print 'done interpolating for W!'

    # setting all values inside interval [0,1]
    gradingBlock = gradingBlock.clip(0, 1)
    return gradingBlock

