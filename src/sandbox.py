#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
#from transfinite import edge, discretize_edge
from test_bd import interpolateNurbs3D, setPoint
from pythonnurbs2vtk import curve_points_2_vtk, control_points_2_vtk

pArray = np.zeros((6, 3))

pArray[0] = (0, 2, 0)
pArray[1] = (0.5, 1.2, 0)
pArray[2] = (1, 1, 0)
pArray[3] = (1.5, 1.1, 0)
pArray[4] = (2, 2, 0)
pArray[5] = (1.5, 3, 0)

curve = interpolateNurbs3D(pArray, degree=2)
#'''
tanPointNp = (1, 0, 0)
tanPointNp = tanPointNp / np.linalg.norm(tanPointNp)
tanPoint = setPoint(tanPointNp)
curve.setTangent(0.5, tanPoint)
#'''
curve_points_2_vtk(curve, path='sanboxCurve.vtp')
control_points_2_vtk(curve, path='sanboxControlPoints.vtp')











