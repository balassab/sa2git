#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2013 by Balint Balassa                                  *
#*                                                                         *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

from os.path import join
from os import getcwd

#import numpy as np

from CGNS import CGNS 




class CgnsWriter(object):
    
    
    def __init__(self, topo):
        self.topo = topo
        self.debugLevel = 1
        
        ## Global Error level. Every CGNS Midlevel Library function returns 
        # a value which represents the error status.
        ## After every Midlevel libraqry function call this value is checked, 
        # and adequate debug messages are printed.
        ## The value 0 means there was no error.
        self.ier = 0
        
        ## Cell dimensions of mesh for CGNS file
        self.cell_dim = 3
        ## Physical dimensions of mesh
        self.phys_dim = 3
        
        # Initializing all names
        ## Name convention for CGNS file
        self.fileName = "mesh.cgns"
        ## Name convention for name of the root node.
        self.baseName = "TurboMachine"
        ## Name convention for each sub node of root.
        self.blockName = "Block-"
        self.fileDir = None
        
        # Initializing required pointers
        ## Required file pointer for midlevel library
        self.filePointer = CGNS.intp()
        ## Required base pointer for midlevel library
        self.basePointer = CGNS.intp()
        ## Required zone pointer for midlevel library
        self.zoneIndexPointer = CGNS.intp()
    
    
    def __Call__(self, topo):
        if self.fileDir == None:
            self.fileDir = getcwd()
        self.write_cgns_file(topo)
    
    
    def write_cgns_file(self):
        self.filePath = join(self.fileDir, self.fileName)
        print 'Writing file %s...' % self.filePath
        self.ier = CGNS.cg_open((self.filePath), CGNS.CG_MODE_WRITE, 
                                self.filePointer)
        self.debug("cg_open")
        
        self.ier = CGNS.cg_base_write(self.filePointer.value(), self.baseName, 
                                      self.cell_dim, self.phys_dim, 
                                      self.basePointer)
        self.debug("cg_base_write")
        
        self.ier = CGNS.cg_goto(self.filePointer.value(), 
                                self.basePointer.value(), "end")
        self.debug("cg_goto")
        
        self.ier = CGNS.cg_state_write("Dimensional")
        self.debug("cg_state_write")
        self.ier = CGNS.cg_dataclass_write(CGNS.Dimensional)
        self.debug("cg_dataclass_write")
        self.ier = CGNS.cg_units_write(CGNS.Kilogram, CGNS.Meter, CGNS.Second, 
                                       CGNS.Kelvin, CGNS.Radian)
        self.debug("cg_units_write")
        
        
        # close cgns file
        self.ier = CGNS.cg_close(self.filePointer.value())
        self.debug("cg_close")
        
        self.write_blocks()
        
        print " done!"
    
    
    ## @brief Helps debugging CGNS functions.
    # @param self Self pointer.
    # @param message Message that is printed if an error occurs 
    # that invokes this method.
    def debug(self, message):
        if self.debugLevel >= 1 and self.ier != 0:
            print message, "\tError:", self.ier
            CGNS.cg_error_exit
            raise('Exiting because of cgns error!')
    
    def write_block(self, npArray, blockNumber):
        self.ier = CGNS.cg_open((self.filePath), CGNS.CG_MODE_MODIFY, 
                                self.filePointer)
        self.debug("cg_open")
        self.ier = CGNS.cg_goto(self.filePointer.value(), 
                                self.basePointer.value(), "end")
        self.debug("cg_goto")
        
        numberVertices = npArray.size
        CoordX = CGNS.doubleArray(numberVertices)
        CoordY = CGNS.doubleArray(numberVertices)
        CoordZ = CGNS.doubleArray(numberVertices)
        
        shape = npArray.shape
        for k in range(shape[2]):
            for j in range(shape[1]):
                for i in range(shape[0]):
                    l = k * (shape[0] * shape[1]) + j * shape[0] + i
                    CoordX[l] = npArray[i, j, k, 0]
                    CoordY[l] = npArray[i, j, k, 1]
                    CoordZ[l] = npArray[i, j, k, 2]
        
        self.zoneArrayp = CGNS.intArray(6)
        self.zoneArrayp[0] = shape[0]
        self.zoneArrayp[1] = shape[1]
        self.zoneArrayp[2] = shape[2]
        self.zoneArrayp[3] = shape[0] - 1
        self.zoneArrayp[4] = shape[1] - 1
        self.zoneArrayp[5] = shape[2] - 1
        
        self.zoneName = self.blockName + str(blockNumber)
        self.ier = CGNS.cg_zone_write(self.filePointer.value(), 
                                      self.basePointer.value(), self.zoneName, 
                                      self.zoneArrayp, CGNS.Structured, 
                                      self.zoneIndexPointer)
        self.debug("cg_zone_write block "+str(blockNumber))
        
        N = CGNS.intp()
        self.ier = CGNS.cg_nzones(self.filePointer.value(), self.basePointer.value(), N)
        self.debug("cg_nzones block "+str(blockNumber))
        
        C = CGNS.intp()
        self.ier = CGNS.cg_coord_write(self.filePointer.value(), 
                                       self.basePointer.value(), 
                                       self.zoneIndexPointer.value(), 
                                       CGNS.RealDouble, "CoordinateX", 
                                       CoordX, C)
        self.debug("cg_coord_write block X "+str(blockNumber))
        self.ier = CGNS.cg_coord_write(self.filePointer.value(), 
                                       self.basePointer.value(), 
                                       self.zoneIndexPointer.value(), 
                                       CGNS.RealDouble, "CoordinateY", 
                                       CoordY, C)
        self.debug("cg_coord_write block Y "+str(blockNumber))
        self.ier = CGNS.cg_coord_write(self.filePointer.value(), 
                                       self.basePointer.value(), 
                                       self.zoneIndexPointer.value(), 
                                       CGNS.RealDouble, "CoordinateZ", 
                                       CoordZ, C)
        self.debug("cg_coord_write block Z "+str(blockNumber))
        
        self.ier = CGNS.cg_close(self.filePointer.value())
        self.debug("cg_close")
        
    
    def write_blocks(self):
        for blockIndex, block in enumerate(self.topo.blockList):
            self.write_block(block, blockIndex)
    






