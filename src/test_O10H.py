#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module Docstring
Docstrings: http://www.python.org/dev/peps/pep-0257/
"""

#***************************************************************************
# *   Copyright (C) 2013 by Balint Balassa                                  *
# *                                                                         *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 3 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import numpy as np

from topo import Topo, create_connection
from foamMeshWriter import FoamMeshWriter
from transfinite import edge, hexahedron, interpolateNurbs3D, getPoint
# from pythonnurbs2vtk import curve_points_2_vtk


class O10H:
    '''Test Class for generic O10H topology'''

    def __init__(self):

        self.topo = Topo()

        self.shape = (6, 6, 2, 3)

        self.origin = np.array((0, 0, 0))
        self.length = np.array((1, 0, 0))
        self.width = np.array((0, 0.5, 0))
        self.height = np.array((0, 0, 0.05))

        self.profileThickness = 0.2
        self.profileLength = 0.5
        self.uValuesProfile = [0.05, 0.25, 0.45, 0.55, 0.75, 0.95]

        self.scaledProfileThickness = 0.3
        self.scaledProfileLength = 0.7
        self.uValuesScaledProfile = [0.05, 0.25, 0.45, 0.55, 0.75, 0.95]

        self.uValuesInlet = [0.333, 0.666]
        self.uValuesOutlet = [0.333, 0.666]

        self.uValuesCyclicLower = [0.2, 0.5, 0.8]
        self.uValuesCyclicUpper = [0.2, 0.5, 0.8]

        self.center = None

        # Bottom Layer
        self.cyclicLowerCurveBottom = None
        self.cyclicUpperCurveBottom = None
        self.inletBottom = None
        self.outletBottom = None
        self.profileBottom = None
        self.scaledProfileBottom = None

        # Top Layer
        self.cyclicLowerCurveTop = None
        self.cyclicUpperCurveTop = None
        self.inletTop = None
        self.outletTop = None
        self.profileTop = None
        self.scaledProfileTop = None

        self.PointsBottom = range(26)
        self.PointsTop = range(26)

        self.edge_1_5_Bottom = None
        self.edge_2_8_Bottom = None
        self.edge_4_5_Bottom = None
        self.edge_5_6_Bottom = None
        self.edge_8_7_Bottom = None
        self.edge_8_9_Bottom = None
        self.edge_10_11_Bottom = None
        self.edge_11_12_Bottom = None
        self.edge_13_14_Bottom = None
        self.edge_14_15_Bottom = None

        self.edge_1_5_Top = None
        self.edge_2_8_Top = None
        self.edge_4_5_Top = None
        self.edge_5_6_Top = None
        self.edge_8_7_Top = None
        self.edge_8_9_Top = None
        self.edge_10_11_Top = None
        self.edge_11_12_Top = None
        self.edge_13_14_Top = None
        self.edge_14_15_Top = None

        self.qo0 = None
        self.qo1 = None
        self.qo2 = None
        self.qo3 = None
        self.qo4 = None
        self.qo5 = None
        self.qo6 = None
        self.qo7 = None
        self.qo8 = None
        self.qo9 = None
        self.qo10 = None
        self.qo11 = None
        self.qo12 = None
        self.qo13 = None
        self.qo14 = None
        self.qo15 = None
        self.qo16 = None
        self.qo17 = None
        self.qo18 = None
        self.qo19 = None
        self.qo20 = None
        self.qo21 = None
        self.qo22 = None
        self.qo23 = None
        self.qo24 = None
        self.qo25 = None


    def __call__(self):
        self.set_outer_edges()
        self.set_points()
        self.set_qo()
        self.set_inner_edges()
        self.create_blocks()
        self.create_connections()
        self.topo.check_connectivity()
        self.write_O10H()


    def set_outer_edges(self):
        self.center = self.origin + (self.length + self.width) / 2.0

        # Bottom Layer
        self.cyclicLowerCurveBottom = edge(self.origin, self.origin +
                                           self.length, 3)
        self.cyclicUpperCurveBottom = edge(self.origin + self.width,
                                           self.origin + self.length +
                                           self.width, 3)
        self.inletBottom = edge(self.origin, self.origin + self.width, 3)
        self.outletBottom = edge(self.origin + self.length, self.origin +
                                 self.length + self.width, 3)
        self.profileBottom = self.create_profile(self.profileLength / 2.0,
                                           self.profileThickness / 2.0)
        self.scaledProfileBottom = self.create_profile(self.scaledProfileLength
                                                       / 2.0,
                                                 self.scaledProfileThickness /
                                                 2.0)

        # Top Layer
        self.cyclicLowerCurveTop = edge(self.origin + self.height,
                                        self.origin + self.length + self.height,
                                        3)
        self.cyclicUpperCurveTop = edge(self.origin + self.width + self.height,
                                        self.origin + self.length + self.width +
                                        self.height, 3)
        self.inletTop = edge(self.origin + self.height, self.origin +
                             self.width + self.height, 3)
        self.outletTop = edge(self.origin + self.length + self.height,
                              self.origin + self.length + self.width +
                              self.height, 3)
        self.profileTop = self.create_profile(self.profileLength / 2.0,
                                           self.profileThickness / 2.0,
                                           z=self.height[2])
        self.scaledProfileTop = self.create_profile(self.scaledProfileLength /
                                                    2.0,
                                                 self.scaledProfileThickness /
                                                 2.0, z=self.height[2])


    def set_points(self):
        # creating list of points on bottom layer:
        self.PointsBottom[0] = getPoint(self.inletBottom.pointAt(0))
        self.PointsBottom[1] = getPoint(self.inletBottom.pointAt(
                                            self.uValuesInlet[0]))
        self.PointsBottom[2] = getPoint(self.inletBottom.pointAt(
                                            self.uValuesInlet[1]))
        self.PointsBottom[3] = getPoint(self.inletBottom.pointAt(1))
        self.PointsBottom[4] = getPoint(self.cyclicLowerCurveBottom.pointAt(
                                            self.uValuesCyclicLower[0]))
        self.PointsBottom[5] = getPoint(self.scaledProfileBottom.pointAt(
                                            self.uValuesScaledProfile[3]))
        self.PointsBottom[6] = getPoint(self.profileBottom.pointAt(
                                            self.uValuesProfile[3]))
        self.PointsBottom[7] = getPoint(self.profileBottom.pointAt(
                                            self.uValuesProfile[2]))
        self.PointsBottom[8] = getPoint(self.scaledProfileBottom.pointAt(
                                            self.uValuesScaledProfile[2]))
        self.PointsBottom[9] = getPoint(self.cyclicUpperCurveBottom.pointAt(
                                            self.uValuesCyclicUpper[0]))
        self.PointsBottom[10] = getPoint(self.cyclicLowerCurveBottom.pointAt(
                                            self.uValuesCyclicLower[1]))
        self.PointsBottom[11] = getPoint(self.scaledProfileBottom.pointAt(
                                            self.uValuesScaledProfile[4]))
        self.PointsBottom[12] = getPoint(self.profileBottom.pointAt(
                                            self.uValuesProfile[4]))
        self.PointsBottom[13] = getPoint(self.profileBottom.pointAt(
                                            self.uValuesProfile[1]))
        self.PointsBottom[14] = getPoint(self.scaledProfileBottom.pointAt(
                                            self.uValuesScaledProfile[1]))
        self.PointsBottom[15] = getPoint(self.cyclicUpperCurveBottom.pointAt(
                                            self.uValuesCyclicUpper[1]))
        self.PointsBottom[16] = getPoint(self.cyclicLowerCurveBottom.pointAt(
                                            self.uValuesCyclicLower[2]))
        self.PointsBottom[17] = getPoint(self.scaledProfileBottom.pointAt(
                                            self.uValuesScaledProfile[5]))
        self.PointsBottom[18] = getPoint(self.profileBottom.pointAt(
                                            self.uValuesProfile[5]))
        self.PointsBottom[19] = getPoint(self.profileBottom.pointAt(
                                            self.uValuesProfile[0]))
        self.PointsBottom[20] = getPoint(self.scaledProfileBottom.pointAt(
                                            self.uValuesScaledProfile[0]))
        self.PointsBottom[21] = getPoint(self.cyclicUpperCurveBottom.pointAt(
                                            self.uValuesCyclicUpper[2]))
        self.PointsBottom[22] = getPoint(self.outletBottom.pointAt(0))
        self.PointsBottom[23] = getPoint(self.outletBottom.pointAt(
                                            self.uValuesOutlet[0]))
        self.PointsBottom[24] = getPoint(self.outletBottom.pointAt(
                                            self.uValuesOutlet[1]))
        self.PointsBottom[25] = getPoint(self.outletBottom.pointAt(1))

        # creating list of points on top layer:
        self.PointsTop[0] = getPoint(self.inletTop.pointAt(0))
        self.PointsTop[1] = getPoint(self.inletTop.pointAt(
                                            self.uValuesInlet[0]))
        self.PointsTop[2] = getPoint(self.inletTop.pointAt(
                                            self.uValuesInlet[1]))
        self.PointsTop[3] = getPoint(self.inletTop.pointAt(1))
        self.PointsTop[4] = getPoint(self.cyclicLowerCurveTop.pointAt(
                                            self.uValuesCyclicLower[0]))
        self.PointsTop[5] = getPoint(self.scaledProfileTop.pointAt(
                                            self.uValuesScaledProfile[3]))
        self.PointsTop[6] = getPoint(self.profileTop.pointAt(
                                            self.uValuesProfile[3]))
        self.PointsTop[7] = getPoint(self.profileTop.pointAt(
                                            self.uValuesProfile[2]))
        self.PointsTop[8] = getPoint(self.scaledProfileTop.pointAt(
                                            self.uValuesScaledProfile[2]))
        self.PointsTop[9] = getPoint(self.cyclicUpperCurveTop.pointAt(
                                            self.uValuesCyclicUpper[0]))
        self.PointsTop[10] = getPoint(self.cyclicLowerCurveTop.pointAt(
                                            self.uValuesCyclicLower[1]))
        self.PointsTop[11] = getPoint(self.scaledProfileTop.pointAt(
                                            self.uValuesScaledProfile[4]))
        self.PointsTop[12] = getPoint(self.profileTop.pointAt(
                                            self.uValuesProfile[4]))
        self.PointsTop[13] = getPoint(self.profileTop.pointAt(
                                            self.uValuesProfile[1]))
        self.PointsTop[14] = getPoint(self.scaledProfileTop.pointAt(
                                            self.uValuesScaledProfile[1]))
        self.PointsTop[15] = getPoint(self.cyclicUpperCurveTop.pointAt(
                                            self.uValuesCyclicUpper[1]))
        self.PointsTop[16] = getPoint(self.cyclicLowerCurveTop.pointAt(
                                            self.uValuesCyclicLower[2]))
        self.PointsTop[17] = getPoint(self.scaledProfileTop.pointAt(
                                            self.uValuesScaledProfile[5]))
        self.PointsTop[18] = getPoint(self.profileTop.pointAt(
                                            self.uValuesProfile[5]))
        self.PointsTop[19] = getPoint(self.profileTop.pointAt(
                                            self.uValuesProfile[0]))
        self.PointsTop[20] = getPoint(self.scaledProfileTop.pointAt(
                                            self.uValuesScaledProfile[0]))
        self.PointsTop[21] = getPoint(self.cyclicUpperCurveTop.pointAt(
                                            self.uValuesCyclicUpper[2]))
        self.PointsTop[22] = getPoint(self.outletTop.pointAt(0))
        self.PointsTop[23] = getPoint(self.outletTop.pointAt(
                                            self.uValuesOutlet[0]))
        self.PointsTop[24] = getPoint(self.outletTop.pointAt(
                                            self.uValuesOutlet[1]))
        self.PointsTop[25] = getPoint(self.outletTop.pointAt(1))


    def set_qo(self):
        # creating quasiorthogonal edges:
        for i in range(26):
            tmpString = 'self.qo%i = edge(self.PointsBottom[%i], \
                            self.PointsTop[%i], 3)' % (i, i, i)
            exec tmpString


    def set_inner_edges(self):
        # creating S1 edges bottom layer:
        self.edge_1_5_Bottom = edge(self.PointsBottom[1],
                                    self.PointsBottom[5], 3)
        self.edge_2_8_Bottom = edge(self.PointsBottom[2],
                                    self.PointsBottom[8], 3)
        self.edge_4_5_Bottom = edge(self.PointsBottom[4],
                                    self.PointsBottom[5], 3)
        self.edge_5_6_Bottom = edge(self.PointsBottom[5],
                                    self.PointsBottom[6], 3)
        self.edge_8_9_Bottom = edge(self.PointsBottom[8],
                                    self.PointsBottom[9], 3)
        self.edge_8_7_Bottom = edge(self.PointsBottom[8],
                                    self.PointsBottom[7], 3)
        self.edge_10_11_Bottom = edge(self.PointsBottom[10],
                                      self.PointsBottom[11], 3)
        self.edge_11_12_Bottom = edge(self.PointsBottom[11],
                                      self.PointsBottom[12], 3)
        self.edge_13_14_Bottom = edge(self.PointsBottom[13],
                                      self.PointsBottom[14], 3)
        self.edge_14_15_Bottom = edge(self.PointsBottom[14],
                                      self.PointsBottom[15], 3)

        # creating S1 edges top layer:
        self.edge_1_5_Top = edge(self.PointsTop[1], self.PointsTop[5], 3)
        self.edge_2_8_Top = edge(self.PointsTop[2], self.PointsTop[8], 3)
        self.edge_4_5_Top = edge(self.PointsTop[4], self.PointsTop[5], 3)
        self.edge_5_6_Top = edge(self.PointsTop[5], self.PointsTop[6], 3)
        self.edge_8_9_Top = edge(self.PointsTop[8], self.PointsTop[9], 3)
        self.edge_8_7_Top = edge(self.PointsTop[8], self.PointsTop[7], 3)
        self.edge_10_11_Top = edge(self.PointsTop[10], self.PointsTop[11], 3)
        self.edge_11_12_Top = edge(self.PointsTop[11], self.PointsTop[12], 3)
        self.edge_13_14_Top = edge(self.PointsTop[13], self.PointsTop[14], 3)
        self.edge_14_15_Top = edge(self.PointsTop[14], self.PointsTop[15], 3)


    def write_O10H(self):
        '''Writing the O10H Topology with the foamMeshWriter'''

        f = FoamMeshWriter()
        f.topo = self.topo
        f.case = '../O10HTest'
        f()
        print '\nFinished!'


    def ellipse(self, x, a=0.3, b=0.05, polar=True, deg=True):
        '''Creating an ellipse'''

        if polar:
            epsilon = np.sqrt(a ** 2 - b ** 2) / a
            if deg:
                phi = np.deg2rad(x)
            else:
                phi = x
            r = b / np.sqrt(1.0 - epsilon ** 2 * np.cos(phi) ** 2)
            y = r * np.sin(phi) + self.center[1]
            x = r * np.cos(phi) + self.center[0]
        else:
            y = self.center[1] + np.sqrt((1.0 - (x - self.center[0]) ** 2 /
                                          a ** 2) * b ** 2)

        return x, y


    def create_profile(self, a, b, z=0, resolution=20):
        '''Creating a blade profile with an ellipse'''

        pntArray = np.zeros((resolution, 3))
        for i in range(resolution):
            phi = float(i) / (resolution - 1.0) * 360.0
            x, y = self.ellipse(phi, a, b, polar=True, deg=True)
            pntArray[i, :] = x, y, z

        return interpolateNurbs3D(pntArray, closed=False)


    def create_blocks(self):
        '''Creating the blocks of the O10H topology'''

        # ======= BLOCK 0 =================
        block0Mapping = {'U0W0':lambda x:x * self.uValuesInlet[0],
                         'U0W1':lambda x:x * self.uValuesInlet[0],
                         'V0W0':lambda x:x * self.uValuesCyclicLower[0],
                         'V0W1':lambda x:x * self.uValuesCyclicLower[0]}

        curvesDict = {'curveU0V0':self.qo0,
                      'curveU0V1':self.qo1,
                      'curveU0W0':self.inletBottom,
                      'curveU0W1':self.inletTop,
                      'curveU1V0':self.qo4,
                      'curveU1V1':self.qo5,
                      'curveU1W0':self.edge_4_5_Bottom,
                      'curveU1W1':self.edge_4_5_Top,
                      'curveV0W0':self.cyclicLowerCurveBottom,
                      'curveV0W1':self.cyclicLowerCurveTop,
                      'curveV1W0':self.edge_1_5_Bottom,
                      'curveV1W1':self.edge_1_5_Top
                      }

        block0 = self.interpolate_block(self.shape, curvesDict, block0Mapping)
        self.topo.blockList.append(block0)

        # ======= BLOCK 1 =================
        curvesDict = {'curveU0V0':self.qo1,
                      'curveU0V1':self.qo2,
                      'curveU0W0':self.inletBottom,
                      'curveU0W1':self.inletTop,
                      'curveU1V0':self.qo5,
                      'curveU1V1':self.qo6,
                      'curveU1W0':self.scaledProfileBottom,
                      'curveU1W1':self.scaledProfileTop,
                      'curveV0W0':self.edge_1_5_Bottom,
                      'curveV0W1':self.edge_1_5_Top,
                      'curveV1W0':self.edge_2_8_Bottom,
                      'curveV1W1':self.edge_2_8_Top
                      }

        block1Mapping = {'U0W0':lambda x:self.uValuesInlet[0] + x *
                                        (self.uValuesInlet[1] -
                                         self.uValuesInlet[0]),
                         'U0W1':lambda x:self.uValuesInlet[0] + x *
                                        (self.uValuesInlet[1] -
                                         self.uValuesInlet[0]),
                         'U1W0':lambda x:self.uValuesScaledProfile[3] - x *
                                        (self.uValuesScaledProfile[3] -
                                         self.uValuesScaledProfile[2]),
                         'U1W1':lambda x:self.uValuesScaledProfile[3] - x *
                                        (self.uValuesScaledProfile[3] -
                                         self.uValuesScaledProfile[2])
                         }
        block1 = self.interpolate_block(self.shape, curvesDict, block1Mapping)
        self.topo.blockList.append(block1)

        # ======= BLOCK 2 =================
        block2Mapping = {'U0W0':lambda x:self.uValuesInlet[1] + x *
                                        (self.uValuesInlet[1] -
                                         self.uValuesInlet[0]),
                         'U0W1':lambda x:self.uValuesInlet[1] + x *
                                        (self.uValuesInlet[1] -
                                         self.uValuesInlet[0]),
                         'V1W0':lambda x:x * self.uValuesCyclicUpper[0],
                         'V1W1':lambda x:x * self.uValuesCyclicUpper[0]}

        curvesDict = {'curveU0V0':self.qo2,
                      'curveU0V1':self.qo3,
                      'curveU0W0':self.inletBottom,
                      'curveU0W1':self.inletTop,
                      'curveU1V0':self.qo8,
                      'curveU1V1':self.qo9,
                      'curveU1W0':self.edge_8_9_Bottom,
                      'curveU1W1':self.edge_8_9_Top,
                      'curveV0W0':self.edge_2_8_Bottom,
                      'curveV0W1':self.edge_2_8_Top,
                      'curveV1W0':self.cyclicUpperCurveBottom,
                      'curveV1W1':self.cyclicUpperCurveTop
                      }

        block2 = self.interpolate_block(self.shape, curvesDict, block2Mapping)
        self.topo.blockList.append(block2)

        # ======= BLOCK 3 =================
        block3Mapping = {'V1W0':lambda x:self.uValuesScaledProfile[3] + x *
                                        (self.uValuesScaledProfile[4] -
                                         self.uValuesScaledProfile[3]),
                         'V1W1':lambda x:self.uValuesScaledProfile[3] + x *
                                        (self.uValuesScaledProfile[4] -
                                         self.uValuesScaledProfile[3]),
                         'V0W0':lambda x:self.uValuesCyclicLower[0] + x *
                                        (self.uValuesCyclicLower[1] -
                                         self.uValuesCyclicLower[0]),
                         'V0W1':lambda x:self.uValuesCyclicLower[0] + x *
                                        (self.uValuesCyclicLower[1] -
                                         self.uValuesCyclicLower[0])}

        curvesDict = {'curveU0V0':self.qo4,
                      'curveU0V1':self.qo5,
                      'curveU0W0':self.edge_4_5_Bottom,
                      'curveU0W1':self.edge_4_5_Top,
                      'curveU1V0':self.qo10,
                      'curveU1V1':self.qo11,
                      'curveU1W0':self.edge_10_11_Bottom,
                      'curveU1W1':self.edge_10_11_Top,
                      'curveV0W0':self.cyclicLowerCurveBottom,
                      'curveV0W1':self.cyclicLowerCurveTop,
                      'curveV1W0':self.scaledProfileBottom,
                      'curveV1W1':self.scaledProfileTop
                      }

        block3 = self.interpolate_block(self.shape, curvesDict, block3Mapping)
        self.topo.blockList.append(block3)

        # ======= BLOCK 4 =================
        block4Mapping = {'V0W0':lambda x:self.uValuesScaledProfile[3] + x *
                                        (self.uValuesScaledProfile[4] -
                                         self.uValuesScaledProfile[3]),
                         'V0W1':lambda x:self.uValuesScaledProfile[3] + x *
                                        (self.uValuesScaledProfile[4] -
                                         self.uValuesScaledProfile[3]),
                         'V1W0':lambda x:self.uValuesProfile[3] + x *
                                        (self.uValuesProfile[4] -
                                         self.uValuesProfile[3]),
                         'V1W1':lambda x:self.uValuesProfile[3] + x *
                                        (self.uValuesProfile[4] -
                                         self.uValuesProfile[3])}

        curvesDict = {'curveU0V0':self.qo5,
                      'curveU0V1':self.qo6,
                      'curveU0W0':self.edge_5_6_Bottom,
                      'curveU0W1':self.edge_5_6_Top,
                      'curveU1V0':self.qo11,
                      'curveU1V1':self.qo12,
                      'curveU1W0':self.edge_11_12_Bottom,
                      'curveU1W1':self.edge_11_12_Top,
                      'curveV0W0':self.scaledProfileBottom,
                      'curveV0W1':self.scaledProfileTop,
                      'curveV1W0':self.profileBottom,
                      'curveV1W1':self.profileTop
                      }

        block4 = self.interpolate_block(self.shape, curvesDict, block4Mapping)
        self.topo.blockList.append(block4)

        # ======= BLOCK 5 =================
        curvesDict = {'curveU0V0':self.qo5,
                      'curveU0V1':self.qo8,
                      'curveU0W0':self.scaledProfileBottom,
                      'curveU0W1':self.scaledProfileTop,
                      'curveU1V0':self.qo6,
                      'curveU1V1':self.qo7,
                      'curveU1W0':self.profileBottom,
                      'curveU1W1':self.profileTop,
                      'curveV0W0':self.edge_5_6_Bottom,
                      'curveV0W1':self.edge_5_6_Top,
                      'curveV1W0':self.edge_8_7_Bottom,
                      'curveV1W1':self.edge_8_7_Top
                      }

        block5Mapping = {'U1W0':lambda x:self.uValuesProfile[3] - x *
                                        (self.uValuesProfile[3] -
                                         self.uValuesProfile[2]),
                         'U1W1':lambda x:self.uValuesProfile[3] - x *
                                        (self.uValuesProfile[3] -
                                         self.uValuesProfile[2]),
                         'U0W0':lambda x:self.uValuesScaledProfile[3] - x *
                                        (self.uValuesScaledProfile[3] -
                                         self.uValuesScaledProfile[2]),
                         'U0W1':lambda x:self.uValuesScaledProfile[3] - x *
                                        (self.uValuesScaledProfile[3] -
                                         self.uValuesScaledProfile[2])
                         }
        block5 = self.interpolate_block(self.shape, curvesDict, block5Mapping)
        self.topo.blockList.append(block5)

        # ======= BLOCK 6 =================
        block6Mapping = {'V1W0':lambda x:self.uValuesScaledProfile[2] + x *
                                        (self.uValuesScaledProfile[1] -
                                         self.uValuesScaledProfile[2]),
                         'V1W1':lambda x:self.uValuesScaledProfile[2] + x *
                                        (self.uValuesScaledProfile[1] -
                                         self.uValuesScaledProfile[2]),
                         'V0W0':lambda x:self.uValuesProfile[2] + x *
                                        (self.uValuesProfile[1] -
                                         self.uValuesProfile[2]),
                         'V0W1':lambda x:self.uValuesProfile[2] + x *
                                        (self.uValuesProfile[1] -
                                         self.uValuesProfile[2]),
                         'U0W0':lambda x:-x,
                         'U0W1':lambda x:-x
                         }

        curvesDict = {'curveU0V0':self.qo7,
                      'curveU0V1':self.qo8,
                      'curveU0W0':self.edge_8_7_Bottom,
                      'curveU0W1':self.edge_8_7_Top,
                      'curveU1V0':self.qo13,
                      'curveU1V1':self.qo14,
                      'curveU1W0':self.edge_13_14_Bottom,
                      'curveU1W1':self.edge_13_14_Top,
                      'curveV0W0':self.profileBottom,
                      'curveV0W1':self.profileTop,
                      'curveV1W0':self.scaledProfileBottom,
                      'curveV1W1':self.scaledProfileTop
                      }

        block6 = self.interpolate_block(self.shape, curvesDict, block6Mapping)
        self.topo.blockList.append(block6)

        # ======= BLOCK 7 =================
        block7Mapping = {'V0W0':lambda x:self.uValuesScaledProfile[2] + x *
                                        (self.uValuesScaledProfile[1] -
                                         self.uValuesScaledProfile[2]),
                         'V0W1':lambda x:self.uValuesScaledProfile[2] + x *
                                        (self.uValuesScaledProfile[1] -
                                         self.uValuesScaledProfile[2]),
                         'V1W0':lambda x:self.uValuesCyclicUpper[0] + x *
                                        (self.uValuesCyclicUpper[1] -
                                         self.uValuesCyclicUpper[0]),
                         'V1W1':lambda x:self.uValuesCyclicUpper[0] + x *
                                        (self.uValuesCyclicUpper[1] -
                                         self.uValuesCyclicUpper[0])}

        curvesDict = {'curveU0V0':self.qo8,
                      'curveU0V1':self.qo9,
                      'curveU0W0':self.edge_8_9_Bottom,
                      'curveU0W1':self.edge_8_9_Top,
                      'curveU1V0':self.qo14,
                      'curveU1V1':self.qo15,
                      'curveU1W0':self.edge_14_15_Bottom,
                      'curveU1W1':self.edge_14_15_Top,
                      'curveV0W0':self.scaledProfileBottom,
                      'curveV0W1':self.scaledProfileTop,
                      'curveV1W0':self.cyclicUpperCurveBottom,
                      'curveV1W1':self.cyclicUpperCurveTop
                      }

        block7 = self.interpolate_block(self.shape, curvesDict, block7Mapping)
        self.topo.blockList.append(block7)



    def interpolate_block(self, shape, curvesDict, blockMapping,
                          gradingBlock=None):
        '''Interpolating a block of given shape with given curves and mapping'''

        block = np.zeros(shape)
        print 'interpolating %i points for hexahedral.' % (shape[0] *
                                                           shape[1] * shape[2])
        print 'This may take a while...'

        for i in range(shape[0]):
            for j in range(shape[1]):
                for k in range(shape[2]):
                    if gradingBlock == None:
                        u = float(i) / (shape[0] - 1.0)
                        v = float(j) / (shape[1] - 1.0)
                        w = float(k) / (shape[2] - 1.0)
                    else:
                        u, v, w = gradingBlock[i, j, k]
                    xyz = hexahedron(u, v, w, curvesDict['curveU0V0'],
                                     curvesDict['curveU0V1'],
                                     curvesDict['curveU0W0'],
                                     curvesDict['curveU0W1'],
                                     curvesDict['curveU1V0'],
                                     curvesDict['curveU1V1'],
                                     curvesDict['curveU1W0'],
                                     curvesDict['curveU1W1'],
                                     curvesDict['curveV0W0'],
                                     curvesDict['curveV0W1'],
                                     curvesDict['curveV1W0'],
                                     curvesDict['curveV1W1'], dim=3,
                                     mappingDictArg=blockMapping)
                    block[i, j, k, :] = xyz
        print 'done interpolating!'

        return block


    def create_connections(self):
        '''Creating all connections for this topology'''

        conn0 = create_connection()
        conn0['j+'] = [1, 'i+', 'k+']
        conn0['i+'] = [3, 'j+', 'k+']

        conn1 = create_connection()
        conn1['j-'] = [0, 'i+', 'k+']
        conn1['j+'] = [2, 'i+', 'k+']
        conn1['i+'] = [5, 'j+', 'k+']

        conn2 = create_connection()
        conn2['j-'] = [1, 'i+', 'k+']
        conn2['i+'] = [7, 'j+', 'k+']

        conn3 = create_connection()
        conn3['i-'] = [0, 'j+', 'k+']
        conn3['j+'] = [4, 'i+', 'k+']

        conn4 = create_connection()
        conn4['j-'] = [3, 'i+', 'k+']
        conn4['i-'] = [5, 'i+', 'k+']

        conn5 = create_connection()
        conn5['j-'] = [4, 'j+', 'k+']
        conn5['i-'] = [1, 'j+', 'k+']
        conn5['j+'] = [6, 'j-', 'k+']

        conn6 = create_connection()
        conn6['i-'] = [5, 'i-', 'k+']
        conn6['j+'] = [7, 'i+', 'k+']

        conn7 = create_connection()
        conn7['j-'] = [6, 'i+', 'k+']
        conn7['i-'] = [2, 'j+', 'k+']

        self.topo.blockConnections = [conn0, conn1, conn2, conn3, conn4, conn5,
                                      conn6, conn7]




if __name__ == '__main__':
    o10h = O10H()
    o10h()



