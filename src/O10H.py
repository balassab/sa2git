#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2013 by Balint Balassa                                  *
#*                                                                         *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************


import os
import numpy as np
#from multiprocessing import Pool

import turboMachine
from pythonnurbs import NurbsCurve

from transfinite import edge, interpolate_block, create_grading_block,\
                        interpolate_block_nurbs_surface

from test_bd import getPoint, transformCoordinates, create_nurbs_channel, \
                    create_central_cyclic_surface, create_s1_surface, \
                    rotate_cyclic_curves_from_center, \
                    rotate_cylcic_surface_from_center, \
                    get_rpz_from_normed_phi_m, \
                    interpolateNurbs3D, project_curve_onto_channel, \
                    setPoint, set_s1_tangent
from pythonnurbs2vtk import surface_points_2_vtk, control_points_2_vtk
from topo import Topo, create_connection
from foamMeshWriter import FoamMeshWriter
from cgnsWriter import CgnsWriter


class Layer(object):

    def __init__(self, span, bladeNumber):

        self.span = span
        self.bladeNumber = bladeNumber

        self.camberCurve = None
        self.bladeCurve = None
        self.channelCurve = None

        self.cyclicCenterCurve = None
        self.cyclicLowerCurve = None
        self.cyclicUpperCurve = None
        self.inletCurve = None
        self.outletCurve = None
        self.phiInletCenter = None
        self.phiOutletCenter = None

        self.s1Surface = None

        self.gradingQo = None



    def read_curves_from_surfaces(self, bladeSurface, camberSurface,
                                  channelSurface,
                                  centerCyclic, lowerCyclic, upperCyclic):
        self.bladeCurve = NurbsCurve.NurbsCurved()
        self.camberCurve = NurbsCurve.NurbsCurved()
        self.channelCurve = NurbsCurve.NurbsCurved()

        bladeSurface.isoCurveU(self.span, self.bladeCurve)
        camberSurface.isoCurveU(self.span, self.camberCurve)
        channelSurface.isoCurveU(self.span, self.channelCurve)

        self.set_center_cyclic(centerCyclic)
        self.set_lower_cyclic(lowerCyclic)
        self.set_upper_cyclic(upperCyclic)


    def set_phi_inlet(self):
        cyclicInletXyz = getPoint(self.cyclicCenterCurve.pointAt(0))
        phi = transformCoordinates(cyclicInletXyz, 'rpz')[1]
        self.phiInletCenter = phi


    def set_phi_outlet(self):
        cyclicOutletXyz = getPoint(self.cyclicCenterCurve.pointAt(1))
        phi = transformCoordinates(cyclicOutletXyz, 'rpz')[1]
        self.phiOutletCenter = phi


    def set_center_cyclic(self, cyclicSurface):
        self.cyclicCenterCurve = NurbsCurve.NurbsCurved()
        cyclicSurface.isoCurveU(self.span, self.cyclicCenterCurve)


    def set_lower_cyclic(self, lowerCyclicSurface):
        self.cyclicLowerCurve = NurbsCurve.NurbsCurved()
        lowerCyclicSurface.isoCurveU(self.span, self.cyclicLowerCurve)


    def set_upper_cyclic(self, upperCyclicSurface):
        self.cyclicUpperCurve = NurbsCurve.NurbsCurved()
        upperCyclicSurface.isoCurveU(self.span, self.cyclicUpperCurve)


    def create_s1_nurbs(self, numberOfBlades):
        s1 = create_s1_surface(self.cyclicUpperCurve, numberOfBlades)
        return s1


    def get_inlet(self):
        inletCurve = NurbsCurve.NurbsCurved()
        self.s1Surface.isoCurveV(0, inletCurve)
        return inletCurve


    def get_outlet(self):
        outletCurve = NurbsCurve.NurbsCurved()
        self.s1Surface.isoCurveV(1, outletCurve)
        return outletCurve




class O10HLayerRound(Layer):

    def __init__(self, span, bladeNumber):

        super(O10HLayerRound, self).__init__(span, bladeNumber)

        self.starLowerLE = None
        self.starLowerTE = None
        self.starUpperLE = None
        self.starUpperTE = None
        self.OTE = None
        self.oBlockUpperMid = None
        self.oBlockLowerMid = None
        self.point11u = None
        self.point14u = None
        self.inletDivision = []
        self.outletDivision = []
        self.cyclicUpperDivision = []
        self.cyclicLowerDivision = []
        self.profileDivision = range(6)

        self.points = np.zeros((29, 3))

        self.intRes = 4
        self.edge_1_5 = None
        self.edge_2_8 = None
        self.edge_4_5 = None
        self.edge_5_8 = None
        self.edge_5_6 = None
        self.edge_5_11_17 = None
        self.edge_7_8 = None
        self.edge_8_9 = None
        self.edge_8_14_22 = None
        self.edge_10_11 = None
        self.edge_11_12 = None
        self.edge_13_14 = None
        self.edge_13_19 = None
        self.edge_14_15 = None
        self.edge_16_17 = None
        self.edge_17_18 = None
        self.edge_17_20 = None
        self.edge_17_25 = None
        self.edge_19_20 = None
        self.edge_20_22 = None
        self.edge_20_26 = None
        self.edge_21_22 = None
        self.edge_22_23 = None
        self.edge_22_27 = None

        self.tan_1_5_1 = (0, 1)
        self.tan_1_5_5 = None
        self.tan_2_8_2 = (0, 1)
        self.tan_2_8_8 = None
        self.tan_4_5_4 = None
        self.tan_4_5_5 = None
        self.tan_5_8_5 = None
        self.tan_5_8_8 = None
        self.tan_5_11_5 = None
        self.tan_8_9_8 = None
        self.tan_8_9_9 = None
        self.tan_8_14_8 = None
        self.tan_10_11_10 = None
        self.tan_10_11_11 = None
        self.tan_11_17_17 = None
        self.tan_14_15_14 = None
        self.tan_14_15_15 = None
        self.tan_14_20_20 = None
        self.tan_16_17_16 = None
        self.tan_16_17_17 = None
        self.tan_17_20_17 = None
        self.tan_17_20_20 = None
        self.tan_17_25_25 = (0, 1)
        self.tan_22_27_27 = (0, 1)
        
        # new topo:
        self.tan_17_25_17 = None
        self.tan_19_20_19 = None
        self.tan_20_22_20 = None
        self.tan_20_22_22 = None
        self.tan_20_26_20 = None
        self.tan_20_26_26 = None
        self.tan_22_23_22 = None
        self.tan_22_23_23 = None
        self.tan_22_27_22 = None
        self.tan_22_27_27 = None
        
        
        self.grading_1_5 = None
        self.grading_2_8 = None
        self.grading_4_5 = None
        self.grading_5_8 = None
        self.grading_5_6 = None
        self.grading_5_11 = None
        self.grading_7_8 = None
        self.grading_8_9 = None
        self.grading_8_14 = None
        self.grading_10_11 = None
        self.grading_11_12 = None
        self.grading_11_17 = None
        self.grading_13_14 = None
        self.grading_13_19 = None
        self.grading_14_15 = None
        self.grading_14_20 = None
        self.grading_16_17 = None
        self.grading_17_18 = None
        self.grading_17_20 = None
        self.grading_17_25 = None
        self.grading_19_20 = None
        self.grading_19_21 = None
        self.grading_20_22 = None
        self.grading_20_26 = None
        self.grading_21_22 = None
        self.grading_22_27 = None
        self.grading_22_23 = None
        self.gradingCyclicLower = [None] * 4
        self.gradingCyclicUpper = [None] * 4
        self.gradingBlade_4 = None
        self.gradingBlade_5 = None
        self.gradingBlade_6 = None
        self.gradingBlade_9 = None
        self.gradingBlade_10 = None
        self.gradingBlade_11 = None
        self.gradingBlade_12 = None


        # Nurbs++ Interpolation parameters:
        self.error = 1e-4
        self.s = 1
        self.sep = 9
        self.maxIter = 10
        self.um = 0
        self.uM = 1


    def __call__(self):
        self.set_edges()


    def set_points(self):
        p1Npp = self.inletCurve.pointAt(self.inletDivision[1])
        self.points[1] = getPoint(p1Npp)

        p2Npp = self.inletCurve.pointAt(self.inletDivision[0])
        self.points[2] = getPoint(p2Npp)

        p3Npp = self.inletCurve.pointAt(0)
        self.points[3] = getPoint(p3Npp)

        p4Npp = self.cyclicLowerCurve.pointAt(self.cyclicLowerDivision[0])
        self.points[4] = getPoint(p4Npp)

        p5Rpz = get_rpz_from_normed_phi_m(self.starLowerLE[0],
                                           self.starLowerLE[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[5] = transformCoordinates(p5Rpz, 'xyz')

        p8Rpz = get_rpz_from_normed_phi_m(self.starUpperLE[0],
                                           self.starUpperLE[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[8] = transformCoordinates(p8Rpz, 'xyz')

        p9Npp = self.cyclicUpperCurve.pointAt(self.cyclicUpperDivision[0])
        self.points[9] = getPoint(p9Npp)

        p10Npp = self.cyclicLowerCurve.pointAt(self.cyclicLowerDivision[1])
        self.points[10] = getPoint(p10Npp)

        p11Rpz = get_rpz_from_normed_phi_m(self.oBlockLowerMid[0],
                                           self.oBlockLowerMid[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[11] = transformCoordinates(p11Rpz, 'xyz')

        p14Rpz = get_rpz_from_normed_phi_m(self.oBlockUpperMid[0],
                                           self.oBlockUpperMid[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[14] = transformCoordinates(p14Rpz, 'xyz')

        p15Npp = self.cyclicUpperCurve.pointAt(self.cyclicUpperDivision[1])
        self.points[15] = getPoint(p15Npp)

        p16Npp = self.cyclicLowerCurve.pointAt(self.cyclicLowerDivision[2])
        self.points[16] = getPoint(p16Npp)

        p17Rpz = get_rpz_from_normed_phi_m(self.starLowerTE[0],
                                           self.starLowerTE[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[17] = transformCoordinates(p17Rpz, 'xyz')

        p20Rpz = get_rpz_from_normed_phi_m(self.OTE[0],
                                           self.OTE[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[20] = transformCoordinates(p20Rpz, 'xyz')

        p22Rpz = get_rpz_from_normed_phi_m(self.starUpperTE[0],
                                           self.starUpperTE[1],
                                           self.channelCurve,
                                           self.cyclicCenterCurve,
                                           self.bladeNumber)
        self.points[22] = transformCoordinates(p22Rpz, 'xyz')

        p23Npp = self.cyclicUpperCurve.pointAt(self.cyclicUpperDivision[2])
        self.points[23] = getPoint(p23Npp)

        p25Npp = self.outletCurve.pointAt(self.outletDivision[2])
        self.points[25] = getPoint(p25Npp)
        
        p26Npp = self.outletCurve.pointAt(self.outletDivision[1])
        self.points[26] = getPoint(p26Npp)
        
        p27Npp = self.outletCurve.pointAt(self.outletDivision[0])
        self.points[27] = getPoint(p27Npp)


        uClosest = NurbsCurve.doublep()
        # Setting point 6 on blade curve:
        if self.profileDivision[3] == None:
            uClosest.assign(0.6)
            self.bladeCurve.minDist2(setPoint(self.points[5]), uClosest,
                                          self.error, self.s, self.sep,
                                          self.maxIter, self.um, self.uM)
            self.profileDivision[3] = uClosest.value()
        p6Npp = self.bladeCurve.pointAt(self.profileDivision[3])
        self.points[6] = getPoint(p6Npp)

        # Setting point 7 on blade curve:
        if self.profileDivision[2] == None:
            uClosest.assign(0.3)
            self.bladeCurve.minDist2(setPoint(self.points[8]), uClosest,
                                          self.error, self.s, self.sep,
                                          self.maxIter, self.um, self.uM)
            self.profileDivision[2] = uClosest.value()
        p7Npp = self.bladeCurve.pointAt(self.profileDivision[2])
        self.points[7] = getPoint(p7Npp)

        # Setting point 12 on blade curve:
        if self.profileDivision[4] == None:
            uClosest.assign(0.7)
            self.bladeCurve.minDist2(setPoint(self.points[11]), uClosest,
                                          self.error, self.s, self.sep,
                                          self.maxIter, self.um, self.uM)
            self.profileDivision[4] = uClosest.value()
        p12Npp = self.bladeCurve.pointAt(self.profileDivision[4])
        self.points[12] = getPoint(p12Npp)

        # Setting point 13 on blade curve:
        if self.profileDivision[1] == None:
            uClosest.assign(0.2)
            self.bladeCurve.minDist2(setPoint(self.points[14]), uClosest,
                                          self.error, self.s, self.sep,
                                          self.maxIter, self.um, self.uM)
            self.profileDivision[1] = uClosest.value()
        p13Npp = self.bladeCurve.pointAt(self.profileDivision[1])
        self.points[13] = getPoint(p13Npp)

        # Setting point 18 on blade curve:
        if self.profileDivision[5] == None:
            uClosest.assign(0.8)
            self.bladeCurve.minDist2(setPoint(self.points[17]), uClosest,
                                          self.error, 0.3, self.sep,
                                          20, 0.5, 1)
            self.profileDivision[5] = uClosest.value()
        p18Npp = self.bladeCurve.pointAt(self.profileDivision[5])
        self.points[18] = getPoint(p18Npp)
        
        # Setting point 19 on blade curve:
        p19Npp = self.bladeCurve.pointAt(0)
        self.points[19] = getPoint(p19Npp)

        # Setting point 21 on blade curve:
        if self.profileDivision[0] == None:
            uClosest.assign(0.05)
            self.bladeCurve.minDist2(setPoint(self.points[22]), uClosest,
                                          self.error, 0.3, self.sep,
                                          20, 0, 0.5)
            #bladePoint = self.bladeCurve.pointAt(uClosest.value())
            self.profileDivision[0] = uClosest.value()
        p21Npp = self.bladeCurve.pointAt(self.profileDivision[0])
        self.points[21] = getPoint(p21Npp)



    def set_edges(self):
        if self.s1Surface == None:
            self.s1Surface = self.create_s1_nurbs(self.bladeNumber)
        self.inletCurve = self.get_inlet()
        self.outletCurve = self.get_outlet()

        if (self.points != 0).sum() == 0:
            self.set_points()

        self.set_edge_1_5()
        self.set_edge_2_8()
        self.set_edge_4_5()
        self.set_edge_5_8()
        self.set_edge_5_11_17()
        self.set_edge_5_6()
        self.set_edge_7_8()
        self.set_edge_8_9()
        self.set_edge_8_14_22()
        self.set_edge_10_11()
        self.set_edge_11_12()
        self.set_edge_13_14()
        self.set_edge_13_19()
        self.set_edge_14_15()
        self.set_edge_16_17()
        self.set_edge_17_18()
        self.set_edge_17_20()
        self.set_edge_17_25()
        self.set_edge_19_20()
        self.set_edge_20_22()
        self.set_edge_20_26()
        self.set_edge_21_22()
        self.set_edge_22_23()
        self.set_edge_22_27()


    def set_edge_1_5(self):
        start = self.points[1]
        end = self.points[5]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_1_5_1 != None:
            if len(self.tan_1_5_1) == 2:
                dPhi, dM = self.tan_1_5_1
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_1_5_1)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_1_5_5 != None:
            if len(self.tan_1_5_5) == 2:
                dPhi, dM = self.tan_1_5_5
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_1_5_5)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_1_5 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_2_8(self):
        start = self.points[2]
        end = self.points[8]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_2_8_2 != None:
            if len(self.tan_2_8_2) == 2:
                dPhi, dM = self.tan_2_8_2
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_2_8_2)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_2_8_8 != None:
            if len(self.tan_2_8_8) == 2:
                dPhi, dM = self.tan_2_8_8
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_2_8_8)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_2_8 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_4_5(self):
        start = self.points[4]
        end = self.points[5]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_4_5_4 != None:
            if len(self.tan_4_5_4) == 2:
                dPhi, dM = self.tan_4_5_4
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_4_5_4)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_4_5_5 != None:
            if len(self.tan_4_5_5) == 2:
                dPhi, dM = self.tan_4_5_5
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_4_5_5)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_4_5 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_5_6(self):
        start = self.points[5]
        end = self.points[6]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_5_6 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_5_8(self):
        start = self.points[5]
        end = self.points[8]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_5_8_5 != None:
            if len(self.tan_5_8_5) == 2:
                dPhi, dM = self.tan_5_8_5
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_5_8_5)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_5_8_8 != None:
            if len(self.tan_5_8_8) == 2:
                dPhi, dM = self.tan_5_8_8
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_5_8_8)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_5_8 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_5_11_17(self):
        start = self.points[5]
        center = self.points[11]
        end = self.points[17]
        pArray = np.zeros((5, 3))
        pArray[0] = start
        pArray[1] = (start + center) / 2.0
        pArray[2] = center
        pArray[3] = (end + center) / 2.0
        pArray[4] = end
        tmpEdge = interpolateNurbs3D(pArray)
        if self.tan_5_11_5 != None:
            if len(self.tan_5_11_5) == 2:
                dPhi, dM = self.tan_5_11_5
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_5_11_5)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_11_17_17 != None:
            if len(self.tan_11_17_17) == 2:
                dPhi, dM = self.tan_11_17_17
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_11_17_17)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_5_11_17 = project_curve_onto_channel(tmpEdge,
                                            self.channelCurve, sampleRate=10)
        u = NurbsCurve.doublep()
        u.assign(0.5)
        p11Npp = setPoint(center)
        self.edge_5_11_17.minDist2(p11Npp, u, self.error, self.s, self.sep,
                                      self.maxIter, self.um, self.uM)
        self.point11u = u.value()

    def set_edge_7_8(self):
        start = self.points[7]
        end = self.points[8]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_7_8 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_8_9(self):
        start = self.points[8]
        end = self.points[9]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_8_9_8 != None:
            if len(self.tan_8_9_8) == 2:
                dPhi, dM = self.tan_8_9_8
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_8_9_8)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_8_9_9 != None:
            if len(self.tan_8_9_9) == 2:
                dPhi, dM = self.tan_8_9_9
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_8_9_9)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_8_9 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_8_14_22(self):
        start = self.points[8]
        center = self.points[14]
        end = self.points[22]
        pArray = np.zeros((5, 3))
        pArray[0] = start
        pArray[1] = (start + center) / 2.0
        pArray[2] = center
        pArray[3] = (end + center) / 2.0
        pArray[4] = end
        tmpEdge = interpolateNurbs3D(pArray)
        if self.tan_8_14_8 != None:
            if len(self.tan_8_14_8) == 2:
                dPhi, dM = self.tan_8_14_8
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_8_14_8)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_14_22_22 != None:
            if len(self.tan_14_22_22) == 2:
                dPhi, dM = self.tan_14_22_22
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_14_22_22)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_8_14_22 = project_curve_onto_channel(tmpEdge,
                                            self.channelCurve, sampleRate=10)
        u = NurbsCurve.doublep()
        u.assign(0.5)
        p14Npp = setPoint(center)
        self.edge_8_14_22.minDist2(p14Npp, u, self.error, self.s, self.sep,
                                      self.maxIter, self.um, self.uM)
        self.point14u = u.value()

    def set_edge_10_11(self):
        start = self.points[10]
        end = self.points[11]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_10_11_10 != None:
            if len(self.tan_10_11_10) == 2:
                dPhi, dM = self.tan_10_11_10
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_10_11_10)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_10_11_11 != None:
            if len(self.tan_10_11_11) == 2:
                dPhi, dM = self.tan_10_11_11
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_10_11_11)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_10_11 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_11_12(self):
        start = self.points[11]
        end = self.points[12]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_11_12 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_13_14(self):
        start = self.points[13]
        end = self.points[14]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_13_14 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_13_19(self):
        start = self.points[13]
        end = self.points[19]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_13_19 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_14_15(self):
        start = self.points[14]
        end = self.points[15]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_14_15_14 != None:
            if len(self.tan_14_15_14) == 2:
                dPhi, dM = self.tan_14_15_14
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_14_15_14)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_14_15_15 != None:
            if len(self.tan_14_15_15) == 2:
                dPhi, dM = self.tan_14_15_15
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_14_15_15)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_14_15 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_16_17(self):
        start = self.points[16]
        end = self.points[17]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_16_17_16 != None:
            if len(self.tan_16_17_16) == 2:
                dPhi, dM = self.tan_16_17_16
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_16_17_16)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_16_17_17 != None:
            if len(self.tan_16_17_17) == 2:
                dPhi, dM = self.tan_16_17_17
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_16_17_17)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_16_17 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_17_18(self):
        start = self.points[17]
        end = self.points[18]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_17_18 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5,
                                                   ignore=[-1])


    def set_edge_17_20(self):
        start = self.points[17]
        end = self.points[20]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_17_20_17 != None:
            if len(self.tan_17_20_17) == 2:
                dPhi, dM = self.tan_17_20_17
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_17_20_17)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_17_20_20 != None:
            if len(self.tan_17_20_20) == 2:
                dPhi, dM = self.tan_17_20_20
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_17_20_20)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_17_20 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_17_25(self):
        start = self.points[17]
        end = self.points[25]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_17_25_17 != None:
            if len(self.tan_17_25_17) == 2:
                dPhi, dM = self.tan_17_25_17
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_17_25_17)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_17_25_25 != None:
            if len(self.tan_17_25_25) == 2:
                dPhi, dM = self.tan_17_25_25
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_17_25_25)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_17_25 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_19_20(self):
        start = self.points[19]
        end = self.points[20]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_19_20 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                     sampleRate=5,
                                                     ignore=[0])

    def set_edge_20_22(self):
        start = self.points[20]
        end = self.points[22]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_20_22_20 != None:
            if len(self.tan_20_22_20) == 2:
                dPhi, dM = self.tan_20_22_20
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_20_22_20)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_20_22_22 != None:
            if len(self.tan_20_22_22) == 2:
                dPhi, dM = self.tan_20_22_22
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_20_22_22)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_20_22 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_20_26(self):
        start = self.points[20]
        end = self.points[26]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_20_26_20 != None:
            if len(self.tan_20_26_20) == 2:
                dPhi, dM = self.tan_20_26_20
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_20_26_20)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_20_26_26 != None:
            if len(self.tan_20_26_26) == 2:
                dPhi, dM = self.tan_20_26_26
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_20_26_26)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_20_26 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_21_22(self):
        start = self.points[21]
        end = self.points[22]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_21_22 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                     sampleRate=5,
                                                     ignore=[0])

    def set_edge_22_23(self):
        start = self.points[22]
        end = self.points[23]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_22_23_22 != None:
            if len(self.tan_22_23_22) == 2:
                dPhi, dM = self.tan_22_23_22
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_22_23_22)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_22_23_23 != None:
            if len(self.tan_22_23_23) == 2:
                dPhi, dM = self.tan_22_23_23
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_22_23_23)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_22_23 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)

    def set_edge_22_27(self):
        start = self.points[22]
        end = self.points[27]
        tmpEdge = edge(start, end, self.intRes)
        if self.tan_22_27_22 != None:
            if len(self.tan_22_27_22) == 2:
                dPhi, dM = self.tan_22_27_22
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(0, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_22_27_22)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(0, setPoint(normTangent))
        if self.tan_22_27_27 != None:
            if len(self.tan_22_27_27) == 2:
                dPhi, dM = self.tan_22_27_27
                tangent = set_s1_tangent(start, dPhi, dM, self.channelCurve)
                normTangent = tangent / np.linalg.norm(tangent)
                tmpEdge.setTangent(1, setPoint(normTangent))
            else:
                npTan = np.array(self.tan_22_27_27)
                normTangent = npTan / np.linalg.norm(npTan)
                tmpEdge.setTangent(1, setPoint(normTangent))
        self.edge_22_27 = project_curve_onto_channel(tmpEdge, self.channelCurve,
                                                   sampleRate=5)



class O10H():

    def __init__(self):
        
        self.writeDir = None
        self.blocksPerLayer = 18
        self.processors = 2
        self.relaxLE = None
        self.relaxTE = None
        self.inletBlockNormLength = 0
        self.outletBlockNormLength = 0
        self.inletUvalues = []
        self.outletUvalues = []
        self.cyclicLowerSurfaceNurbs = None
        self.cyclicUpperSurfaceNurbs = None
        self.cyclicCenterSurfaceNurbs = None
        self.bladeSurfaceNurbs = None
        self.camberSurfaceNurbs = None
        self.hubSurfaceNurbs = None
        self.shroudSurfaceNurbs = None
        self.channelSurfaceNurbs = None
        self.LETEaligned = False
        self.interpolationSpans = [0, 0.25, 0.5, 0.75, 1]
        self.spans = []
        self.layers = []
        self.bladeNumber = None

        self.topo = Topo()

        self.cellsMer0 = 1
        self.cellsMer1 = 1
        self.cellsMer2 = 1
        self.cellsMer3 = 1
        self.cellsCirc0 = 1
        self.cellsCirc1 = 1
        self.cellsCirc2 = 1
        self.cellsRad = 1
        self.cellsOBlock = 1

        self.qo0 = None
        self.qo1 = None
        self.qo2 = None
        self.qo3 = None
        self.qo4 = None
        self.qo5 = None
        self.qo6 = None
        self.qo7 = None
        self.qo8 = None
        self.qo9 = None
        self.qo10 = None
        self.qo11 = None
        self.qo12 = None
        self.qo13 = None
        self.qo14 = None
        self.qo15 = None
        self.qo16 = None
        self.qo17 = None
        self.qo18 = None
        self.qo19 = None
        self.qo20 = None
        self.qo21 = None
        self.qo22 = None
        self.qo23 = None
        self.qo24 = None
        self.qo25 = None




    def __call__(self, cgns=False):
        #self.create_layers()
        if self.inletBlockNormLength > 0:
            self.blocksPerLayer += 3
        if self.outletBlockNormLength > 0:
            self.blocksPerLayer += 3

        #self.set_cyclic_center()
        #self.set_cyclic_sides()

        self.set_QOs()

        self.set_blocks()
        for layerIndex in range(len(self.spans) - 1):
            self.set_connections(layerIndex)
        self.topo.check_connectivity()


        if not cgns:
            self.write_O10H()
        else:
            self.write_cgns()


    def write_O10H(self):
        '''Writing the O10H Topology with the foamMeshWriter'''

        f = FoamMeshWriter()
        f.topo = self.topo
        if self.writeDir == None:
            f.case = '../O10H'
        else:
            f.case = self.writeDir
        f()
        print '\nFinished!'
        
    
    def write_cgns(self):
        c = CgnsWriter(self.topo)
        c.write_cgns_file()


    def set_QOs(self):
        self.set_qo0()
        self.set_qo1()
        self.set_qo2()
        self.set_qo3()
        self.set_qo4()
        self.set_qo5()
        self.set_qo6()
        self.set_qo7()
        self.set_qo8()
        self.set_qo9()
        self.set_qo10()
        self.set_qo11()
        self.set_qo12()
        self.set_qo13()
        self.set_qo14()
        self.set_qo15()
        self.set_qo16()
        self.set_qo17()
        self.set_qo18()
        self.set_qo19()
        self.set_qo20()
        self.set_qo21()
        self.set_qo22()
        self.set_qo23()
        self.set_qo24()
        self.set_qo25()
        self.set_qo26()
        self.set_qo27()
        self.set_qo28()


    def set_qo0(self):
        self.qo0 = NurbsCurve.NurbsCurved()
        self.cyclicLowerSurfaceNurbs.isoCurveV(0, self.qo0)


    def set_qo1(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.inletCurve.pointAt(layer.inletDivision[1])
            pArray[i] = getPoint(pointNpp)
        self.qo1 = interpolateNurbs3D(pArray)


    def set_qo2(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.inletCurve.pointAt(layer.inletDivision[0])
            pArray[i] = getPoint(pointNpp)
        self.qo2 = interpolateNurbs3D(pArray)

    def set_qo3(self):
        self.qo3 = NurbsCurve.NurbsCurved()
        self.cyclicUpperSurfaceNurbs.isoCurveV(0, self.qo3)

    def set_qo4(self):
        self.qo4 = NurbsCurve.NurbsCurved()
        u = self.layers[0].cyclicLowerDivision[0]
        self.cyclicLowerSurfaceNurbs.isoCurveV(u, self.qo4)

    def set_qo5(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            phiNorm, m = layer.starLowerLE
            pointRpz = get_rpz_from_normed_phi_m(phiNorm, m, layer.channelCurve,
                                                 layer.cyclicCenterCurve,
                                                 self.bladeNumber)
            pArray[i] = transformCoordinates(pointRpz, 'xyz')
        self.qo5 = interpolateNurbs3D(pArray)

    def set_qo6(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[6]
        self.qo6 = interpolateNurbs3D(pArray)

    def set_qo7(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[7]
        self.qo7 = interpolateNurbs3D(pArray)

    def set_qo8(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            phiNorm, m = layer.starUpperLE
            pointRpz = get_rpz_from_normed_phi_m(phiNorm, m, layer.channelCurve,
                                                 layer.cyclicCenterCurve,
                                                 self.bladeNumber)
            pArray[i] = transformCoordinates(pointRpz, 'xyz')
        self.qo8 = interpolateNurbs3D(pArray)

    def set_qo9(self):
        self.qo9 = NurbsCurve.NurbsCurved()
        u = self.layers[0].cyclicUpperDivision[0]
        self.cyclicUpperSurfaceNurbs.isoCurveV(u, self.qo9)

    def set_qo10(self):
        self.qo10 = NurbsCurve.NurbsCurved()
        u = self.layers[0].cyclicLowerDivision[1]
        self.cyclicLowerSurfaceNurbs.isoCurveV(u, self.qo10)

    def set_qo11(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            phiNorm, m = layer.oBlockLowerMid
            pointRpz = get_rpz_from_normed_phi_m(phiNorm, m, layer.channelCurve,
                                                 layer.cyclicCenterCurve,
                                                 self.bladeNumber)
            pArray[i] = transformCoordinates(pointRpz, 'xyz')
        self.qo11 = interpolateNurbs3D(pArray)

    def set_qo12(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[12]
        self.qo12 = interpolateNurbs3D(pArray)

    def set_qo13(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[13]
        self.qo13 = interpolateNurbs3D(pArray)

    def set_qo14(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[14]
        self.qo14 = interpolateNurbs3D(pArray)

    def set_qo15(self):
        self.qo15 = NurbsCurve.NurbsCurved()
        u = self.layers[0].cyclicUpperDivision[1]
        self.cyclicLowerSurfaceNurbs.isoCurveV(u, self.qo15)

    def set_qo16(self):
        self.qo16 = NurbsCurve.NurbsCurved()
        u = self.layers[0].cyclicLowerDivision[2]
        self.cyclicLowerSurfaceNurbs.isoCurveV(u, self.qo16)

    def set_qo17(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            phiNorm, m = layer.starLowerTE
            pointRpz = get_rpz_from_normed_phi_m(phiNorm, m, layer.channelCurve,
                                                 layer.cyclicCenterCurve,
                                                 self.bladeNumber)
            pArray[i] = transformCoordinates(pointRpz, 'xyz')
        self.qo17 = interpolateNurbs3D(pArray)

    def set_qo18(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[18]
        self.qo18 = interpolateNurbs3D(pArray)

    def set_qo19(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[19]
        self.qo19 = interpolateNurbs3D(pArray)
        
    def set_qo20(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[20]
        self.qo20 = interpolateNurbs3D(pArray)
        
    def set_qo21(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pArray[i] = layer.points[21]
        self.qo21 = interpolateNurbs3D(pArray)

    def set_qo22(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            phiNorm, m = layer.starUpperTE
            pointRpz = get_rpz_from_normed_phi_m(phiNorm, m, layer.channelCurve,
                                                 layer.cyclicCenterCurve,
                                                 self.bladeNumber)
            pArray[i] = transformCoordinates(pointRpz, 'xyz')
        self.qo22 = interpolateNurbs3D(pArray)

    def set_qo23(self):
        self.qo23 = NurbsCurve.NurbsCurved()
        u = self.layers[0].cyclicUpperDivision[2]
        self.cyclicLowerSurfaceNurbs.isoCurveV(u, self.qo23)

    def set_qo24(self):
        self.qo24 = NurbsCurve.NurbsCurved()
        self.cyclicLowerSurfaceNurbs.isoCurveV(1, self.qo24)

    def set_qo25(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.outletCurve.pointAt(layer.outletDivision[2])
            pArray[i] = getPoint(pointNpp)
        self.qo25 = interpolateNurbs3D(pArray)

    def set_qo26(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.outletCurve.pointAt(layer.outletDivision[1])
            pArray[i] = getPoint(pointNpp)
        self.qo26 = interpolateNurbs3D(pArray)

    def set_qo27(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.outletCurve.pointAt(layer.outletDivision[0])
            pArray[i] = getPoint(pointNpp)
        self.qo27 = interpolateNurbs3D(pArray)

    def set_qo28(self):
        self.qo28 = NurbsCurve.NurbsCurved()
        self.cyclicUpperSurfaceNurbs.isoCurveV(1, self.qo28)

    def create_layers(self):
        for span in self.spans:
            self.layers.append(O10HLayerRound(span, self.bladeNumber))
        self.relaxLE = [0] * len(self.spans)
        self.relaxTE = [0] * len(self.spans)


    def get_nurbs_from_bd(self, bladeRow):
        self.bladeNumber = bladeRow.numberBlades
        self.bladeSurfaceNurbs = bladeRow.bladeSurface
        bladeRow.calculate_camber_surface()
        self.camberSurfaceNurbs = bladeRow.camberSurface
        self.channelSurfaceNurbs = create_nurbs_channel(bladeRow)


    def mesh(self):
        pass


    def set_cyclic_center(self):
        if self.cyclicCenterSurfaceNurbs == None:
            ccs = create_central_cyclic_surface(self.camberSurfaceNurbs,
                                                self.channelSurfaceNurbs,
                                                self.interpolationSpans,
                                                relaxLE=self.relaxLE,
                                                relaxTE=self.relaxTE)
            self.cyclicCenterSurfaceNurbs = ccs


    def set_cyclic_sides(self):
        ls, us = rotate_cylcic_surface_from_center(\
                                self.cyclicCenterSurfaceNurbs, self.bladeNumber)
        self.cyclicLowerSurfaceNurbs, self.cyclicUpperSurfaceNurbs = ls, us


    def set_connections(self, layerIndex):

        maxIndex = len(self.spans) - 1
        blocksPerLayer = len(self.topo.blockList) / maxIndex
        conn0 = create_connection()
        conn0['j+'] = [1 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn0['i+'] = [3 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn0 = self.set_qo_connection(conn0, 0, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn0)

        conn1 = create_connection()
        conn1['j-'] = [0 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn1['j+'] = [2 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn1['i+'] = [5 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn1 = self.set_qo_connection(conn1, 1, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn1)

        conn2 = create_connection()
        conn2['j-'] = [1 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn2['i+'] = [7 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn2 = self.set_qo_connection(conn2, 2, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn2)

        conn3 = create_connection()
        conn3['i-'] = [0 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn3['i+'] = [8 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn3['j+'] = [4 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn3 = self.set_qo_connection(conn3, 3, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn3)

        conn4 = create_connection()
        conn4['j-'] = [3 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn4['i-'] = [5 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn4['i+'] = [9 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn4['j+'] = 'blade'
        conn4 = self.set_qo_connection(conn4, 4, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn4)

        conn5 = create_connection()
        conn5['j-'] = [4 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn5['i-'] = [1 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn5['j+'] = [6 + layerIndex * blocksPerLayer, 'j-', 'k+']
        conn5['i+'] = 'blade'
        conn5 = self.set_qo_connection(conn5, 5, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn5)

        conn6 = create_connection()
        conn6['j+'] = [7 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn6['i-'] = [5 + layerIndex * blocksPerLayer, 'i-', 'k+']
        conn6['i+'] = [12 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn6['j-'] = 'blade'
        conn6 = self.set_qo_connection(conn6, 6, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn6)

        conn7 = create_connection()
        conn7['j-'] = [6 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn7['i-'] = [2 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn7['i+'] = [13 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn7 = self.set_qo_connection(conn7, 7, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn7)

        conn8 = create_connection()
        conn8['j+'] = [9 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn8['i-'] = [3 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn8['i+'] = [14 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn8 = self.set_qo_connection(conn8, 8, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn8)

        conn9 = create_connection()
        conn9['j-'] = [8 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn9['i+'] = [10 + layerIndex * blocksPerLayer, 'i-', 'k+']
        conn9['i-'] = [4 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn9['j+'] = 'blade'
        conn9 = self.set_qo_connection(conn9, 9, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn9)

        conn10 = create_connection()
        conn10['j-'] = [9 + layerIndex * blocksPerLayer, 'j-', 'k+']
        conn10['j+'] = [11 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn10['i+'] = [15 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn10['i-'] = 'blade'
        conn10 = self.set_qo_connection(conn10, 10, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn10)
        
        conn11 = create_connection()
        conn11['j-'] = [10 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn11['j+'] = [12 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn11['i+'] = [16 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn11['i-'] = 'blade'
        conn11 = self.set_qo_connection(conn11, 11, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn11)

        conn12 = create_connection()
        conn12['i-'] = [6 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn12['i+'] = [11 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn12['j+'] = [13 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn12['j-'] = 'blade'
        conn12 = self.set_qo_connection(conn12, 12, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn12)

        conn13 = create_connection()
        conn13['i-'] = [7 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn13['i+'] = [17 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn13['j-'] = [12 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn13 = self.set_qo_connection(conn13, 13, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn13)

        conn14 = create_connection()
        conn14['i-'] = [8 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn14['j+'] = [15 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn14 = self.set_qo_connection(conn14, 14, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn14)

        conn15 = create_connection()
        conn15['i-'] = [10 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn15['j-'] = [14 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn15['j+'] = [16 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn15 = self.set_qo_connection(conn15, 15, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn15)
        
        conn16 = create_connection()
        conn16['i-'] = [11 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn16['j-'] = [15 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn16['j+'] = [17 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn16 = self.set_qo_connection(conn16, 16, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn16)

        conn17 = create_connection()
        conn17['i-'] = [13 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn17['j-'] = [16 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn17 = self.set_qo_connection(conn17, 17, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn17)


    def set_qo_connection(self, conn, blockIndex, layerIndex,
                          blocksPerLayer, maxIndex):
        if maxIndex > 1:
            if layerIndex < maxIndex - 1:
                conn['k+'] = [blockIndex + (layerIndex + 1) * blocksPerLayer,
                              'i+', 'j+']
            if 0 < layerIndex < maxIndex:
                conn['k-'] = [blockIndex + (layerIndex - 1) * blocksPerLayer,
                              'i+', 'j+']
        return conn


    def set_blocks(self):
        self.topo.blockList = (len(self.layers) - 1) * self.blocksPerLayer * \
                                [None]
        for layerIndex in  range(len(self.layers) - 1):
            self.set_blocks_of_layer(layerIndex)
        #pool = Pool(processes=self.processors)
        #pool.map(self.set_blocks_of_layer, range(len(self.layers) - 1))

    def set_blocks_of_layer(self, layerIndex):
        if self.inletBlockNormLength > 0:
            cellsCirc = self.cellsCirc0 + self.cellsCirc1 + self.cellsCirc2
            if layerIndex == 0:
                self.prepare_nurbs_for_inlet_block_creation(self.cellsCirc0,
                                                            self.cellsCirc1,
                                                            self.cellsCirc2)
            self.set_block_inlet_lower(layerIndex)
            self.set_block_inlet_center(layerIndex)
            self.set_block_inlet_upper(layerIndex)
        if self.outletBlockNormLength > 0:
            cellsCirc = self.cellsCirc0 + self.cellsCirc1 + self.cellsCirc2
            if layerIndex == 0:
                self.prepare_nurbs_for_outlet_block_creation(self.cellsCirc0,
                                                            self.cellsCirc1,
                                                            self.cellsCirc2)
            self.set_block_outlet_lower(layerIndex)
            self.set_block_outlet_center(layerIndex)
            self.set_block_outlet_upper(layerIndex)
        self.set_block_0(layerIndex)
        self.set_block_1(layerIndex)
        self.set_block_2(layerIndex)
        self.set_block_3(layerIndex)
        self.set_block_4_nurbs(layerIndex)
        #self.set_block_4(layerIndex)
        self.set_block_5_nurbs(layerIndex)
        #self.set_block_5(layerIndex)
        #self.set_block_6(layerIndex)
        self.set_block_6_nurbs(layerIndex)
        self.set_block_7(layerIndex)
        self.set_block_8(layerIndex)
        #self.set_block_9(layerIndex)
        self.set_block_9_nurbs(layerIndex)
        #self.set_block_10(layerIndex)
        self.set_block_10_nurbs(layerIndex)
        #self.set_block_11(layerIndex)
        self.set_block_11_nurbs(layerIndex)
        #self.set_block_12(layerIndex)
        self.set_block_12_nurbs(layerIndex)
        self.set_block_13(layerIndex)
        self.set_block_14(layerIndex)
        self.set_block_15(layerIndex)
        self.set_block_16(layerIndex)
        self.set_block_17(layerIndex)

    def prepare_nurbs_for_inlet_block_creation(self, cellsCirc0, cellsCirc1,
                                               cellsCirc2):
        cellsCirc = self.cellsCirc0 + self.cellsCirc1 + self.cellsCirc2
        for layerIndex in range(len(self.layers)):
            # TODO: all this stuff...
            pass

    def set_block_inlet_lower(self, layerIndex):
        self.layers[layerIndex].set_phi_inlet()
        phiInlet = self.layers[layerIndex].phiInletCenter
        # TODO: all this stuff...

    def set_block_inlet_center(self, layerIndex):
        phiInlet = self.layers[layerIndex].phiInletCenter
        # TODO: all this stuff...

    def set_block_inlet_upper(self, layerIndex):
        phiInlet = self.layers[layerIndex].phiInletCenter
        # TODO: all this stuff...

    def prepare_nurbs_for_outlet_block_creation(self, cellsCirc0, cellsCirc1,
                                               cellsCirc2):
        cellsCirc = self.cellsCirc0 + self.cellsCirc1 + self.cellsCirc2
        for layerIndex in range(len(self.layers)):
            # TODO: all this stuff...
            pass

    def set_block_outlet_lower(self, layerIndex):
        self.layers[layerIndex].set_phi_outlet()
        phiOutlet = self.layers[layerIndex].phiOutletCenter
        # TODO: all this stuff...

    def set_block_outlet_center(self, layerIndex):
        phiOutlet = self.layers[layerIndex].phiOutletCenter
        # TODO: all this stuff...

    def set_block_outlet_upper(self, layerIndex):
        phiOutlet = self.layers[layerIndex].phiOutletCenter
        # TODO: all this stuff...


    def set_block_0(self, layerIndex):
        # ======= BLOCK 0 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block0Mapping = {'U0W0':lambda x:1 -
                         x * (1.0 - lowerLayer.inletDivision[1]),
                         'U0W1':lambda x:1 -
                         x * (1.0 - upperLayer.inletDivision[1]),
                         'V0W0':lambda x:x * lowerLayer.cyclicLowerDivision[0],
                         'V0W1':lambda x:x * upperLayer.cyclicLowerDivision[0],
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo0,
                      'curveU0V1':self.qo1,
                      'curveU0W0':lowerLayer.inletCurve,
                      'curveU0W1':upperLayer.inletCurve,
                      'curveU1V0':self.qo4,
                      'curveU1V1':self.qo5,
                      'curveU1W0':lowerLayer.edge_4_5,
                      'curveU1W1':upperLayer.edge_4_5,
                      'curveV0W0':lowerLayer.cyclicLowerCurve,
                      'curveV0W1':upperLayer.cyclicLowerCurve,
                      'curveV1W0':lowerLayer.edge_1_5,
                      'curveV1W1':upperLayer.edge_1_5
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'U1W0':lowerLayer.grading_4_5,
                      'U1W1':upperLayer.grading_4_5,
                      'V0W0':lowerLayer.gradingCyclicLower[0],
                      'V0W1':upperLayer.gradingCyclicLower[0],
                      'V1W0':lowerLayer.grading_1_5,
                      'V1W1':upperLayer.grading_1_5
                      }

        shape = (self.cellsMer0 + 1, self.cellsCirc2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block0 = interpolate_block(shape, curvesDict,
                                   block0Mapping, gradingBlock)
        self.topo.blockList[0 + layerIndex * self.blocksPerLayer] = block0



    def set_block_1(self, layerIndex):
        # ======= BLOCK 1 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block1Mapping = {'U0W0':lambda x:lowerLayer.inletDivision[1] -
                         x * (lowerLayer.inletDivision[1] -
                              lowerLayer.inletDivision[0]),
                         'U0W1':lambda x:upperLayer.inletDivision[1] -
                         x * (upperLayer.inletDivision[1] -
                              upperLayer.inletDivision[0]),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo1,
                      'curveU0V1':self.qo2,
                      'curveU0W0':lowerLayer.inletCurve,
                      'curveU0W1':upperLayer.inletCurve,
                      'curveU1V0':self.qo5,
                      'curveU1V1':self.qo8,
                      'curveU1W0':lowerLayer.edge_5_8,
                      'curveU1W1':upperLayer.edge_5_8,
                      'curveV0W0':lowerLayer.edge_1_5,
                      'curveV0W1':upperLayer.edge_1_5,
                      'curveV1W0':lowerLayer.edge_2_8,
                      'curveV1W1':upperLayer.edge_2_8
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'U1W0':lowerLayer.grading_5_8,
                      'U1W1':upperLayer.grading_5_8,
                      'V0W0':lowerLayer.grading_1_5,
                      'V0W1':upperLayer.grading_1_5,
                      'V1W0':lowerLayer.grading_2_8,
                      'V1W1':upperLayer.grading_2_8
                      }

        shape = (self.cellsMer0 + 1, self.cellsCirc1 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block1 = interpolate_block(shape, curvesDict,
                                   block1Mapping, gradingBlock)
        self.topo.blockList[1 + layerIndex * self.blocksPerLayer] = block1



    def set_block_2(self, layerIndex):
        # ======= BLOCK 2 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block2Mapping = {'U0W0':lambda x:lowerLayer.inletDivision[0] -
                         x * lowerLayer.inletDivision[0],
                         'U0W1':lambda x:upperLayer.inletDivision[0] -
                         x * upperLayer.inletDivision[0],
                         'V1W0':lambda x:x * lowerLayer.cyclicUpperDivision[0],
                         'V1W1':lambda x:x * upperLayer.cyclicUpperDivision[0],
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo2,
                      'curveU0V1':self.qo3,
                      'curveU0W0':lowerLayer.inletCurve,
                      'curveU0W1':upperLayer.inletCurve,
                      'curveU1V0':self.qo8,
                      'curveU1V1':self.qo9,
                      'curveU1W0':lowerLayer.edge_8_9,
                      'curveU1W1':upperLayer.edge_8_9,
                      'curveV0W0':lowerLayer.edge_2_8,
                      'curveV0W1':upperLayer.edge_2_8,
                      'curveV1W0':lowerLayer.cyclicUpperCurve,
                      'curveV1W1':upperLayer.cyclicUpperCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'U1W0':lowerLayer.grading_8_9,
                      'U1W1':upperLayer.grading_8_9,
                      'V0W0':lowerLayer.grading_2_8,
                      'V0W1':upperLayer.grading_2_8,
                      'V1W0':lowerLayer.gradingCyclicUpper[0],
                      'V1W1':upperLayer.gradingCyclicUpper[0]
                      }

        shape = (self.cellsMer0 + 1, self.cellsCirc0 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block2 = interpolate_block(shape, curvesDict,
                                   block2Mapping, gradingBlock)
        self.topo.blockList[2 + layerIndex * self.blocksPerLayer] = block2


    def set_block_3(self, layerIndex):
        # ======= BLOCK 3 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block3Mapping = {'V0W0':lambda x:lowerLayer.cyclicLowerDivision[0] +
                            x * (lowerLayer.cyclicLowerDivision[1] -
                                 lowerLayer.cyclicLowerDivision[0]),
                         'V0W1':lambda x:upperLayer.cyclicLowerDivision[0] +
                            x * (upperLayer.cyclicLowerDivision[1] -
                                 upperLayer.cyclicLowerDivision[0]),
                         'V1W0':lambda x:x * lowerLayer.point11u,
                         'V1W1':lambda x:x * upperLayer.point11u,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo4,
                      'curveU0V1':self.qo5,
                      'curveU0W0':lowerLayer.edge_4_5,
                      'curveU0W1':upperLayer.edge_4_5,
                      'curveU1V0':self.qo10,
                      'curveU1V1':self.qo11,
                      'curveU1W0':lowerLayer.edge_10_11,
                      'curveU1W1':upperLayer.edge_10_11,
                      'curveV0W0':lowerLayer.cyclicLowerCurve,
                      'curveV0W1':upperLayer.cyclicLowerCurve,
                      'curveV1W0':lowerLayer.edge_5_11_17,
                      'curveV1W1':upperLayer.edge_5_11_17
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U0W0':lowerLayer.grading_4_5,
                      'U0W1':upperLayer.grading_4_5,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'U1W0':lowerLayer.grading_10_11,
                      'U1W1':upperLayer.grading_10_11,
                      'V0W0':lowerLayer.gradingCyclicLower[1],
                      'V0W1':upperLayer.gradingCyclicLower[1],
                      'V1W0':lowerLayer.grading_5_11,
                      'V1W1':upperLayer.grading_5_11
                      }

        shape = (self.cellsMer1 + 1, self.cellsCirc2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block3 = interpolate_block(shape, curvesDict,
                                   block3Mapping, gradingBlock)
        self.topo.blockList[3 + layerIndex * self.blocksPerLayer] = block3


    def set_block_4(self, layerIndex):
        # ======= BLOCK 4 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block4Mapping = {'V1W0':lambda x:lowerLayer.profileDivision[3] +
                            x * (lowerLayer.profileDivision[4] -
                                 lowerLayer.profileDivision[3]),
                         'V1W1':lambda x:upperLayer.profileDivision[3] +
                            x * (upperLayer.profileDivision[4] -
                                 upperLayer.profileDivision[3]),
                         'V0W0':lambda x:x * lowerLayer.point11u,
                         'V0W1':lambda x:x * upperLayer.point11u,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo5,
                      'curveU0V1':self.qo6,
                      'curveU0W0':lowerLayer.edge_5_6,
                      'curveU0W1':upperLayer.edge_5_6,
                      'curveU1V0':self.qo11,
                      'curveU1V1':self.qo12,
                      'curveU1W0':lowerLayer.edge_11_12,
                      'curveU1W1':upperLayer.edge_11_12,
                      'curveV0W0':lowerLayer.edge_5_11_17,
                      'curveV0W1':upperLayer.edge_5_11_17,
                      'curveV1W0':lowerLayer.bladeCurve,
                      'curveV1W1':upperLayer.bladeCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_5_6,
                       'U0W1':upperLayer.grading_5_6,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_11_12,
                       'U1W1':upperLayer.grading_11_12,
                       'V0W0':lowerLayer.grading_5_11,
                       'V0W1':upperLayer.grading_5_11,
                       'V1W0':lowerLayer.gradingBlade_4,
                       'V1W1':upperLayer.gradingBlade_4
                       }

        shape = (self.cellsMer1 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block4 = interpolate_block(shape, curvesDict,
                                   block4Mapping, gradingBlock)
        self.topo.blockList[4 + layerIndex * self.blocksPerLayer] = block4


    def set_block_4_nurbs(self, layerIndex):
        # ======= BLOCK 4 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block4Mapping = {'V1W0':lambda x:lowerLayer.profileDivision[3] +
                            x * (lowerLayer.profileDivision[4] -
                                 lowerLayer.profileDivision[3]),
                         'V1W1':lambda x:upperLayer.profileDivision[3] +
                            x * (upperLayer.profileDivision[4] -
                                 upperLayer.profileDivision[3]),
                         'V0W0':lambda x:x * lowerLayer.point11u,
                         'V0W1':lambda x:x * upperLayer.point11u,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo5,
                      'U0V1':self.qo6,
                      'U0W0':lowerLayer.edge_5_6,
                      'U0W1':upperLayer.edge_5_6,
                      'U1V0':self.qo11,
                      'U1V1':self.qo12,
                      'U1W0':lowerLayer.edge_11_12,
                      'U1W1':upperLayer.edge_11_12,
                      'V0W0':lowerLayer.edge_5_11_17,
                      'V0W1':upperLayer.edge_5_11_17,
                      'V1W0':lowerLayer.bladeCurve,
                      'V1W1':upperLayer.bladeCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_5_6,
                       'U0W1':upperLayer.grading_5_6,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_11_12,
                       'U1W1':upperLayer.grading_11_12,
                       'V0W0':lowerLayer.grading_5_11,
                       'V0W1':upperLayer.grading_5_11,
                       'V1W0':lowerLayer.gradingBlade_4,
                       'V1W1':upperLayer.gradingBlade_4
                       }

        shape = (self.cellsMer1 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        surfaceDict = {'V1':self.bladeSurfaceNurbs}
        switchUV = {'V1':True}
        master = 'V1'
        cornerU0V0 = [lowerLayer.profileDivision[3], lowerLayer.span]
        cornerU0V1 = [upperLayer.profileDivision[3], upperLayer.span]
        cornerU1V0 = [lowerLayer.profileDivision[4], lowerLayer.span]
        cornerU1V1 = [upperLayer.profileDivision[4], upperLayer.span]
        quadBoundaries = {'V1':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block4 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block4Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV)
        self.topo.blockList[4 + layerIndex * self.blocksPerLayer] = block4


    def set_block_5(self, layerIndex):
        # ======= BLOCK 5 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block5Mapping = {'U1W0':lambda x:lowerLayer.profileDivision[3] -
                            x * (lowerLayer.profileDivision[3] -
                                 lowerLayer.profileDivision[2]),
                         'U1W1':lambda x:upperLayer.profileDivision[3] -
                            x * (upperLayer.profileDivision[3] -
                                 upperLayer.profileDivision[2]),
                         'V1W0':lambda x:1 - x,
                         'V1W1':lambda x:1 - x,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo5,
                      'curveU0V1':self.qo8,
                      'curveU0W0':lowerLayer.edge_5_8,
                      'curveU0W1':upperLayer.edge_5_8,
                      'curveU1V0':self.qo6,
                      'curveU1V1':self.qo7,
                      'curveU1W0':lowerLayer.bladeCurve,
                      'curveU1W1':upperLayer.bladeCurve,
                      'curveV0W0':lowerLayer.edge_5_6,
                      'curveV0W1':upperLayer.edge_5_6,
                      'curveV1W0':lowerLayer.edge_7_8,
                      'curveV1W1':upperLayer.edge_7_8
                      }

        # treating the reversed alignment of neighbouring blocks:
        if upperLayer.grading_7_8 != None:
            upperReverse = list(1 - np.array(upperLayer.grading_7_8)[::-1])
        else:
            upperReverse = None
        if lowerLayer.grading_7_8 != None:
            lowerReverse = list(1 - np.array(lowerLayer.grading_7_8)[::-1])
        else:
            lowerReverse = None
        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_5_8,
                       'U0W1':upperLayer.grading_5_8,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.gradingBlade_5,
                       'U1W1':upperLayer.gradingBlade_5,
                       'V0W0':lowerLayer.grading_5_6,
                       'V0W1':upperLayer.grading_5_6,
                       'V1W0':lowerReverse,
                       'V1W1':upperReverse
                       }

        shape = (self.cellsOBlock + 1, self.cellsCirc1 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block5 = interpolate_block(shape, curvesDict,
                                   block5Mapping, gradingBlock)
        self.topo.blockList[5 + layerIndex * self.blocksPerLayer] = block5


    def set_block_5_nurbs(self, layerIndex):
        # ======= BLOCK 5 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block5Mapping = {'U1W0':lambda x:lowerLayer.profileDivision[3] -
                            x * (lowerLayer.profileDivision[3] -
                                 lowerLayer.profileDivision[2]),
                         'U1W1':lambda x:upperLayer.profileDivision[3] -
                            x * (upperLayer.profileDivision[3] -
                                 upperLayer.profileDivision[2]),
                         'V1W0':lambda x:1 - x,
                         'V1W1':lambda x:1 - x,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo5,
                      'U0V1':self.qo8,
                      'U0W0':lowerLayer.edge_5_8,
                      'U0W1':upperLayer.edge_5_8,
                      'U1V0':self.qo6,
                      'U1V1':self.qo7,
                      'U1W0':lowerLayer.bladeCurve,
                      'U1W1':upperLayer.bladeCurve,
                      'V0W0':lowerLayer.edge_5_6,
                      'V0W1':upperLayer.edge_5_6,
                      'V1W0':lowerLayer.edge_7_8,
                      'V1W1':upperLayer.edge_7_8
                      }

        # treating the reversed alignment of neighbouring blocks:
        if upperLayer.grading_7_8 != None:
            upperReverse = list(1 - np.array(upperLayer.grading_7_8)[::-1])
        else:
            upperReverse = None
        if lowerLayer.grading_7_8 != None:
            lowerReverse = list(1 - np.array(lowerLayer.grading_7_8)[::-1])
        else:
            lowerReverse = None
        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_5_8,
                       'U0W1':upperLayer.grading_5_8,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.gradingBlade_5,
                       'U1W1':upperLayer.gradingBlade_5,
                       'V0W0':lowerLayer.grading_5_6,
                       'V0W1':upperLayer.grading_5_6,
                       'V1W0':lowerReverse,
                       'V1W1':upperReverse
                       }
        surfaceDict = {'U1':self.bladeSurfaceNurbs}
        switchUV = {'U1':True}
        master = 'U1'
        shape = (self.cellsOBlock + 1, self.cellsCirc1 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        cornerU0V0 = [lowerLayer.profileDivision[3], lowerLayer.span]
        cornerU0V1 = [upperLayer.profileDivision[3], upperLayer.span]
        cornerU1V0 = [lowerLayer.profileDivision[2], lowerLayer.span]
        cornerU1V1 = [upperLayer.profileDivision[2], upperLayer.span]
        quadBoundaries = {'U1':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block5 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block5Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV)
        self.topo.blockList[5 + layerIndex * self.blocksPerLayer] = block5


    def set_block_6(self, layerIndex):
        # ======= BLOCK 6 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block6Mapping = {'V0W0':lambda x:lowerLayer.profileDivision[2] -
                            x * (lowerLayer.profileDivision[2] -
                                 lowerLayer.profileDivision[1]),
                         'V0W1':lambda x:upperLayer.profileDivision[2] -
                            x * (upperLayer.profileDivision[2] -
                                 upperLayer.profileDivision[1]),
                         'V1W0':lambda x:x * lowerLayer.point14u,
                         'V1W1':lambda x:x * upperLayer.point14u,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo7,
                      'curveU0V1':self.qo8,
                      'curveU0W0':lowerLayer.edge_7_8,
                      'curveU0W1':upperLayer.edge_7_8,
                      'curveU1V0':self.qo13,
                      'curveU1V1':self.qo14,
                      'curveU1W0':lowerLayer.edge_13_14,
                      'curveU1W1':upperLayer.edge_13_14,
                      'curveV0W0':lowerLayer.bladeCurve,
                      'curveV0W1':upperLayer.bladeCurve,
                      'curveV1W0':lowerLayer.edge_8_14_22,
                      'curveV1W1':upperLayer.edge_8_14_22
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_7_8,
                       'U0W1':upperLayer.grading_7_8,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_13_14,
                       'U1W1':upperLayer.grading_13_14,
                       'V0W0':lowerLayer.gradingBlade_6,
                       'V0W1':upperLayer.gradingBlade_6,
                       'V1W0':lowerLayer.grading_8_14,
                       'V1W1':upperLayer.grading_8_14
                       }

        shape = (self.cellsMer1 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block6 = interpolate_block(shape, curvesDict,
                                   block6Mapping, gradingBlock)
        self.topo.blockList[6 + layerIndex * self.blocksPerLayer] = block6


    def set_block_6_nurbs(self, layerIndex):
        # ======= BLOCK 6 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block6Mapping = {'V0W0':lambda x:lowerLayer.profileDivision[2] -
                            x * (lowerLayer.profileDivision[2] -
                                 lowerLayer.profileDivision[1]),
                         'V0W1':lambda x:upperLayer.profileDivision[2] -
                            x * (upperLayer.profileDivision[2] -
                                 upperLayer.profileDivision[1]),
                         'V1W0':lambda x:x * lowerLayer.point14u,
                         'V1W1':lambda x:x * upperLayer.point14u,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo7,
                      'U0V1':self.qo8,
                      'U0W0':lowerLayer.edge_7_8,
                      'U0W1':upperLayer.edge_7_8,
                      'U1V0':self.qo13,
                      'U1V1':self.qo14,
                      'U1W0':lowerLayer.edge_13_14,
                      'U1W1':upperLayer.edge_13_14,
                      'V0W0':lowerLayer.bladeCurve,
                      'V0W1':upperLayer.bladeCurve,
                      'V1W0':lowerLayer.edge_8_14_22,
                      'V1W1':upperLayer.edge_8_14_22
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_7_8,
                       'U0W1':upperLayer.grading_7_8,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_13_14,
                       'U1W1':upperLayer.grading_13_14,
                       'V0W0':lowerLayer.gradingBlade_6,
                       'V0W1':upperLayer.gradingBlade_6,
                       'V1W0':lowerLayer.grading_8_14,
                       'V1W1':upperLayer.grading_8_14
                       }

        shape = (self.cellsMer1 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        surfaceDict = {'V0':self.bladeSurfaceNurbs}
        switchUV = {'V0':True}
        master = 'V0'
        cornerU0V0 = [lowerLayer.profileDivision[2], lowerLayer.span]
        cornerU0V1 = [upperLayer.profileDivision[2], upperLayer.span]
        cornerU1V0 = [lowerLayer.profileDivision[1], lowerLayer.span]
        cornerU1V1 = [upperLayer.profileDivision[1], upperLayer.span]
        quadBoundaries = {'V0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block6 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block6Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV)
        self.topo.blockList[6 + layerIndex * self.blocksPerLayer] = block6


    def set_block_7(self, layerIndex):
        # ======= BLOCK 7 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block7Mapping = {'V1W0':lambda x:lowerLayer.cyclicUpperDivision[0] +
                            x * (lowerLayer.cyclicUpperDivision[1] -
                                 lowerLayer.cyclicUpperDivision[0]),
                         'V1W1':lambda x:upperLayer.cyclicUpperDivision[0] +
                            x * (upperLayer.cyclicUpperDivision[1] -
                                 upperLayer.cyclicUpperDivision[0]),
                         'V0W0':lambda x:x * lowerLayer.point14u,
                         'V0W1':lambda x:x * upperLayer.point14u,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo8,
                      'curveU0V1':self.qo9,
                      'curveU0W0':lowerLayer.edge_8_9,
                      'curveU0W1':upperLayer.edge_8_9,
                      'curveU1V0':self.qo14,
                      'curveU1V1':self.qo15,
                      'curveU1W0':lowerLayer.edge_14_15,
                      'curveU1W1':upperLayer.edge_14_15,
                      'curveV0W0':lowerLayer.edge_8_14_22,
                      'curveV0W1':upperLayer.edge_8_14_22,
                      'curveV1W0':lowerLayer.cyclicUpperCurve,
                      'curveV1W1':upperLayer.cyclicUpperCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_8_9,
                       'U0W1':upperLayer.grading_8_9,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_14_15,
                       'U1W1':upperLayer.grading_14_15,
                       'V0W0':lowerLayer.grading_8_14,
                       'V0W1':upperLayer.grading_8_14,
                       'V1W0':lowerLayer.gradingCyclicUpper[1],
                       'V1W1':upperLayer.gradingCyclicUpper[1]
                       }

        shape = (self.cellsMer1 + 1, self.cellsCirc0 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block7 = interpolate_block(shape, curvesDict,
                                   block7Mapping, gradingBlock)
        self.topo.blockList[7 + layerIndex * self.blocksPerLayer] = block7


    def set_block_8(self, layerIndex):
        # ======= BLOCK 8 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block8Mapping = {'V0W0':lambda x:lowerLayer.cyclicLowerDivision[1] +
                            x * (lowerLayer.cyclicLowerDivision[2] -
                                 lowerLayer.cyclicLowerDivision[1]),
                         'V0W1':lambda x:upperLayer.cyclicLowerDivision[1] +
                            x * (upperLayer.cyclicLowerDivision[2] -
                                 upperLayer.cyclicLowerDivision[1]),
                         'V1W0':lambda x:lowerLayer.point11u + x *
                                        (1 - lowerLayer.point11u),
                         'V1W1':lambda x:upperLayer.point11u + x *
                                        (1 - upperLayer.point11u),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo10,
                      'curveU0V1':self.qo11,
                      'curveU0W0':lowerLayer.edge_10_11,
                      'curveU0W1':upperLayer.edge_10_11,
                      'curveU1V0':self.qo16,
                      'curveU1V1':self.qo17,
                      'curveU1W0':lowerLayer.edge_16_17,
                      'curveU1W1':upperLayer.edge_16_17,
                      'curveV0W0':lowerLayer.cyclicLowerCurve,
                      'curveV0W1':upperLayer.cyclicLowerCurve,
                      'curveV1W0':lowerLayer.edge_5_11_17,
                      'curveV1W1':upperLayer.edge_5_11_17
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U0W0':lowerLayer.grading_10_11,
                      'U0W1':upperLayer.grading_10_11,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'U1W0':lowerLayer.grading_16_17,
                      'U1W1':upperLayer.grading_16_17,
                      'V0W0':lowerLayer.gradingCyclicLower[2],
                      'V0W1':upperLayer.gradingCyclicLower[2],
                      'V1W0':lowerLayer.grading_5_11,
                      'V1W1':upperLayer.grading_5_11
                      }

        shape = (self.cellsMer2 + 1, self.cellsCirc2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block8 = interpolate_block(shape, curvesDict,
                                   block8Mapping, gradingBlock)
        self.topo.blockList[8 + layerIndex * self.blocksPerLayer] = block8



    def set_block_9(self, layerIndex):
        # ======= BLOCK 9 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block9Mapping = {'V1W0':lambda x:lowerLayer.profileDivision[4] +
                            x * (lowerLayer.profileDivision[5] -
                                 lowerLayer.profileDivision[4]),
                         'V1W1':lambda x:upperLayer.profileDivision[4] +
                            x * (upperLayer.profileDivision[5] -
                                 upperLayer.profileDivision[4]),
                         'V0W0':lambda x:lowerLayer.point11u + x *
                                        (1 - lowerLayer.point11u),
                         'V0W1':lambda x:upperLayer.point11u + x *
                                        (1 - upperLayer.point11u),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo11,
                      'curveU0V1':self.qo12,
                      'curveU0W0':lowerLayer.edge_11_12,
                      'curveU0W1':upperLayer.edge_11_12,
                      'curveU1V0':self.qo17,
                      'curveU1V1':self.qo18,
                      'curveU1W0':lowerLayer.edge_17_18,
                      'curveU1W1':upperLayer.edge_17_18,
                      'curveV0W0':lowerLayer.edge_5_11_17,
                      'curveV0W1':upperLayer.edge_5_11_17,
                      'curveV1W0':lowerLayer.bladeCurve,
                      'curveV1W1':upperLayer.bladeCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_11_12,
                       'U0W1':upperLayer.grading_11_12,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_17_18,
                       'U1W1':upperLayer.grading_17_18,
                       'V0W0':lowerLayer.grading_5_11,
                       'V0W1':upperLayer.grading_5_11,
                       'V1W0':lowerLayer.gradingBlade_9,
                       'V1W1':upperLayer.gradingBlade_9
                       }

        shape = (self.cellsMer2 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block9 = interpolate_block(shape, curvesDict,
                                   block9Mapping, gradingBlock)
        self.topo.blockList[9 + layerIndex * self.blocksPerLayer] = block9


    def set_block_9_nurbs(self, layerIndex):
        # ======= BLOCK 9 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block9Mapping = {'V1W0':lambda x:lowerLayer.profileDivision[4] +
                            x * (lowerLayer.profileDivision[5] -
                                 lowerLayer.profileDivision[4]),
                         'V1W1':lambda x:upperLayer.profileDivision[4] +
                            x * (upperLayer.profileDivision[5] -
                                 upperLayer.profileDivision[4]),
                         'V0W0':lambda x:lowerLayer.point11u + x *
                                        (1 - lowerLayer.point11u),
                         'V0W1':lambda x:upperLayer.point11u + x *
                                        (1 - upperLayer.point11u),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo11,
                      'U0V1':self.qo12,
                      'U0W0':lowerLayer.edge_11_12,
                      'U0W1':upperLayer.edge_11_12,
                      'U1V0':self.qo17,
                      'U1V1':self.qo18,
                      'U1W0':lowerLayer.edge_17_18,
                      'U1W1':upperLayer.edge_17_18,
                      'V0W0':lowerLayer.edge_5_11_17,
                      'V0W1':upperLayer.edge_5_11_17,
                      'V1W0':lowerLayer.bladeCurve,
                      'V1W1':upperLayer.bladeCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_11_12,
                       'U0W1':upperLayer.grading_11_12,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_17_18,
                       'U1W1':upperLayer.grading_17_18,
                       'V0W0':lowerLayer.grading_5_11,
                       'V0W1':upperLayer.grading_5_11,
                       'V1W0':lowerLayer.gradingBlade_9,
                       'V1W1':upperLayer.gradingBlade_9
                       }

#         shape = (self.cellsMer2 + 1, self.cellsOBlock + 1,
#                  self.cellsRad[layerIndex] + 1, 3)
#         gradingBlock = create_grading_block(shape, gradingDict)
#         block9 = interpolate_block(shape, curvesDict,
#                                    block9Mapping, gradingBlock)
#         self.topo.blockList[9 + layerIndex * self.blocksPerLayer] = block9

        shape = (self.cellsMer2 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        surfaceDict = {'V1':self.bladeSurfaceNurbs}
        switchUV = {'V1':True}
        master = 'V1'
        cornerU0V0 = [lowerLayer.profileDivision[4], lowerLayer.span]
        cornerU0V1 = [upperLayer.profileDivision[4], upperLayer.span]
        cornerU1V0 = [lowerLayer.profileDivision[5], lowerLayer.span]
        cornerU1V1 = [upperLayer.profileDivision[5], upperLayer.span]
        quadBoundaries = {'V1':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block9 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block9Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV)
        self.topo.blockList[9 + layerIndex * self.blocksPerLayer] = block9


    def set_block_10(self, layerIndex):
        # ======= BLOCK 10 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        def mapping_func_TE_lower(x):
            u = lowerLayer.profileDivision[5] + x * (1 -
                lowerLayer.profileDivision[5])
            if u > 1:
                return u - 1.0
            else:
                return u

        def mapping_func_TE_upper(x):
            u = upperLayer.profileDivision[5] + x * (1 - 
                upperLayer.profileDivision[5])
            if u > 1:
                return u - 1.0
            else:
                return u

        block10Mapping = {'U0W0':mapping_func_TE_lower,
                         'U0W1':mapping_func_TE_upper,
                         'V0W0':lambda x:1 - x,
                         'V0W1':lambda x:1 - x,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo18,
                      'curveU0V1':self.qo19,
                      'curveU0W0':lowerLayer.bladeCurve,
                      'curveU0W1':upperLayer.bladeCurve,
                      'curveU1V0':self.qo17,
                      'curveU1V1':self.qo20,
                      'curveU1W0':lowerLayer.edge_17_20,
                      'curveU1W1':upperLayer.edge_17_20,
                      'curveV0W0':lowerLayer.edge_17_18,
                      'curveV0W1':upperLayer.edge_17_18,
                      'curveV1W0':lowerLayer.edge_19_20,
                      'curveV1W1':upperLayer.edge_19_20
                      }

        # treating the reversed alignment of neighbouring blocks:
        if upperLayer.grading_17_18 != None:
            upperReverse = list(1 - np.array(upperLayer.grading_17_18)[::-1])
        else:
            upperReverse = None
        if lowerLayer.grading_17_18 != None:
            lowerReverse = list(1 - np.array(lowerLayer.grading_17_18)[::-1])
        else:
            lowerReverse = None
        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.gradingBlade_10,
                       'U0W1':upperLayer.gradingBlade_10,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_17_20,
                       'U1W1':upperLayer.grading_17_20,
                       'V0W0':lowerReverse,
                       'V0W1':upperReverse,
                       'V1W0':lowerLayer.grading_19_20,
                       'V1W1':upperLayer.grading_19_20
                       }

        shape = (self.cellsOBlock + 1, self.cellsCirc1/2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block10 = interpolate_block(shape, curvesDict,
                                    block10Mapping, gradingBlock)
        self.topo.blockList[10 + layerIndex * self.blocksPerLayer] = block10


    def set_block_10_nurbs(self, layerIndex):
        # ======= BLOCK 10 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        def mapping_func_TE_lower(x):
            u = lowerLayer.profileDivision[5] + x * (1 -
                lowerLayer.profileDivision[5])
            if u > 1:
                return u - 1.0
            else:
                return u

        def mapping_func_TE_upper(x):
            u = upperLayer.profileDivision[5] + x * (1 -
                upperLayer.profileDivision[5])
            if u > 1:
                return u - 1.0
            else:
                return u

        block10Mapping = {'U0W0':mapping_func_TE_lower,
                         'U0W1':mapping_func_TE_upper,
                         'V0W0':lambda x:1 - x,
                         'V0W1':lambda x:1 - x,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo18,
                      'U0V1':self.qo19,
                      'U0W0':lowerLayer.bladeCurve,
                      'U0W1':upperLayer.bladeCurve,
                      'U1V0':self.qo17,
                      'U1V1':self.qo20,
                      'U1W0':lowerLayer.edge_17_20,
                      'U1W1':upperLayer.edge_17_20,
                      'V0W0':lowerLayer.edge_17_18,
                      'V0W1':upperLayer.edge_17_18,
                      'V1W0':lowerLayer.edge_19_20,
                      'V1W1':upperLayer.edge_19_20
                      }

        # treating the reversed alignment of neighbouring blocks:
        if upperLayer.grading_17_18 != None:
            upperReverse = list(1 - np.array(upperLayer.grading_17_18)[::-1])
        else:
            upperReverse = None
        if lowerLayer.grading_17_18 != None:
            lowerReverse = list(1 - np.array(lowerLayer.grading_17_18)[::-1])
        else:
            lowerReverse = None
        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.gradingBlade_10,
                       'U0W1':upperLayer.gradingBlade_10,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_17_20,
                       'U1W1':upperLayer.grading_17_20,
                       'V0W0':lowerReverse,
                       'V0W1':upperReverse,
                       'V1W0':lowerLayer.grading_19_20,
                       'V1W1':upperLayer.grading_19_20
                       }

#         shape = (self.cellsOBlock + 1, self.cellsCirc1 + 1,
#                  self.cellsRad[layerIndex] + 1, 3)
#         gradingBlock = create_grading_block(shape, gradingDict)
#         block10 = interpolate_block(shape, curvesDict,
#                                     block10Mapping, gradingBlock)
#         self.topo.blockList[10 + layerIndex * self.blocksPerLayer] = block10

        shape = (self.cellsOBlock + 1, self.cellsCirc1/2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        surfaceDict = {'U0':self.bladeSurfaceNurbs}
        switchUV = {'U0':True}
        master = 'U0'
        cornerU0V0 = [lowerLayer.profileDivision[5], lowerLayer.span]
        cornerU0V1 = [upperLayer.profileDivision[5], upperLayer.span]
        cornerU1V0 = [1, lowerLayer.span]
        cornerU1V1 = [1, upperLayer.span]
        quadBoundaries = {'U0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        bridgeDict = {}#'U0':True}
        block10 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block10Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV, 
                                                 bridgeU=bridgeDict)
        self.topo.blockList[10 + layerIndex * self.blocksPerLayer] = block10


    def set_block_11(self, layerIndex):
        # ======= BLOCK 11 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        def mapping_func_TE_lower(x):
            u = x * lowerLayer.profileDivision[0]
            return u

        def mapping_func_TE_upper(x):
            u = x * upperLayer.profileDivision[0]
            return u

        block11Mapping = {'U0W0':mapping_func_TE_lower,
                         'U0W1':mapping_func_TE_upper,
                         'V0W0':lambda x:x,
                         'V0W1':lambda x:x,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo19,
                      'curveU0V1':self.qo21,
                      'curveU0W0':lowerLayer.bladeCurve,
                      'curveU0W1':upperLayer.bladeCurve,
                      'curveU1V0':self.qo20,
                      'curveU1V1':self.qo22,
                      'curveU1W0':lowerLayer.edge_20_22,
                      'curveU1W1':upperLayer.edge_20_22,
                      'curveV0W0':lowerLayer.edge_19_20,
                      'curveV0W1':upperLayer.edge_19_20,
                      'curveV1W0':lowerLayer.edge_21_22,
                      'curveV1W1':upperLayer.edge_21_22
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.gradingBlade_11,
                       'U0W1':upperLayer.gradingBlade_11,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_20_22,
                       'U1W1':upperLayer.grading_20_22,
                       'V0W0':lowerLayer.grading_19_20,
                       'V0W1':upperLayer.grading_19_20,
                       'V1W0':lowerLayer.grading_21_22,
                       'V1W1':upperLayer.grading_21_22
                       }

        shape = (self.cellsOBlock + 1, self.cellsCirc1/2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block11 = interpolate_block(shape, curvesDict,
                                    block11Mapping, gradingBlock)
        self.topo.blockList[11 + layerIndex * self.blocksPerLayer] = block11




    def set_block_11_nurbs(self, layerIndex):
        # ======= BLOCK 11 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        def mapping_func_TE_lower(x):
            u = x * lowerLayer.profileDivision[0]
            return u

        def mapping_func_TE_upper(x):
            u = x * upperLayer.profileDivision[0]
            return u

        block11Mapping = {'U0W0':mapping_func_TE_lower,
                         'U0W1':mapping_func_TE_upper,
                         'V0W0':lambda x:x,
                         'V0W1':lambda x:x,
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo19,
                      'U0V1':self.qo21,
                      'U0W0':lowerLayer.bladeCurve,
                      'U0W1':upperLayer.bladeCurve,
                      'U1V0':self.qo20,
                      'U1V1':self.qo22,
                      'U1W0':lowerLayer.edge_20_22,
                      'U1W1':upperLayer.edge_20_22,
                      'V0W0':lowerLayer.edge_19_20,
                      'V0W1':upperLayer.edge_19_20,
                      'V1W0':lowerLayer.edge_21_22,
                      'V1W1':upperLayer.edge_21_22
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.gradingBlade_11,
                       'U0W1':upperLayer.gradingBlade_11,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_20_22,
                       'U1W1':upperLayer.grading_20_22,
                       'V0W0':lowerLayer.grading_19_20,
                       'V0W1':upperLayer.grading_19_20,
                       'V1W0':lowerLayer.grading_21_22,
                       'V1W1':upperLayer.grading_21_22
                       }

#         shape = (self.cellsOBlock + 1, self.cellsCirc1 + 1,
#                  self.cellsRad[layerIndex] + 1, 3)
#         gradingBlock = create_grading_block(shape, gradingDict)
#         block10 = interpolate_block(shape, curvesDict,
#                                     block10Mapping, gradingBlock)
#         self.topo.blockList[10 + layerIndex * self.blocksPerLayer] = block10

        shape = (self.cellsOBlock + 1, self.cellsCirc1/2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        surfaceDict = {'U0':self.bladeSurfaceNurbs}
        switchUV = {'U0':True}
        master = 'U0'
        cornerU1V0 = [lowerLayer.profileDivision[0], lowerLayer.span]
        cornerU1V1 = [upperLayer.profileDivision[0], upperLayer.span]
        cornerU0V0 = [0, lowerLayer.span]
        cornerU0V1 = [0, upperLayer.span]
        quadBoundaries = {'U0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        bridgeDict = {}#'U0':True}
        block11 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block11Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV, 
                                                 bridgeU=bridgeDict)
        self.topo.blockList[11 + layerIndex * self.blocksPerLayer] = block11


    def set_block_12(self, layerIndex):
        # ======= BLOCK 12 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block12Mapping = {'V0W0':lambda x:lowerLayer.profileDivision[1] -
                            x * (lowerLayer.profileDivision[1] -
                                 lowerLayer.profileDivision[0]),
                         'V0W1':lambda x:upperLayer.profileDivision[1] -
                            x * (upperLayer.profileDivision[1] -
                                 upperLayer.profileDivision[0]),
                         'V1W0':lambda x:lowerLayer.point14u + x * (1 - lowerLayer.point14u),
                         'V1W1':lambda x:upperLayer.point14u + x * (1 - upperLayer.point14u),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo13,
                      'curveU0V1':self.qo14,
                      'curveU0W0':lowerLayer.edge_13_14,
                      'curveU0W1':upperLayer.edge_13_14,
                      'curveU1V0':self.qo21,
                      'curveU1V1':self.qo22,
                      'curveU1W0':lowerLayer.edge_21_22,
                      'curveU1W1':upperLayer.edge_21_22,
                      'curveV0W0':lowerLayer.bladeCurve,
                      'curveV0W1':upperLayer.bladeCurve,
                      'curveV1W0':lowerLayer.edge_8_14_22,
                      'curveV1W1':upperLayer.edge_8_14_22
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_13_14,
                       'U0W1':upperLayer.grading_13_14,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_21_22,
                       'U1W1':upperLayer.grading_21_22,
                       'V0W0':lowerLayer.gradingBlade_12,
                       'V0W1':upperLayer.gradingBlade_12,
                       'V1W0':lowerLayer.grading_8_14,
                       'V1W1':upperLayer.grading_8_14
                       }

        shape = (self.cellsMer2 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block12 = interpolate_block(shape, curvesDict,
                                    block12Mapping, gradingBlock)
        self.topo.blockList[12 + layerIndex * self.blocksPerLayer] = block12


    def set_block_12_nurbs(self, layerIndex):
        # ======= BLOCK 12 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block12Mapping = {'V0W0':lambda x:lowerLayer.profileDivision[1] -
                            x * (lowerLayer.profileDivision[1] -
                                 lowerLayer.profileDivision[0]),
                         'V0W1':lambda x:upperLayer.profileDivision[1] -
                            x * (upperLayer.profileDivision[1] -
                                 upperLayer.profileDivision[0]),
                         'V1W0':lambda x:lowerLayer.point14u + x * (1 - lowerLayer.point14u),
                         'V1W1':lambda x:upperLayer.point14u + x * (1 - upperLayer.point14u),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'U0V0':self.qo13,
                      'U0V1':self.qo14,
                      'U0W0':lowerLayer.edge_13_14,
                      'U0W1':upperLayer.edge_13_14,
                      'U1V0':self.qo21,
                      'U1V1':self.qo22,
                      'U1W0':lowerLayer.edge_21_22,
                      'U1W1':upperLayer.edge_21_22,
                      'V0W0':lowerLayer.bladeCurve,
                      'V0W1':upperLayer.bladeCurve,
                      'V1W0':lowerLayer.edge_8_14_22,
                      'V1W1':upperLayer.edge_8_14_22
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_13_14,
                       'U0W1':upperLayer.grading_13_14,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_21_22,
                       'U1W1':upperLayer.grading_21_22,
                       'V0W0':lowerLayer.gradingBlade_12,
                       'V0W1':upperLayer.gradingBlade_12,
                       'V1W0':lowerLayer.grading_8_14,
                       'V1W1':upperLayer.grading_8_14
                       }

#         shape = (self.cellsMer2 + 1, self.cellsOBlock + 1,
#                  self.cellsRad[layerIndex] + 1, 3)
#         gradingBlock = create_grading_block(shape, gradingDict)
#         block11 = interpolate_block(shape, curvesDict,
#                                     block11Mapping, gradingBlock)
#         self.topo.blockList[11 + layerIndex * self.blocksPerLayer] = block11

        shape = (self.cellsMer2 + 1, self.cellsOBlock + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        surfaceDict = {'V0':self.bladeSurfaceNurbs}
        switchUV = {'V0':True}
        master = 'V0'
        cornerU0V0 = [lowerLayer.profileDivision[1], lowerLayer.span]
        cornerU0V1 = [upperLayer.profileDivision[1], upperLayer.span]
        cornerU1V0 = [lowerLayer.profileDivision[0], lowerLayer.span]
        cornerU1V1 = [upperLayer.profileDivision[0], upperLayer.span]
        quadBoundaries = {'V0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block12 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block12Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 gradingBlock, switchUV)
        self.topo.blockList[12 + layerIndex * self.blocksPerLayer] = block12


    def set_block_13(self, layerIndex):
        # ======= BLOCK 13 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block13Mapping = {'V1W0':lambda x:lowerLayer.cyclicUpperDivision[1] +
                            x * (lowerLayer.cyclicUpperDivision[2] -
                                 lowerLayer.cyclicUpperDivision[1]),
                         'V1W1':lambda x:upperLayer.cyclicUpperDivision[1] +
                            x * (upperLayer.cyclicUpperDivision[2] -
                                 upperLayer.cyclicUpperDivision[1]),
                          'V0W0':lambda x:lowerLayer.point14u + x *
                                            (1 - lowerLayer.point14u),
                         'V0W1':lambda x:upperLayer.point14u + x *
                                            (1 - upperLayer.point14u),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo14,
                      'curveU0V1':self.qo15,
                      'curveU0W0':lowerLayer.edge_14_15,
                      'curveU0W1':upperLayer.edge_14_15,
                      'curveU1V0':self.qo22,
                      'curveU1V1':self.qo23,
                      'curveU1W0':lowerLayer.edge_22_23,
                      'curveU1W1':upperLayer.edge_22_23,
                      'curveV0W0':lowerLayer.edge_8_14_22,
                      'curveV0W1':upperLayer.edge_8_14_22,
                      'curveV1W0':lowerLayer.cyclicUpperCurve,
                      'curveV1W1':upperLayer.cyclicUpperCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                       'U0V1':lowerLayer.gradingQo,
                       'U0W0':lowerLayer.grading_14_15,
                       'U0W1':upperLayer.grading_14_15,
                       'U1V0':lowerLayer.gradingQo,
                       'U1V1':lowerLayer.gradingQo,
                       'U1W0':lowerLayer.grading_22_23,
                       'U1W1':upperLayer.grading_22_23,
                       'V0W0':lowerLayer.grading_8_14,
                       'V0W1':upperLayer.grading_8_14,
                       'V1W0':lowerLayer.gradingCyclicUpper[2],
                       'V1W1':upperLayer.gradingCyclicUpper[2]
                       }

        shape = (self.cellsMer2 + 1, self.cellsCirc0 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block13 = interpolate_block(shape, curvesDict, block13Mapping, gradingBlock)
        self.topo.blockList[13 + layerIndex * self.blocksPerLayer] = block13


    def set_block_14(self, layerIndex):
        # ======= BLOCK 14 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block14Mapping = {'U1W0':lambda x:1 -
                         x * (1.0 - lowerLayer.outletDivision[1]),
                         'U1W1':lambda x:1 -
                         x * (1.0 - upperLayer.outletDivision[1]),
                          'V0W0':lambda x:lowerLayer.cyclicLowerDivision[2] +
                            x * (1 - lowerLayer.cyclicLowerDivision[2]),
                         'V0W1':lambda x:upperLayer.cyclicLowerDivision[2] +
                            x * (1 - upperLayer.cyclicLowerDivision[2]),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo16,
                      'curveU0V1':self.qo17,
                      'curveU0W0':lowerLayer.edge_16_17,
                      'curveU0W1':upperLayer.edge_16_17,
                      'curveU1V0':self.qo24,
                      'curveU1V1':self.qo25,
                      'curveU1W0':lowerLayer.outletCurve,
                      'curveU1W1':upperLayer.outletCurve,
                      'curveV0W0':lowerLayer.cyclicLowerCurve,
                      'curveV0W1':upperLayer.cyclicLowerCurve,
                      'curveV1W0':lowerLayer.edge_17_25,
                      'curveV1W1':upperLayer.edge_17_25
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U0W0':lowerLayer.grading_16_17,
                      'U0W1':upperLayer.grading_16_17,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'V0W0':lowerLayer.gradingCyclicLower[3],
                      'V0W1':upperLayer.gradingCyclicLower[3],
                      'V1W0':lowerLayer.grading_17_25,
                      'V1W1':upperLayer.grading_17_25
                      }

        shape = (self.cellsMer3 + 1, self.cellsCirc2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block14 = interpolate_block(shape, curvesDict,
                                    block14Mapping, gradingBlock)
        self.topo.blockList[14 + layerIndex * self.blocksPerLayer] = block14


    def set_block_15(self, layerIndex):
        # ======= BLOCK 15 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block15Mapping = {'U1W0':lambda x:lowerLayer.outletDivision[2] -
                         x * (lowerLayer.outletDivision[2] -
                              lowerLayer.outletDivision[1]),
                         'U1W1':lambda x:upperLayer.outletDivision[2] -
                         x * (upperLayer.outletDivision[2] -
                              upperLayer.outletDivision[1]),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo17,
                      'curveU0V1':self.qo20,
                      'curveU0W0':lowerLayer.edge_17_20,
                      'curveU0W1':upperLayer.edge_17_20,
                      'curveU1V0':self.qo25,
                      'curveU1V1':self.qo26,
                      'curveU1W0':lowerLayer.outletCurve,
                      'curveU1W1':upperLayer.outletCurve,
                      'curveV0W0':lowerLayer.edge_17_25,
                      'curveV0W1':upperLayer.edge_17_25,
                      'curveV1W0':lowerLayer.edge_20_26,
                      'curveV1W1':upperLayer.edge_20_26
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U0W0':lowerLayer.grading_17_20,
                      'U0W1':upperLayer.grading_17_20,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'V0W0':lowerLayer.grading_17_25,
                      'V0W1':upperLayer.grading_17_25,
                      'V1W0':lowerLayer.grading_20_26,
                      'V1W1':upperLayer.grading_20_26
                      }

        shape = (self.cellsMer3 + 1, self.cellsCirc1/2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block15 = interpolate_block(shape, curvesDict,
                                    block15Mapping, gradingBlock)
        self.topo.blockList[15 + layerIndex * self.blocksPerLayer] = block15


    def set_block_16(self, layerIndex):
        # ======= BLOCK 16 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block16Mapping = {'U1W0':lambda x:lowerLayer.outletDivision[1] -
                         x * (lowerLayer.outletDivision[1] -
                              lowerLayer.outletDivision[0]),
                         'U1W1':lambda x:upperLayer.outletDivision[1] -
                         x * (upperLayer.outletDivision[1] -
                              upperLayer.outletDivision[0]),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo20,
                      'curveU0V1':self.qo22,
                      'curveU0W0':lowerLayer.edge_20_22,
                      'curveU0W1':upperLayer.edge_20_22,
                      'curveU1V0':self.qo26,
                      'curveU1V1':self.qo27,
                      'curveU1W0':lowerLayer.outletCurve,
                      'curveU1W1':upperLayer.outletCurve,
                      'curveV0W0':lowerLayer.edge_20_26,
                      'curveV0W1':upperLayer.edge_20_26,
                      'curveV1W0':lowerLayer.edge_22_27,
                      'curveV1W1':upperLayer.edge_22_27
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U0W0':lowerLayer.grading_20_22,
                      'U0W1':upperLayer.grading_20_22,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'V0W0':lowerLayer.grading_20_26,
                      'V0W1':upperLayer.grading_20_26,
                      'V1W0':lowerLayer.grading_22_27,
                      'V1W1':upperLayer.grading_22_27
                      }

        shape = (self.cellsMer3 + 1, self.cellsCirc1/2 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block16 = interpolate_block(shape, curvesDict,
                                    block16Mapping, gradingBlock)
        self.topo.blockList[16 + layerIndex * self.blocksPerLayer] = block16


    def set_block_17(self, layerIndex):
        # ======= BLOCK 17 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]
        block17Mapping = {'U1W0':lambda x:lowerLayer.outletDivision[0] -
                         x * lowerLayer.outletDivision[0],
                         'U1W1':lambda x:upperLayer.outletDivision[0] -
                         x * upperLayer.outletDivision[0],
                         'V1W0':lambda x:lowerLayer.cyclicUpperDivision[2] +
                         x * (1 - lowerLayer.cyclicUpperDivision[2]),
                         'V1W1':lambda x:lowerLayer.cyclicUpperDivision[2] +
                         x * (1 - lowerLayer.cyclicUpperDivision[2]),
                         'U0V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U0V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V0':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span),
                         'U1V1':lambda x:lowerLayer.span +
                         x * (upperLayer.span - lowerLayer.span)}

        curvesDict = {'curveU0V0':self.qo22,
                      'curveU0V1':self.qo23,
                      'curveU1W0':lowerLayer.outletCurve,
                      'curveU1W1':upperLayer.outletCurve,
                      'curveU1V0':self.qo27,
                      'curveU1V1':self.qo28,
                      'curveU0W0':lowerLayer.edge_22_23,
                      'curveU0W1':upperLayer.edge_22_23,
                      'curveV0W0':lowerLayer.edge_22_27,
                      'curveV0W1':upperLayer.edge_22_27,
                      'curveV1W0':lowerLayer.cyclicUpperCurve,
                      'curveV1W1':upperLayer.cyclicUpperCurve
                      }

        gradingDict = {'U0V0':lowerLayer.gradingQo,
                      'U0V1':lowerLayer.gradingQo,
                      'U0W0':lowerLayer.grading_22_23,
                      'U0W1':upperLayer.grading_22_23,
                      'U1V0':lowerLayer.gradingQo,
                      'U1V1':lowerLayer.gradingQo,
                      'V0W0':lowerLayer.grading_22_27,
                      'V0W1':upperLayer.grading_22_27,
                      'V1W0':lowerLayer.gradingCyclicUpper[3],
                      'V1W1':upperLayer.gradingCyclicUpper[3]
                      }

        shape = (self.cellsMer3 + 1, self.cellsCirc0 + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        gradingBlock = create_grading_block(shape, gradingDict)
        block17 = interpolate_block(shape, curvesDict,
                                    block17Mapping, gradingBlock)
        self.topo.blockList[17 + layerIndex * self.blocksPerLayer] = block17








