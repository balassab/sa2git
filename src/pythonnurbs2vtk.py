#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
# *   Copyright (C) 2013 by Balint Balassa                                  *
# *                                                                         *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 3 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************


import vtk
import os.path




def control_points_2_vtk(nurbs, path='controlPoints.vts', weight=True):
    if path[-1] == 's':
        nU = nurbs.ctrlPnts().rows()
        nV = nurbs.ctrlPnts().cols()
        s = vtk.vtkStructuredGrid()
        s.SetDimensions(nU, nV, 1)
        p = vtk.vtkPoints()
        p.SetNumberOfPoints(nU * nV)
        if weight:
            a = vtk.vtkDoubleArray()
            a.SetNumberOfComponents(1)
            a.SetNumberOfTuples(nU * nV)
            a.SetName('Weight')

        for i in range(nU):
            for j in range(nV):
                x = nurbs.ctrlPnts(i, j).getx()
                y = nurbs.ctrlPnts(i, j).gety()
                z = nurbs.ctrlPnts(i, j).getz()
                counter = j * nU + i
                if weight:
                    w = nurbs.ctrlPnts(i, j).getw()
                    a.SetTuple1(counter, w)
                p.SetPoint(counter, (x, y, z))
        s.SetPoints(p)
        if weight:
            s.GetPointData().AddArray(a)
        write_file(path, s)
        print '%ix%i=%i control points written to %s' % (nU, nV, nU * nV,
                                                         os.path.abspath(path))
    elif path[-1] == 'p':
        nU = nurbs.ctrlPnts().rows()
        p = vtk.vtkPoints()
        p.SetNumberOfPoints(nU)
        if weight:
            a = vtk.vtkDoubleArray()
            a.SetNumberOfComponents(1)
            a.SetNumberOfTuples(nU)
            a.SetName('Weight')
        polyLine = vtk.vtkPolyLine()
        polyLine.GetPointIds().SetNumberOfIds(nU)

        for i in range(nU):
            x = nurbs.ctrlPnts(i).getx()
            y = nurbs.ctrlPnts(i).gety()
            z = nurbs.ctrlPnts(i).getz()
            counter = i
            if weight:
                w = nurbs.ctrlPnts(i).getw()
                a.SetTuple1(counter, w)
            p.SetPoint(counter, (x, y, z))
            polyLine.GetPointIds().SetId(i, i)

        cells = vtk.vtkCellArray()
        cells.InsertNextCell(polyLine)
        polyData = vtk.vtkPolyData()
        polyData.SetPoints(p)
        polyData.SetLines(cells)
        if weight:
            polyData.GetPointData().AddArray(a)
        write_file(path, polyData, polyData=True)
        print '%i curve points written to %s' % (nU, os.path.abspath(path))
    else:
        raise ValueError('path name %s has no correct file ending. \
                          Use .vts for surfaces or .vtp for curves.' % path)


def curve_points_2_vtk(nurbsCurve, nU=100, path='curvePoints.vtp'):
    print 'starting to write curve...'

    p = vtk.vtkPoints()
    p.SetNumberOfPoints(nU)

    a = vtk.vtkDoubleArray()
    a.SetNumberOfComponents(1)
    a.SetNumberOfTuples(nU)
    a.SetName('Weight')

    a2 = vtk.vtkDoubleArray()
    a2.SetNumberOfComponents(1)
    a2.SetNumberOfTuples(nU)
    a2.SetName('u-value')

    polyLine = vtk.vtkPolyLine()
    polyLine.GetPointIds().SetNumberOfIds(nU)

    for i in range(nU):
        u = float(i) / (nU - 1.0)
        point = nurbsCurve.hpointAt(u)
        x = point.getx()
        y = point.gety()
        z = point.getz()
        w = point.getw()
        counter = i
        a.SetTuple1(counter, w)
        a2.SetTuple1(counter, u)
        p.SetPoint(counter, (x, y, z))

        polyLine.GetPointIds().SetId(i, i)

    cells = vtk.vtkCellArray()
    cells.InsertNextCell(polyLine)

    polyData = vtk.vtkPolyData()
    polyData.SetPoints(p)
    polyData.SetLines(cells)
    polyData.GetPointData().AddArray(a)
    polyData.GetPointData().AddArray(a2)
    write_file(path, polyData, polyData=True)
    print '%i curve points written to %s' % (nU, os.path.abspath(path))


def surface_points_2_vtk(nurbsSurface, nU=10, nV=200, path='surfacePoints.vts',
                         weight=True):
    s = vtk.vtkStructuredGrid()
    s.SetDimensions(nU, nV, 1)

    p = vtk.vtkPoints()
    p.SetNumberOfPoints(nU * nV)

    if weight:
        a = vtk.vtkDoubleArray()
        a.SetNumberOfComponents(1)
        a.SetNumberOfTuples(nU * nV)
        a.SetName('Weight')

    a2 = vtk.vtkDoubleArray()
    a2.SetNumberOfComponents(1)
    a2.SetNumberOfTuples(nU * nV)
    a2.SetName('u-value')

    a3 = vtk.vtkDoubleArray()
    a3.SetNumberOfComponents(1)
    a3.SetNumberOfTuples(nU * nV)
    a3.SetName('v-value')

    for i in range(nU):
        u = float(i) / (nU - 1)
        for j in range(nV):
            v = float(j) / (nV - 1)
            if weight:
                point = nurbsSurface.hpointAt(u, v)
                w = point.getw()
            else:
                point = nurbsSurface.pointAt(u, v)
            x = point.getx()
            y = point.gety()
            z = point.getz()
            
            

            counter = j * nU + i
            if weight:
                a.SetTuple1(counter, w)
            a2.SetTuple1(counter, u)
            a3.SetTuple1(counter, v)
            p.SetPoint(counter, (x, y, z))

    s.SetPoints(p)
    if weight:
        s.GetPointData().AddArray(a)
    s.GetPointData().AddArray(a2)
    s.GetPointData().AddArray(a3)
    write_file(path, s)
    print '%ix%i=%i surface points written to %s' % (nU, nV, nU * nV,
                                                     os.path.abspath(path))



def write_file(path, data, polyData=False):
    if not polyData:
        w = vtk.vtkXMLStructuredGridWriter()
    else:
        w = vtk.vtkXMLPolyDataWriter()
    w.SetFileName(path)
    w.SetInput(data)
    w.Write()


