#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
# *   Copyright (C) 2013 by Balint Balassa                                  *
# *                                                                         *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 3 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import os
# import pylab as pl
import numpy as np
from scipy.optimize import brentq

import turboMachine
from pythonnurbs import NurbsCurve, NurbsSurface, NurbsMatrix, NurbsPoint, \
                        NurbsMatrixRT, NurbsVector, NurbsHPoint

from pythonnurbs2vtk import surface_points_2_vtk#, control_points_2_vtk


def transformCoordinates(coordinates, outputCS):
    tCoordinates = np.ones(3, 'd')
    if outputCS == 'xyz':
        tCoordinates[0] = coordinates[0] * np.cos(coordinates[1])
        tCoordinates[1] = coordinates[0] * np.sin(coordinates[1])
        tCoordinates[2] = coordinates[2]
        return tCoordinates
    elif outputCS == 'rpz':
        tCoordinates[0] = np.sqrt(coordinates[0] ** 2.0 + coordinates[1] ** 2.0)
        tCoordinates[1] = np.arctan2(coordinates[1], coordinates[0])
        tCoordinates[2] = coordinates[2]
        return tCoordinates
    elif outputCS == 'm':
        m = np.sqrt(coordinates[0] ** 2.0 + coordinates[1] ** 2.0 +
                    coordinates[2] ** 2.0)
        return m
    else:
        raise ValueError('*** Wrong outputCS! Current outputCS! = \
                            {0}'.format(outputCS))

def set_s1_tangent(startPoint, dPhiR, dM, rotCurve):
    startRpz = transformCoordinates(startPoint, 'rpz')
    r, p, z = startRpz
    uRot = get_uRot_from_rz(rotCurve, r, z)
    derivationVector = NurbsVector.Vector_Point3Dd()
    rotCurve.deriveAt(uRot, 1, derivationVector)
    rotCurveTangent = getPoint(derivationVector[1])
    normRotCurveTan = rotCurveTangent / np.linalg.norm(rotCurveTangent)
    meridianTan = transform_meridian_vector(startRpz, normRotCurveTan, 'xyz')
    radialVec = np.array((startPoint[0], startPoint[1], 0))
    cross = -np.cross(radialVec, meridianTan)
    normCross = cross / np.linalg.norm(cross)
    dPhiXyz = normCross * dPhiR
    dMeridianXyz = meridianTan * dM
    s1TangentXyz = dPhiXyz + dMeridianXyz
    return s1TangentXyz


def transform_meridian_vector(startPoint, vector, outputCS):
    if outputCS == 'xyz':
        rpzEndPoint = startPoint + vector
        xyzEndPoint = transformCoordinates(rpzEndPoint, 'xyz')
        xyzStartPoint = transformCoordinates(startPoint, 'xyz')
        vectorXyz = xyzEndPoint - xyzStartPoint
        return vectorXyz
    elif outputCS == 'rpz':
        raise NotImplementedError()


# from bladedesigner package:
def interpolateNurbs3D(parray, kps=0, degree=3, dim=3, closed=False):
    if len(parray) == 2:
        nurbsCurve = NurbsCurve.NurbsCurved()
        P0 = setPoint(parray[0])
        P1 = setPoint(parray[1])
        nurbsCurve.makeLine(P0, P1, degree)
        return nurbsCurve
    else:
        if kps == 0:
            pts = len(parray)
        else:
            pts = kps

        if pts <= degree:
            degree = pts - 1

        if dim == 3:
            ptsVector = NurbsVector.Vector_Point3Dd(len(parray))
            vector = NurbsVector.Vector_Point3Dd(1)
            for n in range(len(parray)):
                vector.reset(NurbsPoint.Point3Dd(parray[n, 0], parray[n, 1],
                                                 parray[n, 2]))
                ptsVector.as_func(n, vector)
            nurbsCurve = NurbsCurve.NurbsCurved()
            if not closed:
                nurbsCurve.leastSquares(ptsVector, degree, pts)
            else:
                nurbsCurve.leastSquaresClosed(ptsVector, degree, pts)
        elif dim == 4:
            ptsVector = NurbsVector.Vector_HPoint3Dd(len(parray))
            # knotVector = NurbsVector.Vector_DOUBLE(len(parray)+degree+1)
            vector = NurbsVector.Vector_HPoint3Dd(1)
            for n in range(len(parray)):
                pnt = NurbsHPoint.HPoint3Dd()
                pnt.setx(parray[n, 0])
                pnt.sety(parray[n, 1])
                pnt.setz(parray[n, 2])
                pnt.setw(parray[n, 3])
                vector.reset(pnt)
                ptsVector.as_func(n, vector)
                # knotVector[n] = parray[n,3]
            nurbsCurve = NurbsCurve.NurbsCurved()
            if not closed:
                nurbsCurve.globalInterpH(ptsVector, degree)
            else:
                nurbsCurve.globalInterpHClosed(ptsVector, degree)

        return nurbsCurve


# from bladedesigner package:
def interpolateNurbsSurface(pmatrix, du=3, dv=3, kpsu=0, kpsv=0):
    if len(pmatrix) < (du + 1):
        raise Exception, "Number of Points in u-Direction to few for " + \
                         "NURBS-Interpolation with current degree"
    if len(pmatrix[0]) < (dv + 1):
        raise Exception, "Number of Points in v-Direction to few for " + \
                         "NURBS-Interpolation with current degree"
    if kpsu == 0:
        kpsu = len(pmatrix)
    if kpsv == 0:
        kpsv = len(pmatrix[0])
    # Matrix mit 3D-Punkten der Profile
    mpts = NurbsMatrix.Matrix_Point3Dd(len(pmatrix), len(pmatrix[0]))
    for i, pro in enumerate(pmatrix):
        for j, p in enumerate(pro):
            mpts.setelem(i, j, NurbsPoint.Point3Dd(p[0], p[1], p[2]))
    # Erzeuge Nurbs-Flaeche
    surface = NurbsSurface.NurbsSurfaced()
    surface.leastSquares(mpts, du, dv, kpsu, kpsv)
    return surface


def create_nurbs_channel(bladeRow, sampleRate=20):
    interpPoints = np.zeros((len(bladeRow.nurbsRot), sampleRate, 3))
    for i, rot in enumerate(bladeRow.nurbsRot):
        for sample in range(sampleRate):
            u = float(sample) / (sampleRate - 1.0)
            interpPoints[i, sample, :] = getPoint(rot.pointAt(u))
    return interpolateNurbsSurface(interpPoints)


def getPoint(point):
    return np.array([point.getx(), point.gety(), point.getz()])


def setPoint(point):
    return NurbsPoint.Point3Dd(point[0], point[1], point[2])


def setHPoint(hpoint):
    return NurbsHPoint.HPoint3Dd(hpoint[0], hpoint[1], hpoint[2], hpoint[3])


def calculate_phi_range(camber, sampleRate=100):
    samples = np.linspace(0, 1, sampleRate)
    phiArray = np.zeros((sampleRate))
    for i, u in enumerate(samples):
        pointXyz = getPoint(camber.pointAt(u))
        pointRpz = transformCoordinates(pointXyz, 'rpz')
        phiArray[i] = pointRpz[1]
    return phiArray.min(), phiArray.max()


def calculate_phi_cyclic_in_out(camberCurve, relaxLE=0, relaxTE=0):
    phiMin, phiMax = calculate_phi_range(camberCurve)
    deltaPhi = phiMax - phiMin
    LErpz = transformCoordinates(getPoint(camberCurve.pointAt(0)), 'rpz')
    TErpz = transformCoordinates(getPoint(camberCurve.pointAt(1)), 'rpz')
    phiCylcicCenterIn = 0
    if LErpz[1] < phiMin + deltaPhi / 2.0:
        phiCylcicCenterIn = phiMin + relaxLE * deltaPhi
    else:
        phiCylcicCenterIn = phiMax - relaxLE * deltaPhi
    phiCylcicCenterOut = 0
    if TErpz[1] < phiMin + deltaPhi / 2.0:
        phiCylcicCenterOut = phiMin + relaxTE * deltaPhi
    else:
        phiCylcicCenterOut = phiMax - relaxTE * deltaPhi
    return phiCylcicCenterIn, phiCylcicCenterOut


def get_phi_camber(camberCurve, u=0.5):
    xyz = getPoint(camberCurve.pointAt(u))
    rpz = transformCoordinates(xyz, 'rpz')
    return rpz[1]


def get_relaxed_phi_camber(camberCurve, rotationCurve, uCamber=0.5, relax=0.9):
    phiCamber = get_phi_camber(camberCurve, u=uCamber)
    phiIn, phiOut = calculate_phi_cyclic_in_out(camberCurve)
    deltaPhiInOut = phiOut - phiIn
    uRot = get_uRot_from_uCurve(camberCurve, uCamber, rotationCurve)
    phiInOutLinAv = phiIn + uRot * deltaPhiInOut
    relaxedPhi = relax * phiCamber + (1.0 - relax) * phiInOutLinAv
    return relaxedPhi, uRot




def get_uRot_from_rz(rotationCurve, r, z, guessU=0.5, error=1e-6, s=1, sep=15,
                     maxIter=15, um=0, uM=1):
    rpz = np.array((r, 0, z))
    rzNpp = setPoint(transformCoordinates(rpz, 'xyz'))
    uRotCamber = NurbsCurve.doublep()
    uRotCamber.assign(guessU)
    rotationCurve.minDist2(rzNpp, uRotCamber, error, s, sep, maxIter, um, uM)
    return uRotCamber.value()


def get_uRot_from_uCurve(curve, uValue, rotationCurve, guessU=0.5):
    pointNpp = curve.pointAt(uValue)
    point = getPoint(pointNpp)
    pointRpz = transformCoordinates(point, 'rpz')
    r, z = pointRpz[0], pointRpz[2]
    return get_uRot_from_rz(rotationCurve, r, z, guessU=guessU)


def get_cyclic_tangent_rpz(camberCurve, rotationCurve, side='inlet',
                       lengthFactor=0.04):
    if side == 'inlet':
        uRot = 0
        uCamber = 0
    elif side == 'outlet':
        uRot = 1
        uCamber = 1
    uRotCamber = get_uRot_from_uCurve(camberCurve, uCamber, rotationCurve)
    if side == 'inlet':
        distanceSide2Camber = rotationCurve.lengthIn(0, uRotCamber)
    elif side == 'outlet':
        distanceSide2Camber = rotationCurve.lengthIn(uRotCamber, 1)
    rotDerivative = getPoint(rotationCurve.derive3D(uRot, 1))
    xyzNorm = rotDerivative / np.linalg.norm(rotDerivative)
    xyz = xyzNorm * lengthFactor * distanceSide2Camber
    tangentVector = setPoint(xyz)
    return tangentVector


def project_curve_onto_channel(curve, channelCurve, sampleRate=20, um=0, uM=1,
                               ignore=[]):
    points = project_curve_points_onto_channel(curve, channelCurve,
                                               sampleRate=sampleRate,
                                               um=um, uM=uM, ignore=ignore)
    return interpolateNurbs3D(points)


def get_rpz_from_normed_phi_m(phiNorm, m, rotCurve, cyclicCenter, bladeNumber):
    uRot = root_finding_length(rotCurve, m, 0, 1)
    r, phi, z = transformCoordinates(getPoint(rotCurve.pointAt(uRot)), 'rpz')
    uCyclic = find_uCyclic_from_rz(r, z, cyclicCenter, us=0, ue=1)
    xyzCyclicCenter = getPoint(cyclicCenter.pointAt(uCyclic))
    rpzCyclicCenter = transformCoordinates(xyzCyclicCenter, 'rpz')
    phi0 = rpzCyclicCenter[1] - np.pi / float(bladeNumber)
    deltaPhi = 2.0 * np.pi / float(bladeNumber)
    phi = phi0 + phiNorm * deltaPhi
    return r, phi, z


def root_func_rz(u, curve, targetRZ):
    xyzCurrentPoint = getPoint(curve.pointAt(u))
    rpzCurrentPoint = transformCoordinates(xyzCurrentPoint, 'rpz')
    currentRZ = np.sqrt(rpzCurrentPoint[0] ** 2 + rpzCurrentPoint[2] ** 2)
    diff = targetRZ - currentRZ
    return diff


def find_uCyclic_from_rz(r, z, cyclicCurve, us=0, ue=1):
    targetRZ = np.sqrt(r ** 2 + z ** 2)
    u = brentq(root_func_rz, us, ue, args=(cyclicCurve, targetRZ))
    return u


def root_func_length(u, curve, us, targetLength):
    currentLength = curve.lengthIn(us, u)
    diff = targetLength - currentLength
    return diff


def root_finding_length(curve, paramLength, us, ue):
    targetLength = curve.length() * paramLength
    return brentq(root_func_length, us, ue, args=(curve, us, targetLength))


def discretize_curve_equidistantly(curve, us=0, ue=1, sampleRate=20):
    uList = []
    #totalLength = curve.lengthIn(us, ue)
    for sample in np.linspace(us, ue, sampleRate):
        if us < sample < ue:
            #targetLength = sample * totalLength
            #u = brentq(root_func_length, us, ue, args=(curve, us, targetLength))
            u = root_finding_length(curve, sample, us, ue)
            uList.append(u)
        elif sample == us:
            uList.append(sample)
        elif sample == ue:
            uList.append(sample)
    return uList



def project_curve_points_onto_channel(curve, channelCurve, sampleRate=20,
                                      preciseDiscretization=False, um=0, uM=1,
                                      ignore=[]):
    projectedPoints = np.zeros((sampleRate, 3))
    if not preciseDiscretization:
        uList = np.linspace(0, 1, sampleRate)
    else:
        uList = discretize_curve_equidistantly(curve, sampleRate=sampleRate)
    start = 0
    end = sampleRate
    if 0 in ignore:
        start += 1
    if -1 in ignore:
        end -= 1
    #for i, u in enumerate(uList):
    for i in range(start, end):
        u = uList[i]
        curvePointXyz = getPoint(curve.pointAt(u))
        curvePointRpz = transformCoordinates(curvePointXyz, 'rpz')
        r, z = curvePointRpz[0], curvePointRpz[2]
        uChannel = get_uRot_from_rz(channelCurve, r, z, um=um, uM=uM)
        channelPointXyz = getPoint(channelCurve.pointAt(uChannel))
        channelPointRpz = transformCoordinates(channelPointXyz, 'rpz')
        # using channel rz coordinates:
        projectedCurvePointRpz = channelPointRpz
        # setting phi from original curve:
        projectedCurvePointRpz[1] = curvePointRpz[1]
        projectedCurvePointXyz = transformCoordinates(projectedCurvePointRpz,
                                                      'xyz')
        projectedPoints[i] = projectedCurvePointXyz
    if 0 in ignore:
        projectedPoints[0] = getPoint(curve.pointAt(0))
    if -1 in ignore:
        projectedPoints[-1] = getPoint(curve.pointAt(1))
    return projectedPoints


def rotate_cyclic_curves_from_center(centerCurve, bladeNumber):
    lowerCyclicNurbs = NurbsCurve.NurbsCurved(centerCurve)
    mRT = NurbsMatrixRT.MatrixRTd()
    mRT.rotate(0, 0, np.pi / bladeNumber)
    lowerCyclicNurbs.transform(mRT)
    upperCyclicNurbs = NurbsCurve.NurbsCurved(centerCurve)
    mRT.rotate(0, 0, -np.pi / bladeNumber)
    upperCyclicNurbs.transform(mRT)
    return lowerCyclicNurbs, upperCyclicNurbs


def create_s1_surface(upperCyclic, numberOfBlades):
    s1Nurbs = NurbsSurface.NurbsSurfaced()
    theta = 2.0 * np.pi / float(numberOfBlades)
    axisPoint = setPoint((0, 0, 0))
    axisTangent = setPoint((0, 0, 1))
    # TODO: nurbs++ function not correct!
    s1Nurbs.makeFromRevolution(upperCyclic, axisPoint, axisTangent, theta)
    return s1Nurbs


def rotate_cylcic_surface_from_center(centerSurface, bladeNumber):
    lowerCyclicNurbs = NurbsSurface.NurbsSurfaced(centerSurface)
    mRT = NurbsMatrixRT.MatrixRTd()
    mRT.rotate(0, 0, np.pi / bladeNumber)
    lowerCyclicNurbs.transform(mRT)
    upperCyclicNurbs = NurbsSurface.NurbsSurfaced(centerSurface)
    mRT.rotate(0, 0, -np.pi / bladeNumber)
    upperCyclicNurbs.transform(mRT)
    return lowerCyclicNurbs, upperCyclicNurbs


def set_inlet_outlet_block_u_value(blockNormLength, uValues,
                                   index, rotCurve):
    uValues.append(root_finding_length(rotCurve, blockNormLength, 0, 1))


def create_central_cyclic_surface(camberSurface, channelSurface, layers,
                                  vRes=20, relaxLE=None, relaxTE=None,
                                  inletBlockNormLength=0,
                                  outletBlockNormLength=0,
                                  uValuesInlet=None, uValuesOutlet=None):
    if relaxLE == None:
        relaxLE = [0] * len(layers)
    if relaxTE == None:
        relaxTE = [0] * len(layers)
    if len(layers) != len(relaxLE) or len(layers) != len(relaxTE):
        raise IndexError('Length of relaxLE and relaxTE lists must be equal ' +
                         'to length of layers list!')
    pArray = np.zeros((len(layers), vRes, 3))
    inletblockU = 0
    outletblockU = 1
    for k, layer in enumerate(layers):
        camberCurve = NurbsCurve.NurbsCurved()
        camberSurface.isoCurveU(layer, camberCurve)
        rotCurve = NurbsCurve.NurbsCurved()
        channelSurface.isoCurveU(layer, rotCurve)

        if inletBlockNormLength > 0:
            set_inlet_outlet_block_u_value(inletBlockNormLength,
                                           uValuesInlet, k, rotCurve)
            inletblockU = uValuesInlet[k]
        if outletBlockNormLength > 0:
            set_inlet_outlet_block_u_value(1 - outletBlockNormLength,
                                           uValuesOutlet, k, rotCurve)
            outletblockU = uValuesOutlet[k]

        cyclicRes = 5
        cyclic = np.zeros((cyclicRes, 3))
        phiIn, phiOut = calculate_phi_cyclic_in_out(camberCurve,
                                                    relaxLE=relaxLE[k],
                                                    relaxTE=relaxTE[k])
        tmpPoint0 = getPoint(rotCurve.pointAt(inletblockU))
        cyclic[0] = transformCoordinates(tmpPoint0, 'rpz')
        cyclic[0, 1] = phiIn
        tmpPointLast = getPoint(rotCurve.pointAt(outletblockU))
        cyclic[-1] = transformCoordinates(tmpPointLast, 'rpz')
        cyclic[-1, 1] = phiOut

        phiCenter, uCenter = get_relaxed_phi_camber(camberCurve, rotCurve,
                                                    uCamber=0.1)
        tmpPoint1 = getPoint(rotCurve.pointAt(uCenter))
        cyclic[1] = transformCoordinates(tmpPoint1, 'rpz')
        cyclic[1, 1] = phiCenter

        phiCenter, uCenter = get_relaxed_phi_camber(camberCurve, rotCurve,
                                                    uCamber=0.5)
        tmpPoint2 = getPoint(rotCurve.pointAt(uCenter))
        cyclic[2] = transformCoordinates(tmpPoint2, 'rpz')
        cyclic[2, 1] = phiCenter

        phiCenter, uCenter = get_relaxed_phi_camber(camberCurve, rotCurve,
                                                    uCamber=0.9)
        tmpPoint2 = getPoint(rotCurve.pointAt(uCenter))
        cyclic[3] = transformCoordinates(tmpPoint2, 'rpz')
        cyclic[3, 1] = phiCenter

        for i in range(cyclicRes):
            cyclic[i] = transformCoordinates(cyclic[i], 'xyz')
        cyclicNurbs = interpolateNurbs3D(cyclic)

        inletTangent = getPoint(get_cyclic_tangent_rpz(camberCurve, rotCurve,
                                                       side='inlet'))
        outletTangent = getPoint(get_cyclic_tangent_rpz(camberCurve, rotCurve,
                                                        side='outlet'))
        inletPoint = getPoint(cyclicNurbs.pointAt(0))
        outletPoint = getPoint(cyclicNurbs.pointAt(1))
        inletTangentXyz = transform_meridian_vector(inletPoint, inletTangent, 'xyz')
        outletTangentXyz = transform_meridian_vector(outletPoint, outletTangent, 'xyz')
        cyclicNurbs.setTangent(0, setPoint(inletTangentXyz))
        cyclicNurbs.setTangent(1, setPoint(outletTangentXyz))

        # control_points_2_vtk(cyclicNurbs, path='controlPointsCyclic.vtp')

        projectedPoints = project_curve_points_onto_channel(cyclicNurbs,
                                                            rotCurve,
                                                            sampleRate=vRes)
        pArray[k] = projectedPoints
    degreeU = 3
    if len(layers) == 3:
        degreeU = 2
    return interpolateNurbsSurface(pArray, du=degreeU)



if __name__ == '__main__':
    workdir = os.path.abspath('/home/belenon/Dokumente/SA1/cax')
    workdir = os.path.abspath('/home/belenon/Dokumente/SA1/cu_Over_r')
    turbo = turboMachine.turboMachine(workdir)
    turbo.listRows[0].useNurbsXml = True
    turbo.listRows[1].useNurbsXml = True
    turbo.computeTurboMachine()


    rotor = turbo.listRows[0]
    rotor.calculate_camber_surface()
    cs = rotor.camberSurface
    channel = create_nurbs_channel(rotor)

    cyclicSurface = create_central_cyclic_surface(cs, channel,
                                                layers=(0, 0.25, 0.5, 0.75, 1))
    surface_points_2_vtk(cyclicSurface, path='cyclicCenterSurface.vts')
    #control_points_2_vtk(cyclicSurface, path='controlPointsCyclic.vts')

    upperS , lowerS = rotate_cylcic_surface_from_center(cyclicSurface, 28)
    surface_points_2_vtk(upperS, path='upperS.vts')
    surface_points_2_vtk(lowerS, path='lowerS.vts')
    #surface_points_2_vtk(rotor.bladeSurface, path='blade.vts')


