#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2013 by Balint Balassa                                  *
#*                                                                         *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import numpy as np

from pythonnurbs2vtk import surface_points_2_vtk, control_points_2_vtk
from topo import Topo, create_connection
from foamMeshWriter import FoamMeshWriter
from cgnsWriter import CgnsWriter

from pythonnurbs import NurbsCurve, NurbsSurface

from test_bd import getPoint, setPoint, interpolateNurbs3D, setHPoint
from transfinite import edge, interpolate_block_nurbs_surface


def create_cylinder():
    axisStart = (0, 0, 0)
    axisEnd = (0, 0, 1)
    axisStartNpp = setPoint(axisStart)
    axisEndNpp = setPoint(axisEnd)
    profileStart = np.array((1, 0, 0))
    profileEnd = np.array((1, 0, 1))
    
    pArray = np.zeros((5, 3))
    pArray[0] = profileStart
    pArray[1] = 0.75 * profileStart + 0.25 * profileEnd
    pArray[2] = 0.5 * profileStart + 0.5 * profileEnd
    pArray[3] = 0.25 * profileStart + 0.75 * profileEnd
    pArray[4] = profileEnd
    profile = interpolateNurbs3D(pArray)

    cylinder = NurbsSurface.NurbsSurfaced()
    cylinder.makeFromRevolution(profile, axisStartNpp, axisEndNpp)
    
    return cylinder


def modify_cylinder(cylinder):
    i, j = 4, 1
    deltaHPoint = setHPoint((-0.3, 0, 0, 0))
    cylinder.modCPby(i, j, deltaHPoint)
    return cylinder



class LayerBoundaries(object):

    def __init__(self, span, cylinder):
        self.cylinder = cylinder
        self.span = span

        self.inletCurve = None
        self.outletCurve = None
        self.sideUpperCurve=  None
        self.sideLowerCurve=  None

        self.gradingQo = None
        
        self.set_curves()

    def set_curves(self):
        self.inletCurve = edge((-2, -2, self.span), (-2, 2, self.span), 3)
        self.outletCurve = edge((2, -2, self.span), (2, 2, self.span), 3)
        self.sideUpperCurve = edge((-2, 2, self.span), (2, 2, self.span), 3)
        self.sideLowerCurve = edge((-2, -2, self.span), (2, -2, self.span), 3)
        
        self.circle = NurbsCurve.NurbsCurved()
        self.cylinder.isoCurveV(self.span, self.circle)



class LayerTopo(LayerBoundaries):
    def __init__(self, span, cylinder):

        super(LayerTopo, self).__init__(span, cylinder)
        
        self.circleDivision = [0.125, 0.325, 0.675, 0.875]
        self.points = np.zeros((10, 3))
        self.intRes = 3
    
    def __call__(self):
        self.set_edges()
    
    def set_points(self):
        
        point0Npp = self.inletCurve.pointAt(0)
        self.points[0] = getPoint(point0Npp)
        
        point1Npp = self.circle.pointAt(self.circleDivision[2])
        self.points[1] = getPoint(point1Npp)
        
        point2Npp = self.circle.pointAt(self.circleDivision[1])
        self.points[2] = getPoint(point2Npp)
        
        point3Npp = self.inletCurve.pointAt(1)
        self.points[3] = getPoint(point3Npp)
        
        point4Npp = self.outletCurve.pointAt(0)
        self.points[4] = getPoint(point4Npp)
        
        point5Npp = self.circle.pointAt(self.circleDivision[3])
        self.points[5] = getPoint(point5Npp)
        
        point6Npp = self.circle.pointAt(0)
        self.points[6] = getPoint(point6Npp)
        
        point7Npp = self.outletCurve.pointAt(0.5)
        self.points[7] = getPoint(point7Npp)
        
        point8Npp = self.circle.pointAt(self.circleDivision[0])
        self.points[8] = getPoint(point8Npp)
        
        point9Npp = self.outletCurve.pointAt(1)
        self.points[9] = getPoint(point9Npp)


    def set_edges(self):
        if (self.points != 0).sum() == 0:
            self.set_points()

        self.set_edge_0_1()
        self.set_edge_2_3()
        self.set_edge_4_5()
        self.set_edge_6_7()
        self.set_edge_8_9()

    def set_edge_0_1(self):
        start = self.points[0]
        end = self.points[1]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_0_1 = tmpEdge

    def set_edge_2_3(self):
        start = self.points[2]
        end = self.points[3]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_2_3 = tmpEdge

    def set_edge_4_5(self):
        start = self.points[4]
        end = self.points[5]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_4_5 = tmpEdge

    def set_edge_6_7(self):
        start = self.points[6]
        end = self.points[7]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_6_7 = tmpEdge

    def set_edge_8_9(self):
        start = self.points[8]
        end = self.points[9]
        tmpEdge = edge(start, end, self.intRes)
        self.edge_8_9 = tmpEdge



class CircleTopo():
    
    def __init__(self, cylinder):
        self.cylinder = cylinder
        self.blocksPerLayer = 5
        self.outletUvalues = []
        self.spans = []
        self.layers = []
        
        self.topo = Topo()
        
        self.cellsInlet = 2
        self.cellsSideLower = 2
        self.cellsSideUpper = 2
        self.cellsOutletLower = 2
        self.cellsOutletUpper = 2
        self.cellsO = 2
        self.cellsRad = []
        
        self.qo0 = None
        self.qo1 = None
        self.qo2 = None
        self.qo3 = None
        self.qo4 = None
        self.qo5 = None
        self.qo6 = None
        self.qo7 = None
        self.qo8 = None
        self.qo9 = None

    def __call__(self, cgns=False):
        self.set_QOs()
        self.set_blocks()
        for layerIndex in range(len(self.spans) - 1):
            self.set_connections(layerIndex)
        self.topo.check_connectivity()

        if not cgns:
            self.write_O10H()
        else:
            self.write_cgns()

    def create_layers(self):
        for span in self.spans:
            self.layers.append(LayerTopo(span, self.cylinder))

    def write_O10H(self):
        f = FoamMeshWriter()
        f.topo = self.topo
        f.case = '../circle'
        f()
        print '\nFinished!'
    
    def write_cgns(self):
        c = CgnsWriter(self.topo)
        c.write_cgns_file()
        print '\nFinished!'



    def set_QOs(self):
        self.set_qo0()
        self.set_qo1()
        self.set_qo2()
        self.set_qo3()
        self.set_qo4()
        self.set_qo5()
        self.set_qo6()
        self.set_qo7()
        self.set_qo8()
        self.set_qo9()

    def set_qo0(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.inletCurve.pointAt(0)
            pArray[i] = getPoint(pointNpp)
        self.qo0 = interpolateNurbs3D(pArray)
    
    def set_qo1(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.circle.pointAt(layer.circleDivision[2])
            pArray[i] = getPoint(pointNpp)
        self.qo1 = interpolateNurbs3D(pArray)

    def set_qo2(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.circle.pointAt(layer.circleDivision[1])
            pArray[i] = getPoint(pointNpp)
        self.qo2 = interpolateNurbs3D(pArray)
    
    def set_qo3(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.inletCurve.pointAt(1)
            pArray[i] = getPoint(pointNpp)
        self.qo3 = interpolateNurbs3D(pArray)

    def set_qo4(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.outletCurve.pointAt(0)
            pArray[i] = getPoint(pointNpp)
        self.qo4 = interpolateNurbs3D(pArray)

    def set_qo5(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.circle.pointAt(layer.circleDivision[3])
            pArray[i] = getPoint(pointNpp)
        self.qo5 = interpolateNurbs3D(pArray)
        
    def set_qo6(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.circle.pointAt(0)
            pArray[i] = getPoint(pointNpp)
        self.qo6 = interpolateNurbs3D(pArray)

    def set_qo7(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.outletCurve.pointAt(0.5)
            pArray[i] = getPoint(pointNpp)
        self.qo7 = interpolateNurbs3D(pArray)

    def set_qo8(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.circle.pointAt(layer.circleDivision[0])
            pArray[i] = getPoint(pointNpp)
        self.qo8 = interpolateNurbs3D(pArray)
    
    def set_qo9(self):
        pArray = np.zeros((len(self.layers), 3))
        for i, layer in enumerate(self.layers):
            pointNpp = layer.outletCurve.pointAt(1)
            pArray[i] = getPoint(pointNpp)
        self.qo9 = interpolateNurbs3D(pArray)


    def set_blocks(self):
        self.topo.blockList = (len(self.layers) - 1) * self.blocksPerLayer * \
                                [None]
        for layerIndex in  range(len(self.layers) - 1):
            self.set_blocks_of_layer(layerIndex)



    def set_blocks_of_layer(self, layerIndex):
        self.set_block_0_nurbs(layerIndex)
        self.set_block_1_nurbs(layerIndex)
        self.set_block_2_nurbs(layerIndex)
        self.set_block_3_nurbs(layerIndex)
        self.set_block_4_nurbs(layerIndex)



    def set_block_0_nurbs(self, layerIndex):
        # ======= BLOCK 0 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        curvesDict = {'U0V0':self.qo0,
                      'U0V1':self.qo3,
                      'U0W0':lowerLayer.inletCurve,
                      'U0W1':upperLayer.inletCurve,
                      'U1V0':self.qo1,
                      'U1V1':self.qo2,
                      'U1W0':lowerLayer.circle,
                      'U1W1':upperLayer.circle,
                      'V0W0':lowerLayer.edge_0_1,
                      'V0W1':upperLayer.edge_0_1,
                      'V1W0':lowerLayer.edge_2_3,
                      'V1W1':upperLayer.edge_2_3
                      }
        
        block0Mapping = {'U1W0':lambda x:lowerLayer.circleDivision[2] -
                            x * (lowerLayer.circleDivision[2] -
                                 lowerLayer.circleDivision[1]),
                         'U1W1':lambda x:upperLayer.circleDivision[2] -
                            x * (upperLayer.circleDivision[2] -
                                 upperLayer.circleDivision[1]),
                         'V1W0':lambda x:1.0-x,
                         'V1W1':lambda x:1.0-x,
                         }

        shape = (self.cellsO + 1, self.cellsInlet + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        surfaceDict = {'U1':self.cylinder}
        switchUV = {}#'U1':False}
        master = 'U1'
        cornerU0V0 = [lowerLayer.circleDivision[2], lowerLayer.span]
        cornerU0V1 = [upperLayer.circleDivision[2], upperLayer.span]
        cornerU1V0 = [lowerLayer.circleDivision[1], lowerLayer.span]
        cornerU1V1 = [upperLayer.circleDivision[1], upperLayer.span]
        quadBoundaries = {'U1':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block0 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block0Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 None,
                                                 switchUV)
        self.topo.blockList[0 + layerIndex * self.blocksPerLayer] = block0


    def set_block_1_nurbs(self, layerIndex):
        # ======= BLOCK 1 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        curvesDict = {'U0V0':self.qo0,
                      'U0V1':self.qo1,
                      'U0W0':lowerLayer.edge_0_1,
                      'U0W1':upperLayer.edge_0_1,
                      'U1V0':self.qo4,
                      'U1V1':self.qo5,
                      'V1W0':lowerLayer.circle,
                      'V1W1':upperLayer.circle,
                      'V0W0':lowerLayer.sideLowerCurve,
                      'V0W1':upperLayer.sideLowerCurve,
                      'U1W0':lowerLayer.edge_4_5,
                      'U1W1':upperLayer.edge_4_5
                      }
        
        block0Mapping = {'V1W0':lambda x:lowerLayer.circleDivision[2] +
                            x * (lowerLayer.circleDivision[3] -
                                 lowerLayer.circleDivision[2]),
                         'V1W1':lambda x:upperLayer.circleDivision[2] +
                            x * (upperLayer.circleDivision[3] -
                                 upperLayer.circleDivision[2]),
                         }

        shape = (self.cellsSideLower + 1, self.cellsO + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        surfaceDict = {'V1':self.cylinder}
        switchUV = {}
        master = 'V1'
        cornerU0V0 = [lowerLayer.circleDivision[2], lowerLayer.span]
        cornerU0V1 = [upperLayer.circleDivision[2], upperLayer.span]
        cornerU1V0 = [lowerLayer.circleDivision[3], lowerLayer.span]
        cornerU1V1 = [upperLayer.circleDivision[3], upperLayer.span]
        quadBoundaries = {'V1':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block1 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block0Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 None,
                                                 switchUV)
        self.topo.blockList[1 + layerIndex * self.blocksPerLayer] = block1



    def set_block_2_nurbs(self, layerIndex):
        # ======= BLOCK 2 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        curvesDict = {'U0V0':self.qo2,
                      'U0V1':self.qo3,
                      'U0W0':lowerLayer.edge_2_3,
                      'U0W1':upperLayer.edge_2_3,
                      'U1V0':self.qo8,
                      'U1V1':self.qo9,
                      'V1W0':lowerLayer.sideUpperCurve,
                      'V1W1':upperLayer.sideUpperCurve,
                      'V0W0':lowerLayer.circle,
                      'V0W1':upperLayer.circle,
                      'U1W0':lowerLayer.edge_8_9,
                      'U1W1':upperLayer.edge_8_9
                      }
        
        block0Mapping = {'V0W0':lambda x:lowerLayer.circleDivision[1] -
                            x * (lowerLayer.circleDivision[1] -
                                 lowerLayer.circleDivision[0]),
                         'V0W1':lambda x:upperLayer.circleDivision[1] -
                            x * (upperLayer.circleDivision[1] -
                                 upperLayer.circleDivision[0]),
                         }

        shape = (self.cellsSideUpper + 1, self.cellsO + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        surfaceDict = {'V0':self.cylinder}
        switchUV = {}
        master = 'V0'
        cornerU0V0 = [lowerLayer.circleDivision[1], lowerLayer.span]
        cornerU0V1 = [upperLayer.circleDivision[1], upperLayer.span]
        cornerU1V0 = [lowerLayer.circleDivision[0], lowerLayer.span]
        cornerU1V1 = [upperLayer.circleDivision[0], upperLayer.span]
        quadBoundaries = {'V0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block2 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block0Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 None,
                                                 switchUV)
        self.topo.blockList[2 + layerIndex * self.blocksPerLayer] = block2


    def set_block_3_nurbs(self, layerIndex):
        # ======= BLOCK 3 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        curvesDict = {'U0V0':self.qo5,
                      'U0V1':self.qo6,
                      'U0W0':lowerLayer.circle,
                      'U0W1':upperLayer.circle,
                      'U1V0':self.qo4,
                      'U1V1':self.qo7,
                      'V1W0':lowerLayer.edge_6_7,
                      'V1W1':upperLayer.edge_6_7,
                      'V0W0':lowerLayer.edge_4_5,
                      'V0W1':upperLayer.edge_4_5,
                      'U1W0':lowerLayer.outletCurve,
                      'U1W1':upperLayer.outletCurve
                      }
        
        block0Mapping = {'U0W0':lambda x:lowerLayer.circleDivision[3] +
                            x * (1.0 - lowerLayer.circleDivision[3]),
                         'U0W1':lambda x:upperLayer.circleDivision[3] +
                            x * (1.0 - upperLayer.circleDivision[3]),
                        'V0W0':lambda x:1.0-x,
                        'V0W1':lambda x:1.0-x,
                         }

        shape = (self.cellsO + 1, self.cellsOutletLower + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        surfaceDict = {'U0':self.cylinder}
        switchUV = {}
        master = 'U0'
        cornerU0V0 = [lowerLayer.circleDivision[3], lowerLayer.span]
        cornerU0V1 = [upperLayer.circleDivision[3], upperLayer.span]
        cornerU1V0 = [1.0, lowerLayer.span]
        cornerU1V1 = [1.0, upperLayer.span]
        quadBoundaries = {'U0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block3 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block0Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 None,
                                                 switchUV)
        self.topo.blockList[3 + layerIndex * self.blocksPerLayer] = block3


    def set_block_4_nurbs(self, layerIndex):
        # ======= BLOCK 4 =================
        lowerLayer = self.layers[layerIndex]
        upperLayer = self.layers[layerIndex + 1]

        curvesDict = {'U0V0':self.qo6,
                      'U0V1':self.qo8,
                      'U0W0':lowerLayer.circle,
                      'U0W1':upperLayer.circle,
                      'U1V0':self.qo7,
                      'U1V1':self.qo9,
                      'V1W0':lowerLayer.edge_8_9,
                      'V1W1':upperLayer.edge_8_9,
                      'V0W0':lowerLayer.edge_6_7,
                      'V0W1':upperLayer.edge_6_7,
                      'U1W0':lowerLayer.outletCurve,
                      'U1W1':upperLayer.outletCurve
                      }
        
        block0Mapping = {'U0W0':lambda x:x * lowerLayer.circleDivision[0],
                         'U0W1':lambda x:x * upperLayer.circleDivision[0],
                         }

        shape = (self.cellsO + 1, self.cellsOutletUpper + 1,
                 self.cellsRad[layerIndex] + 1, 3)
        surfaceDict = {'U0':self.cylinder}
        switchUV = {}
        master = 'U0'
        cornerU0V0 = [0.0, lowerLayer.span]
        cornerU0V1 = [0.0, upperLayer.span]
        cornerU1V0 = [lowerLayer.circleDivision[0], lowerLayer.span]
        cornerU1V1 = [lowerLayer.circleDivision[0], upperLayer.span]
        quadBoundaries = {'U0':[(cornerU0V0, cornerU0V1), 
                                (cornerU1V0, cornerU1V1), 
                                (cornerU0V0, cornerU1V0), 
                                (cornerU0V1, cornerU1V1)]}
        block4 = interpolate_block_nurbs_surface(shape, curvesDict,
                                                 block0Mapping, surfaceDict,
                                                 master, quadBoundaries, 
                                                 None,
                                                 switchUV)
        self.topo.blockList[4 + layerIndex * self.blocksPerLayer] = block4



    def set_connections(self, layerIndex):

        maxIndex = len(self.spans) - 1
        blocksPerLayer = len(self.topo.blockList) / maxIndex
        conn0 = create_connection()
        conn0['j-'] = [1 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn0['j+'] = [2 + layerIndex * blocksPerLayer, 'j-', 'k+']
        conn0 = self.set_qo_connection(conn0, 0, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn0)
        
        conn1 = create_connection()
        conn1['i-'] = [0 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn1['i+'] = [3 + layerIndex * blocksPerLayer, 'i-', 'k+']
        conn1 = self.set_qo_connection(conn1, 1, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn1)
        
        conn2 = create_connection()
        conn2['i-'] = [0 + layerIndex * blocksPerLayer, 'i-', 'k+']
        conn2['i+'] = [4 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn2 = self.set_qo_connection(conn2, 2, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn2)
        
        conn3 = create_connection()
        conn3['j-'] = [1 + layerIndex * blocksPerLayer, 'j-', 'k+']
        conn3['j+'] = [4 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn3 = self.set_qo_connection(conn3, 3, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn3)
        
        conn4 = create_connection()
        conn4['j-'] = [3 + layerIndex * blocksPerLayer, 'i+', 'k+']
        conn4['j+'] = [2 + layerIndex * blocksPerLayer, 'j+', 'k+']
        conn4 = self.set_qo_connection(conn4, 4, layerIndex,
                                       blocksPerLayer, maxIndex)
        self.topo.blockConnections.append(conn4)
        


    def set_qo_connection(self, conn, blockIndex, layerIndex,
                          blocksPerLayer, maxIndex):
        if maxIndex > 1:
            if layerIndex < maxIndex - 1:
                conn['k+'] = [blockIndex + (layerIndex + 1) * blocksPerLayer,
                              'i+', 'j+']
            if 0 < layerIndex < maxIndex:
                conn['k-'] = [blockIndex + (layerIndex - 1) * blocksPerLayer,
                              'i+', 'j+']
        return conn



if __name__ == '__main__':
    cylinder = create_cylinder()
    #circle = NurbsCurve.NurbsCurved()
    #cylinder.isoCurveV(0, circle)
    cylinder = modify_cylinder(cylinder)


    #'''    SWITCH HERE!
    mesh = CircleTopo(cylinder)
    mesh.spans = [0, 1]
    mesh.create_layers()
    mesh.cellsRad = [10]
    mesh.cellsInlet = 15
    mesh.cellsSideLower = 6
    mesh.cellsSideUpper = 4
    mesh.cellsO = 4
    mesh.layers[0].circleDivision = [0.125, 0.375, 0.625, 0.875]
    mesh.layers[1].circleDivision = [0.125, 0.375, 0.625, 0.875]
    mesh.layers[0]()
    mesh.layers[1]()
    
    mesh()
    #'''
    
    
    surface_points_2_vtk(cylinder, nU=200, nV=10, weight=False)
    control_points_2_vtk(cylinder, weight=True)
    
    print 'All Done!!!'




