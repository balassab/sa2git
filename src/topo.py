#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module Docstring
Docstrings: http://www.python.org/dev/peps/pep-0257/
"""

#***************************************************************************
# *   Copyright (C) 2013 by Balint Balassa                                  *
# *                                                                         *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 3 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import sys

import numpy as np



class Topo():
    '''Topo Class for defining mesh topologies'''

    def __init__(self):
        self.blockList = []
        self.blockConnections = []

    # TODO: Point count not correct in some cases!!!
    def count_points_faces_etc(self):
        '''Counting points, faces and boundary faces'''
        points = 0
        cells = 0
        faces = 0
        boundaryFaces = 0
        # first count for each block without considering neighbours:
        for block in self.blockList:
            i, j, k = block.shape[0:3]
            points += i * j * k
            cells += (i - 1) * (j - 1) * (k - 1)
            faces += (i - 1) * (j - 1) * k + ((i - 1) * j * (k - 1) +
                                              i * (j - 1) * (k - 1))
            boundaryFaces += (i - 1) * (j - 1) * 2 + ((i - 1) * (k - 1) * 2 +
                                                      (j - 1) * (k - 1) * 2)
        print points, cells, faces, boundaryFaces
        # CONSIDER CORRECTION FOR BORDERING BLOCKS:
        for blockIndex in range(len(self.blockConnections)):
            conn = self.blockConnections[blockIndex]
            block = self.blockList[blockIndex]
            i, j, k = block.shape[0:3]
            # iterate over all 6 sides:
            for side, neighbour in conn.items():
                # if current side has neighbour:
                if type(neighbour) == type([]):
                    # if neighbour has not already been considered:
                    if neighbour[0] > blockIndex:
                        if side[0] == 'i':
                            points -= j * k
                            faces -= (j - 1) * (k - 1)
                            boundaryFaces -= 2 * (j - 1) * (k - 1)
                        elif side[0] == 'j':
                            points -= i * k
                            faces -= (i - 1) * (k - 1)
                            boundaryFaces -= 2 * (i - 1) * (k - 1)
                        elif side[0] == 'k':
                            points -= i * j
                            faces -= (i - 1) * (j - 1)
                            boundaryFaces -= 2 * (i - 1) * (j - 1)
        print points, cells, faces, boundaryFaces
        return points, cells, faces, boundaryFaces

    def check_connectivity(self):
        '''checking the connectivity of the blocks of the topology'''
        if len(self.blockList) != len(self.blockConnections):
            raise Exception('Number of Blocks in blockList must be equal to number' + \
            'of Connections in blockConnections! ' + \
            '(%i!=%i)' % (len(self.blockList) , len(self.blockConnections)))
        else:
            connectionCounter = 0
            # iterate over all blocks:
            for blockIndex in range(len(self.blockList)):
                conn = self.blockConnections[blockIndex]
                # iterate over all 6 sides of the block:
                for index in conn.values():
                    # print side,index
                    # check if current side has neighbour:
                    if type(index) == type([]):
                        if index[0] > blockIndex:
                            connectionCounter += 1
                        # check if neighbour block is connected to
                        # current block as well:
                        if blockIndex not in [value[0] for
                                              value in self.blockConnections[
                                                        index[0]].values()]:
                            raise Exception('Block number %i is not ' % index[0] + \
                                'connected to block number %i!' % blockIndex)
        if connectionCounter > int(len(self.blockList) ** 0.67 *
                                   len(self.blockList) ** 0.33 * 6):
            raise Exception('Too many connections! Check connectivity again!')
        if connectionCounter < len(self.blockList) - 1:
            raise Exception('Too few connections! Check connectivity again!')
        print 'Connectivity is OK!'


def create_block(ijk=(4, 4, 4), lXYZ=(1.0, 1.0, 1.0), offSetXYZ=(0, 0, 0)):
    '''creating simple block'''
    block = np.zeros((ijk[0], ijk[1], ijk[2], 3))
    for i, x in enumerate(np.linspace(offSetXYZ[0], offSetXYZ[0] + lXYZ[0],
                                      ijk[0])):
        block[i, :, :, 0] = x
    for j, y in enumerate(np.linspace(offSetXYZ[1], offSetXYZ[1] + lXYZ[1],
                                      ijk[1])):
        block[:, j, :, 1] = y
    for k, z in enumerate(np.linspace(offSetXYZ[2], offSetXYZ[2] + lXYZ[2],
                                      ijk[2])):
        block[:, :, k, 2] = z
    return block


def create_connection():
    '''creating default connection dictionairy'''
    return {'i-':'inlet', 'i+':'outlet', 'j-':'cyclicLower',
            'j+':'cyclicUpper', 'k-':'hub', 'k+':'shroud'}

