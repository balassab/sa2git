#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2013 by Balint Balassa                                  *
#*                                                                         *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import numpy as np


class FoamMeshWriter():

    def __init__(self):
        self.case = ''
        self.topo = None
        self.boundaryType = 'wall'

        f = open('header.txt', 'r')
        self.header = f.read()
        f.close()

    def __call__(self):
        self.count()
        pointIndexesList = self.write_points()
        self.write_faces(pointIndexesList)
        self.write_boundary()
        self.write_owner()
        self.write_neighbour()

    def count(self):
        self.points, self.cells, self.faces, self.boundaryFaces = self.topo.count_points_faces_etc()

    def write_points(self):

        stringList = []

        pointIndexesList = []
        uniquePoints = 0
        uniqueArray = np.zeros((1), dtype='int')

        for blockIndex in range(len(self.topo.blockList)):
            conn = self.topo.blockConnections[blockIndex]
            block = self.topo.blockList[blockIndex]
            shape = block.shape

            iRange = range(shape[0])
            jRange = range(shape[1])
            kRange = range(shape[2])

            # create numpy array for storing point indexes used in openfoam
            # especially useful for defining the faces etc.
            pointIndexes = np.zeros(shape[:-1], dtype='int')

            # iterate over all 6 sides:
            for side, neighbour in conn.items():
                # if current side has neighbour:
                if type(neighbour) == type([]):
                    # getting neighbour index and the directions of block alignment:
                    neighbour, neighbourDir1, neighbourDir2 = neighbour
                    # check if entries are in correct order, and if not: switch
                    if neighbourDir2[0] < neighbourDir1[0]:
                        neighbourDir2, neighbourDir1 = neighbourDir1, neighbourDir2
                    # creating tuple to help checking if one direction is reversed:
                    neighbourDirs = (neighbourDir1[1], neighbourDir2[1])
                    # if neighbour has already been considered:
                    if neighbour < blockIndex:
                        #print [(value, key) for key, value in self.topo.blockConnections[neighbour].iteritems()]
                        # switching keys for values so that adjacent side of neighbour can be found:
                        reverseCon = dict((value[0], key) for key, value in self.topo.blockConnections[neighbour].iteritems())
                        neighbourSide = reverseCon[blockIndex]#[0]

                        if side == 'i-':
                            # Adapt iteration range for later point writing:
                            iRange = iRange[1:]
                            #TEST:
                            #iRange = iRange[:-1]
                            # the following ifs copy the openfoam indexes from
                            # neighbouring blocks to current block:
                            if neighbourSide == 'i-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][0, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][0, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][0, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][0, :, ::-1]
                            elif neighbourSide == 'i+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][-1, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][-1, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][-1, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][-1, :, ::-1]
                            elif neighbourSide == 'j-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, 0, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, 0, ::-1]
                            elif neighbourSide == 'j+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, -1, :]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, -1, ::-1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, -1, ::-1]
                            elif neighbourSide == 'k-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, ::-1, 0]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, ::-1, 0]
                            elif neighbourSide == 'k+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][::-1, ::-1, -1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[0, :, :] = pointIndexesList[neighbour][:, ::-1, -1]

                        elif side == 'i+':
                            #print 'inside', blockIndex
                            #print side, neighbourSide
                            #print pointIndexesList[neighbour]#[0,:,:]
                            # Adapt iteration range for later point writing:
                            iRange = iRange[:-1]
                            # the following ifs copy the openfoam indexes from
                            # neighbouring blocks to current block:
                            if neighbourSide == 'i-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][0, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][0, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][0, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][0, :, ::-1]
                            elif neighbourSide == 'i+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][-1, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][-1, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][-1, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][-1, :, ::-1]
                            elif neighbourSide == 'j-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, 0, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, 0, ::-1]
                            elif neighbourSide == 'j+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, -1, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, -1, ::-1]
                            elif neighbourSide == 'k-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, ::-1, 0]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, ::-1, 0]
                            elif neighbourSide == 'k+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][::-1, ::-1, -1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[-1, :, :] = pointIndexesList[neighbour][:, ::-1, -1]

                        elif side == 'j-':
                            # Adapt iteration range for later point writing:
                            jRange = jRange[1:]
                            #TEST:
                            #jRange = jRange[:-1]
                            # the following ifs copy the openfoam indexes from
                            # neighbouring blocks to current block:
                            if neighbourSide == 'i-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][0, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][0, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][0, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][0, :, ::-1]
                            elif neighbourSide == 'i+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][-1, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][-1, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][-1, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][-1, :, ::-1]
                            elif neighbourSide == 'j-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, 0, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, 0, ::-1]
                            elif neighbourSide == 'j+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, -1, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, -1, ::-1]
                            elif neighbourSide == 'k-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, ::-1, 0]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, ::-1, 0]
                            elif neighbourSide == 'k+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][::-1, ::-1, -1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, 0, :] = pointIndexesList[neighbour][:, ::-1, -1]

                        elif side == 'j+':
                            # Adapt iteration range for later point writing:
                            jRange = jRange[:-1]
                            # the following ifs copy the openfoam indexes from
                            # neighbouring blocks to current block:
                            if neighbourSide == 'i-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][0, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][0, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][0, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][0, :, ::-1]
                            elif neighbourSide == 'i+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][-1, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][-1, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][-1, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][-1, :, ::-1]
                            elif neighbourSide == 'j-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, 0, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, 0, ::-1]
                            elif neighbourSide == 'j+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, -1, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, -1, ::-1]
                            elif neighbourSide == 'k-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, ::-1, 0]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, ::-1, 0]
                            elif neighbourSide == 'k+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][::-1, ::-1, -1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, -1, :] = pointIndexesList[neighbour][:, ::-1, -1]

                        elif side == 'k-':
                            # Adapt iteration range for later point writing:
                            kRange = kRange[1:]
                            # TEST:
                            #kRange = kRange[:-1]
                            # the following ifs copy the openfoam indexes from
                            # neighbouring blocks to current block:
                            if neighbourSide == 'i-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][0, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][0, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][0, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][0, :, ::-1]
                            elif neighbourSide == 'i+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][-1, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][-1, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][-1, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][-1, :, ::-1]
                            elif neighbourSide == 'j-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, 0, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, 0, ::-1]
                            elif neighbourSide == 'j+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, -1, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, -1, ::-1]
                            elif neighbourSide == 'k-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, ::-1, 0]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, ::-1, 0]
                            elif neighbourSide == 'k+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][::-1, ::-1, -1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, 0] = pointIndexesList[neighbour][:, ::-1, -1]

                        elif side == 'k+':
                            # Adapt iteration range for later point writing:
                            kRange = kRange[:-1]
                            # the following ifs copy the openfoam indexes from
                            # neighbouring blocks to current block:
                            if neighbourSide == 'i-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][0, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][0, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][0, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][0, :, ::-1]
                            elif neighbourSide == 'i+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][-1, :, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][-1, ::-1, :]
                                elif 'j-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][-1, ::-1, ::-1]
                                elif 'j-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][-1, :, ::-1]
                            elif neighbourSide == 'j-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, 0, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, 0, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, 0, ::-1]
                            elif neighbourSide == 'j+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, -1, :]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, -1, ::-1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'k-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, -1, ::-1]
                            elif neighbourSide == 'k-':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, :, 0]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, ::-1, 0]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, ::-1, 0]
                            elif neighbourSide == 'k+':
                                if  '-' not in neighbourDirs:
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 != 'j-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, :, -1]
                                elif 'i-' == neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][::-1, ::-1, -1]
                                elif 'i-' != neighbourDir1 and neighbourDir2 == 'j-':
                                    pointIndexes[:, :, -1] = pointIndexesList[neighbour][:, ::-1, -1]
            # use adapted iteration range (considering already written points from neighbour blocks) for point writing:
            #print '\nsetting point indexes:'
            #print 'uniquepoints',uniquePoints
            #print iRange, jRange, kRange
            for kTmp, k in enumerate(kRange):
                for jTmp, j in enumerate(jRange):
                    for iTmp, i in enumerate(iRange):
                        #print i,j,k,
                        #iTmp, jTmp, kTmp = range(len(iRange)), range(len(jRange)), range(len(kRange))
                        #print uniquePoints + k*len(iRange)* len(jRange)+ j*len(iRange) + i
                        # set openfoam indexing (and consider previous blocks):
                        pointIndexes[i, j, k] = uniquePoints + kTmp * len(iRange) * len(jRange) + jTmp * len(iRange) + iTmp
                        # do the striong joining:
                        pointString = ''.join(['(', str(block[i, j, k, 0]), ' ', str(block[i, j, k, 1]), ' ', str(block[i, j, k, 2]), ')\t// ' + str(pointIndexes[i, j, k]) + '\n'])
                        stringList.append(pointString)
            flattened = pointIndexes.flatten()
            uniqueArray = np.unique(np.concatenate((uniqueArray, flattened)))
            #print uniqueArray
            uniquePoints = uniqueArray.size
            #print 'uniquePoints', uniquePoints
            pointIndexesList.append(pointIndexes)

        pointsHeader = self.header.replace('OBJECT', 'points')
        pointsHeader = pointsHeader.replace('CLASS', 'vectorField')
        noteString = "nPoints: %i nCells: %i nFaces: %i nInternalFaces: %i" % (uniquePoints, self.cells, self.faces, self.faces - self.boundaryFaces)
        pointsHeader = pointsHeader.replace('NOTE', noteString)
        startList = "\n%i\n(\n" % uniquePoints
        stringList.insert(0, pointsHeader)
        stringList.insert(1, startList)
        endList = ")\n\n\n// ************************************************************************* //\n\n"
        stringList.append(endList)

        string2write = ''.join(stringList)
        f = open(self.case + '/constant/polyMesh/points', 'w')
        f.write(string2write)
        f.close()
        print 'File', self.case + '/constant/polyMesh/points', 'has been written'
        #print pointIndexesList[0],'\n'
        #print pointIndexesList[1]
        return pointIndexesList

    def get_neighbourCellIndex(self, blockIndex, side, i, j, k):
        neighbourIndex = self.topo.blockConnections[blockIndex][side][0]
        alignment1, alignment2 = self.topo.blockConnections[blockIndex][side][1:3]
        neighbourShape = self.topo.blockList[neighbourIndex].shape
        # switching keys for values so that adjacent side of neighbour can be found:
        reverseCon = dict((value[0], key) for key, value in self.topo.blockConnections[neighbourIndex].iteritems())
        neighbourSide = reverseCon[blockIndex]#[0]
        #print 'neighbourSide',neighbourSide
        cellSumUpToNeighbourBlock = sum([(block2.shape[0] - 1) * (block2.shape[1] - 1) * (block2.shape[2] - 1) for block2 in self.topo.blockList[:neighbourIndex]])
        #print cellSumUpToNeighbourBlock
        #print '\n',blockIndex, i,j,k
        #print side, neighbourSide
        if side[0] == 'i':
            if neighbourSide[0] == 'i':
                if neighbourSide[1] == '+':
                    neighbourI = neighbourShape[0] - 2
                else:
                    #print 'setting neighbourI=0!'
                    neighbourI = 0
                if alignment1 == 'j+':
                    neighbourJ = j
                elif alignment1 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - j
                elif alignment1 == 'k+':
                    neighbourK = j
                elif alignment1 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - j
                if alignment2 == 'j+':
                    neighbourJ = k
                elif alignment2 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - k
                elif alignment2 == 'k+':
                    neighbourK = k
                elif alignment2 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - k
            elif neighbourSide[0] == 'j':
                if neighbourSide[1] == '+':
                    neighbourJ = neighbourShape[1] - 2
                else:
                    neighbourJ = 0
                if alignment1 == 'i+':
                    neighbourI = j
                elif alignment1 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - j
                elif alignment1 == 'k+':
                    neighbourK = j
                elif alignment1 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - j
                if alignment2 == 'i+':
                    neighbourI = k
                elif alignment2 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - k
                elif alignment2 == 'k+':
                    neighbourK = k
                elif alignment2 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - k
            elif neighbourSide[0] == 'k':
                if neighbourSide[1] == '+':
                    neighbourK = neighbourShape[2] - 2
                else:
                    neighbourK = 0
                if alignment1 == 'i+':
                    neighbourI = j
                elif alignment1 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - j
                elif alignment1 == 'j+':
                    neighbourJ = j
                elif alignment1 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - j
                if alignment2 == 'i+':
                    neighbourI = k
                elif alignment2 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - k
                elif alignment2 == 'j+':
                    neighbourJ = k
                elif alignment2 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - k

        if side[0] == 'j':
            if neighbourSide[0] == 'i':
                if neighbourSide[1] == '+':
                    neighbourI = neighbourShape[0] - 2
                else:
                    neighbourI = 0
                if alignment1 == 'j+':
                    neighbourJ = i
                elif alignment1 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - i
                elif alignment1 == 'k+':
                    neighbourK = i
                elif alignment1 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - i
                if alignment2 == 'j+':
                    neighbourJ = k
                elif alignment2 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - k
                elif alignment2 == 'k+':
                    neighbourK = k
                elif alignment2 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - k
            elif neighbourSide[0] == 'j':
                if neighbourSide[1] == '+':
                    neighbourJ = neighbourShape[1] - 2
                else:
                    neighbourJ = 0
                if alignment1 == 'i+':
                    neighbourI = i
                elif alignment1 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - i
                elif alignment1 == 'k+':
                    neighbourK = i
                elif alignment1 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - i
                if alignment2 == 'i+':
                    neighbourI = k
                elif alignment2 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - k
                elif alignment2 == 'k+':
                    neighbourK = k
                elif alignment2 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - k
            elif neighbourSide[0] == 'k':
                if neighbourSide[1] == '+':
                    neighbourK = neighbourShape[2] - 2
                else:
                    neighbourK = 0
                if alignment1 == 'i+':
                    neighbourI = i
                elif alignment1 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - i
                elif alignment1 == 'j+':
                    neighbourJ = i
                elif alignment1 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - i
                if alignment2 == 'i+':
                    neighbourI = k
                elif alignment2 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - k
                elif alignment2 == 'j+':
                    neighbourJ = k
                elif alignment2 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - k

        if side[0] == 'k':
            if neighbourSide[0] == 'i':
                if neighbourSide[1] == '+':
                    neighbourI = neighbourShape[0] - 2
                else:
                    neighbourI = 0
                if alignment1 == 'j+':
                    neighbourJ = i
                elif alignment1 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - i
                elif alignment1 == 'k+':
                    neighbourK = i
                elif alignment1 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - i
                if alignment2 == 'j+':
                    neighbourJ = j
                elif alignment2 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - j
                elif alignment2 == 'k+':
                    neighbourK = j
                elif alignment2 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - j
            elif neighbourSide[0] == 'j':
                if neighbourSide[1] == '+':
                    neighbourJ = neighbourShape[1] - 2
                else:
                    neighbourJ = 0
                if alignment1 == 'i+':
                    neighbourI = i
                elif alignment1 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - i
                elif alignment1 == 'k+':
                    neighbourK = i
                elif alignment1 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - i
                if alignment2 == 'i+':
                    neighbourI = j
                elif alignment2 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - j
                elif alignment2 == 'k+':
                    neighbourK = j
                elif alignment2 == 'k-':
                    neighbourK = neighbourShape[2] - 2 - j
            elif neighbourSide[0] == 'k':
                if neighbourSide[1] == '+':
                    neighbourK = neighbourShape[2] - 2
                else:
                    neighbourK = 0
                if alignment1 == 'i+':
                    neighbourI = i
                elif alignment1 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - i
                elif alignment1 == 'j+':
                    neighbourJ = i
                elif alignment1 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - i
                if alignment2 == 'i+':
                    neighbourI = j
                elif alignment2 == 'i-':
                    neighbourI = neighbourShape[0] - 2 - j
                elif alignment2 == 'j+':
                    neighbourJ = j
                elif alignment2 == 'j-':
                    neighbourJ = neighbourShape[1] - 2 - j

        #print neighbourI, neighbourJ, neighbourK
        neighbourCellIndex = cellSumUpToNeighbourBlock + neighbourI + neighbourJ * (neighbourShape[0] - 1) + neighbourK * (neighbourShape[0] - 1) * (neighbourShape[1] - 1)
        #print  neighbourCellIndex
        return neighbourCellIndex

    def write_faces(self, pointIndexesList):
        facesHeader = self.header.replace('OBJECT', 'faces')
        facesHeader = facesHeader.replace('CLASS', 'faceList')
        noteString = "nPoints: %i nCells: %i nFaces: %i nInternalFaces: %i" % (self.points, self.cells, self.faces, self.faces - self.boundaryFaces)
        facesHeader = facesHeader.replace('NOTE', noteString)
        startList = "\n%i\n(\n" % self.faces
        stringList = [facesHeader, startList]



        self.ownerList = []
        self.neighbourList = []

        faceCounter = 0
        cellsFromPreviousBlocks = 0

        # INTERNAL FACES:
        for blockIndex in range(len(self.topo.blockList)):

            conn = self.topo.blockConnections[blockIndex]
            block = self.topo.blockList[blockIndex]
            shape = block.shape
            pointIndexes = pointIndexesList[blockIndex]

            #owner = np.ones((shape[0]-1, shape[1]-1, shape[2]-1, 6), dtype='int') * -1
            #neighbour = np.ones((shape[0]-1, shape[1]-1, shape[2]-1, 6), dtype='int') * -1

            ni = shape[0]
            nj = shape[1]
            nk = shape[2]
            #print "shape:", shape
            #print "previousPoints:", previousPoints
            iRange = range(ni - 1)
            jRange = range(nj - 1)
            kRange = range(nk - 1)

            # iterating over cells, not points:
            for k in kRange:
                for j in jRange:
                    for i in iRange:
                        #print i,j,k
                        # i+ side:
                        if i < ni - 2: # all i-cells but last
                            p0 = pointIndexes[i + 1, j, k]
                            p1 = pointIndexes[i + 1, j + 1, k]
                            p2 = pointIndexes[i + 1, j + 1, k + 1]
                            p3 = pointIndexes[i + 1, j, k + 1]
                            facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                            stringList.append(facesString)
                            #owner[i,j,k,0] = faceCounter
                            self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                            self.neighbourList.append(cellsFromPreviousBlocks + i + 1 + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                            faceCounter += 1
                            #print facesString[:-1], 'i+'
                        else: # last i cell
                            if type(conn['i+']) == type([]): # if block has neighbour
                                if conn['i+'][0] > blockIndex: # if neighbour has not already been considered
                                    p0 = pointIndexes[i + 1, j, k]
                                    p1 = pointIndexes[i + 1, j + 1, k]
                                    p2 = pointIndexes[i + 1, j + 1, k + 1]
                                    p3 = pointIndexes[i + 1, j, k + 1]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,0] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    # TODO:
#                                    neighbourIndex = conn['i+'][0]
#                                    neighbourShape = self.topo.blockList[neighbourIndex].shape
#                                     switching keys for values so that adjacent side of neighbour can be found:
#                                    reverseCon = dict((value[0], key) for key, value in self.topo.blockConnections[neighbourIndex].iteritems())
#                                    neighbourSide = reverseCon[blockIndex][0]
#                                    cellSumUpToNeighbourBlock = sum([(block2.shape[0]-1)*(block2.shape[1]-1)*(block2.shape[2]-1) for block2 in self.topo.blockList[:neighbourIndex]])
                                    neighbourCellIndex = self.get_neighbourCellIndex(blockIndex, 'i+', i, j, k)
                                    self.neighbourList.append(neighbourCellIndex)
                                    faceCounter += 1
                                    #print facesString[:-1], 'i+max'

                        # j+ side:
                        if j < nj - 2: # all j-cells but last
                            p0 = pointIndexes[i, j + 1, k]
                            p1 = pointIndexes[i, j + 1, k + 1]
                            p2 = pointIndexes[i + 1, j + 1, k + 1]
                            p3 = pointIndexes[i + 1, j + 1, k]
                            facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                            stringList.append(facesString)
                            #owner[i,j,k,2] = faceCounter
                            self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                            self.neighbourList.append(cellsFromPreviousBlocks + i + (j + 1) * (ni - 1) + k * (ni - 1) * (nj - 1))
                            faceCounter += 1
                            #print facesString[:-1], 'j+'
                        else: # last j cell
                            if type(conn['j+']) == type([]): # if block has neighbour
                                if conn['j+'][0] > blockIndex: # if neighbour has not already been considered
                                    p0 = pointIndexes[i, j + 1, k]
                                    p1 = pointIndexes[i, j + 1, k + 1]
                                    p2 = pointIndexes[i + 1, j + 1, k + 1]
                                    p3 = pointIndexes[i + 1, j + 1, k]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,2] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    # TODO:
                                    neighbourCellIndex = self.get_neighbourCellIndex(blockIndex, 'j+', i, j, k)
                                    self.neighbourList.append(neighbourCellIndex)
                                    faceCounter += 1
                                    #print facesString[:-1], 'j+max'

                        # k+ side:
                        if k < nk - 2: # all k-cells but last
                            p0 = pointIndexes[i, j, k + 1]
                            p1 = pointIndexes[i + 1, j, k + 1]
                            p2 = pointIndexes[i + 1, j + 1, k + 1]
                            p3 = pointIndexes[i, j + 1, k + 1]
                            facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                            stringList.append(facesString)
                            #owner[i,j,k,4] = faceCounter
                            self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                            self.neighbourList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + (k + 1) * (ni - 1) * (nj - 1))
                            faceCounter += 1
                        else: # last k cell
                            if type(conn['k+']) == type([]): # if block has neighbour
                                if conn['k+'][0] > blockIndex: # if neighbour has not already been considered
                                    p0 = pointIndexes[i, j, k + 1]
                                    p1 = pointIndexes[i + 1, j, k + 1]
                                    p2 = pointIndexes[i + 1, j + 1, k + 1]
                                    p3 = pointIndexes[i, j + 1, k + 1]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,4] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    # TODO:
                                    neighbourCellIndex = self.get_neighbourCellIndex(blockIndex, 'k+', i, j, k)
                                    self.neighbourList.append(neighbourCellIndex)
                                    faceCounter += 1

                        # i- side:
                        if i == 0:
                            if type(conn['i-']) == type([]): # if block has neighbour
                                if conn['i-'][0] > blockIndex: # if neighbour has not already been considered
                                    p0 = pointIndexes[i, j, k]
                                    p1 = pointIndexes[i, j, k + 1]
                                    p2 = pointIndexes[i, j + 1, k + 1]
                                    p3 = pointIndexes[i, j + 1, k]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,1] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    # TODO:
                                    neighbourCellIndex = self.get_neighbourCellIndex(blockIndex, 'i-', i, j, k)
                                    self.neighbourList.append(neighbourCellIndex)
                                    faceCounter += 1
                                    #print facesString[:-1], 'i-'

                        # j- side:
                        if j == 0:
                            if type(conn['j-']) == type([]): # if block has neighbour
                                if conn['j-'][0] > blockIndex: # if neighbour has not already been considered
                                    p0 = pointIndexes[i, j, k]
                                    p1 = pointIndexes[i + 1, j, k]
                                    p2 = pointIndexes[i + 1, j, k + 1]
                                    p3 = pointIndexes[i, j, k + 1]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,3] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + k * (ni - 1) * (nj - 1))
                                    # TODO:
                                    neighbourCellIndex = self.get_neighbourCellIndex(blockIndex, 'j-', i, j, k)
                                    self.neighbourList.append(neighbourCellIndex)
                                    faceCounter += 1
                                    #print facesString[:-1], 'j-'

                        # k- side:
                        if k == 0:
                            if type(conn['k-']) == type([]): # if block has neighbour
                                if conn['k-'][0] > blockIndex: # if neighbour has not already been considered
                                    p0 = pointIndexes[i, j, k]
                                    p1 = pointIndexes[i, j + 1, k]
                                    p2 = pointIndexes[i + 1, j + 1, k]
                                    p3 = pointIndexes[i + 1, j, k]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,5] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1))
                                    # TODO:
                                    neighbourCellIndex = self.get_neighbourCellIndex(blockIndex, 'k-', i, j, k)
                                    self.neighbourList.append(neighbourCellIndex)
                                    faceCounter += 1
            #previousPoints += ni*nj*nk
            cellsFromPreviousBlocks += (ni - 1) * (nj - 1) * (nk - 1)

        # BOUNDARY FACES:
        self.patchesList = []
        self.patchesStartIndexes = []
        for blockIndex in range(len(self.topo.blockList)):
            conn = self.topo.blockConnections[blockIndex]
            for side, patch in conn.items():
                # if current side is boundary:
                if type(patch) == type('') and patch not in self.patchesList:
                    self.patchesList.append(patch)
        #print self.patchesList
        for patch in self.patchesList:
            #print "patch", patch
            self.patchesStartIndexes.append(faceCounter)
            cellsFromPreviousBlocks = 0
            for blockIndex in range(len(self.topo.blockList)):
                conn = self.topo.blockConnections[blockIndex]
                block = self.topo.blockList[blockIndex]
                shape = block.shape
                pointIndexes = pointIndexesList[blockIndex]
                #print pointIndexes
                ni = shape[0]
                nj = shape[1]
                nk = shape[2]
                sortedSides = conn.keys()
                sortedSides.sort()
                #print sortedSides
                for side in sortedSides:
                    patch2 = conn[side]
                    #print "patch2", patch2
                    # if current side is boundary:
                    if patch2 == patch:

                        #print 'pathc:',patch
                        #print shape
                        #print pointIndexes.shape
                        if side == 'i+':
                            for k in range(nk - 1):
                                for j in range(nj - 1):
                                    p0 = pointIndexes[ni - 1, j, k]
                                    p1 = pointIndexes[ni - 1, j + 1, k]
                                    p2 = pointIndexes[ni - 1, j + 1, k + 1]
                                    p3 = pointIndexes[ni - 1, j, k + 1]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,0] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + ni - 2 + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    faceCounter += 1
                        elif side == 'i-':
                            for k in range(nk - 1):
                                for j in range(nj - 1):
                                    p0 = pointIndexes[0, j, k]
                                    p1 = pointIndexes[0, j, k + 1]
                                    p2 = pointIndexes[0, j + 1, k + 1]
                                    p3 = pointIndexes[0, j + 1, k]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,1] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + 0 + j * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    faceCounter += 1
                        elif side == 'j+':
                            for i in range(ni - 1):
                                for k in range(nk - 1):
                                    p0 = pointIndexes[i, nj - 1, k]
                                    p1 = pointIndexes[i, nj - 1, k + 1]
                                    p2 = pointIndexes[i + 1, nj - 1, k + 1]
                                    p3 = pointIndexes[i + 1, nj - 1, k]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,2] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + (nj - 2) * (ni - 1) + k * (ni - 1) * (nj - 1))
                                    faceCounter += 1
                        elif side == 'j-':
                            for i in range(ni - 1):
                                for k in range(nk - 1):
                                    p0 = pointIndexes[i, 0, k]
                                    p1 = pointIndexes[i + 1, 0, k]
                                    p2 = pointIndexes[i + 1, 0, k + 1]
                                    p3 = pointIndexes[i, 0, k + 1]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,3] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + 0 + k * (ni - 1) * (nj - 1))
                                    faceCounter += 1
                        elif side == 'k+':
                            for i in range(ni - 1):
                                for j in range(nj - 1):
                                    #print i,j
                                    p0 = pointIndexes[i, j, nk - 1]
                                    p1 = pointIndexes[i + 1, j, nk - 1]
                                    p2 = pointIndexes[i + 1, j + 1, nk - 1]
                                    p3 = pointIndexes[i, j + 1, nk - 1]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,4] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + (nk - 2) * (ni - 1) * (nj - 1))
                                    faceCounter += 1
                        elif side == 'k-':
                            for i in range(ni - 1):
                                for j in range(nj - 1):
                                    p0 = pointIndexes[i, j, 0]
                                    p1 = pointIndexes[i, j + 1, 0]
                                    p2 = pointIndexes[i + 1, j + 1, 0]
                                    p3 = pointIndexes[i + 1, j, 0]
                                    facesString = '4(%i %i %i %i)\t// %i\n' % (p0, p1, p2, p3, faceCounter)
                                    stringList.append(facesString)
                                    #owner[i,j,k,5] = faceCounter
                                    self.ownerList.append(cellsFromPreviousBlocks + i + j * (ni - 1) + 0)
                                    faceCounter += 1
                #self.ownerList.append(owner)
                cellsFromPreviousBlocks += (ni - 1) * (nj - 1) * (nk - 1)

        self.patchesStartIndexes.append(faceCounter)
        endList = ")\n\n\n// ************************************************************************* //\n\n"
        stringList.append(endList)

        string2write = ''.join(stringList)
        f = open(self.case + '/constant/polyMesh/faces', 'w')
        f.write(string2write)
        f.close()
        print 'File', self.case + '/constant/polyMesh/faces', 'has been written'

    def write_boundary(self):
        boundaryHeader = self.header.replace('OBJECT', 'boundary')
        boundaryHeader = boundaryHeader.replace('CLASS', 'polyBoundaryMesh')
        noteString = "nPoints: %i nCells: %i nFaces: %i nInternalFaces: %i" % (self.points, self.cells, self.faces, self.faces - self.boundaryFaces)
        boundaryHeader = boundaryHeader.replace('NOTE', noteString)
        startList = "\n%i\n(\n" % len(self.patchesList)
        stringList = [boundaryHeader, startList]
        for i, patch in enumerate(self.patchesList):
            nFaces = self.patchesStartIndexes[i + 1] - self.patchesStartIndexes[i]
            startFace = self.patchesStartIndexes[i]
            patchString = '\t%s\n\t{\n\t\ttype\t\t\t%s;\n\t\tnFaces\t\t\t%i;\n\t\tstartFace\t\t%i;\n\t}\n' % (patch, self.boundaryType, nFaces, startFace)
            stringList.append(patchString)
        endList = ")\n\n\n// ************************************************************************* //\n\n"
        stringList.append(endList)

        string2write = ''.join(stringList)
        f = open(self.case + '/constant/polyMesh/boundary', 'w')
        f.write(string2write)
        f.close()
        print 'File', self.case + '/constant/polyMesh/boundary', 'has been written'

    def write_owner(self):
        ownerHeader = self.header.replace('OBJECT', 'owner')
        ownerHeader = ownerHeader.replace('CLASS', 'labelList')
        noteString = "nPoints: %i nCells: %i nFaces: %i nInternalFaces: %i" % (self.points, self.cells, self.faces, self.faces - self.boundaryFaces)
        ownerHeader = ownerHeader.replace('NOTE', noteString)
        startList = "\n%i\n(\n" % self.faces
        stringList = [ownerHeader, startList]


        for faceIndex in range(self.faces):
            ownerString = "%i\t// %i\n" % (self.ownerList[faceIndex], faceIndex)
            stringList.append(ownerString)
        endList = ")\n\n\n// ************************************************************************* //\n\n"
        stringList.append(endList)
        string2write = ''.join(stringList)
        f = open(self.case + '/constant/polyMesh/owner', 'w')
        f.write(string2write)
        f.close()
        print 'File', self.case + '/constant/polyMesh/owner', 'has been written'

    def write_neighbour(self):
        neighbourHeader = self.header.replace('OBJECT', 'neighbour')
        neighbourHeader = neighbourHeader.replace('CLASS', 'labelList')
        noteString = "nPoints: %i nCells: %i nFaces: %i nInternalFaces: %i" % (self.points, self.cells, self.faces, self.faces - self.boundaryFaces)
        neighbourHeader = neighbourHeader.replace('NOTE', noteString)
        startList = "\n%i\n(\n" % (self.faces - self.boundaryFaces)
        stringList = [neighbourHeader, startList]

        for faceIndex in range(self.faces - self.boundaryFaces):
            ownerString = "%i\t// %i\n" % (self.neighbourList[faceIndex], faceIndex)
            stringList.append(ownerString)

        endList = ")\n\n\n// ************************************************************************* //\n\n"
        stringList.append(endList)
        string2write = ''.join(stringList)
        f = open(self.case + '/constant/polyMesh/neighbour', 'w')
        f.write(string2write)
        f.close()
        print 'File', self.case + '/constant/polyMesh/neighbour', 'has been written'




