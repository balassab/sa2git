#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Module Docstring
Docstrings: http://www.python.org/dev/peps/pep-0257/
"""

#***************************************************************************
# *   Copyright (C) 2013 by Balint Balassa                                  *
# *                                                                         *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 3 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************

import numpy as np

from topo import Topo, create_connection, create_block
from foamMeshWriter import FoamMeshWriter
from transfinite import edge, discretize_edge, hexahedron

def create_grading_block(shape):
    '''Create a block with grading information.'''
    nurbsintres = 3
    discretizationX = list(np.linspace(0, 1, shape[0]))
    discretizationY = list(np.linspace(0, 1, shape[1]))
    discretizationZ = list(np.linspace(0, 1, shape[2]))
    tmp = 2 ** np.linspace(0, 4, shape[0])
    tmp -= tmp.min()
    tmp /= tmp.max()
    discretizationX = tmp
    # print discretizationY
    # ==============

    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres)
    curveU0W0 = discretize_edge(curveU0W0, [0] * shape[1])


    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres)
    curveU1W0 = discretize_edge(curveU1W0, [1] * shape[1])


    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres)
    curveV0W0 = discretize_edge(curveV0W0, discretizationX)

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres)
    curveV1W0 = discretize_edge(curveV1W0, discretizationX)

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres)
    curveU0W1 = discretize_edge(curveU0W1, [0] * shape[1])

    start = (1, 0, 1)
    end = (1, 1, 1)
    curveU1W1 = edge(start, end, nurbsintres)
    curveU1W1 = discretize_edge(curveU1W1, [1] * shape[1])

    discretizationX = 1 - discretizationX[::-1]
    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres)
    curveV0W1 = discretize_edge(curveV0W1, discretizationX)

    start = (0, 1, 1)
    end = (1, 1, 1)
    curveV1W1 = edge(start, end, nurbsintres)
    curveV1W1 = discretize_edge(curveV1W1, discretizationX)

    # ============

    start = (0, 0, 0)
    end = (0, 0, 1)
    curveU0V0 = edge(start, end, nurbsintres)
    curveU0V0 = discretize_edge(curveU0V0, [0] * shape[2])

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres)
    curveU1V0 = discretize_edge(curveU1V0, [1] * shape[2])


    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres)
    curveU0V1 = discretize_edge(curveU0V1, [0] * shape[2])

    start = (1, 1, 0)
    end = (1, 1, 1)
    curveU1V1 = edge(start, end, nurbsintres)
    curveU1V1 = discretize_edge(curveU1V1, [1] * shape[2])

    # ==============

    gradingBlock = np.zeros((shape[0], shape[1], shape[2], 3))
    print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
                                                       shape[2])
    print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                u = float(i) / (shape[0] - 1.0)
                v = float(j) / (shape[1] - 1.0)
                w = float(k) / (shape[2] - 1.0)
                interpolU = hexahedron(u, v, w,
                                       curveU0V0, curveU0V1, curveU0W0,
                                       curveU0W1, curveU1V0, curveU1V1,
                                       curveU1W0, curveU1W1, curveV0W0,
                                       curveV0W1, curveV1W0, curveV1W1, dim=4)
                # Subtracting 1 to account for adding 1 in nts.discretize_edge!
                gradingBlock[i, j, k, 0] = interpolU - 1.0
    print 'done interpolating for U!'


    discretizationX = list(np.linspace(0, 1, shape[0]))
    discretizationY = list(np.linspace(0, 1, shape[1]))
    discretizationZ = list(np.linspace(0, 1, shape[2]))
    tmp = 2 ** np.linspace(0, 3, shape[1])
    tmp -= tmp.min()
    tmp /= tmp.max()
    discretizationY = tmp
    # print discretizationY
    # ==============

    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres)
    curveU0W0 = discretize_edge(curveU0W0, discretizationY)


    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres)
    curveU1W0 = discretize_edge(curveU1W0, discretizationY)


    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres)
    curveV0W0 = discretize_edge(curveV0W0, [0] * shape[0])

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres)
    curveV1W0 = discretize_edge(curveV1W0, [1] * shape[0])

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres)
    curveU0W1 = discretize_edge(curveU0W1, discretizationY)

    start = (1, 0, 1)
    end = (1, 1, 1)
    curveU1W1 = edge(start, end, nurbsintres)
    curveU1W1 = discretize_edge(curveU1W1, discretizationY)


    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres)
    curveV0W1 = discretize_edge(curveV0W1, [0] * shape[0])

    start = (0, 1, 1)
    end = (1, 1, 1)
    curveV1W1 = edge(start, end, nurbsintres)
    curveV1W1 = discretize_edge(curveV1W1, [1] * shape[0])

    # ============

    start = (0, 0, 0)
    end = (0, 0, 1)
    curveU0V0 = edge(start, end, nurbsintres)
    curveU0V0 = discretize_edge(curveU0V0, [0] * shape[2])

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres)
    curveU1V0 = discretize_edge(curveU1V0, [0] * shape[2])


    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres)
    curveU0V1 = discretize_edge(curveU0V1, [1] * shape[2])

    start = (1, 1, 0)
    end = (1, 1, 1)
    curveU1V1 = edge(start, end, nurbsintres)
    curveU1V1 = discretize_edge(curveU1V1, [1] * shape[2])

    # ==============

    print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
                                                       shape[2])
    print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                u = float(i) / (shape[0] - 1.0)
                v = float(j) / (shape[1] - 1.0)
                w = float(k) / (shape[2] - 1.0)
                interpolV = hexahedron(u, v, w,
                                       curveU0V0, curveU0V1, curveU0W0,
                                       curveU0W1, curveU1V0, curveU1V1,
                                       curveU1W0, curveU1W1, curveV0W0,
                                       curveV0W1, curveV1W0, curveV1W1, dim=4)
                # Subtracting 1 to account for adding 1 in nts.discretize_edge!
                gradingBlock[i, j, k, 1] = interpolV - 1.0
    print 'done interpolating for V!'


    discretizationX = list(np.linspace(0, 1, shape[0]))
    discretizationY = list(np.linspace(0, 1, shape[1]))
    discretizationZ = list(np.linspace(0, 1, shape[2]))
    # ==============

    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres)
    curveU0W0 = discretize_edge(curveU0W0, [0] * shape[1])


    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres)
    curveU1W0 = discretize_edge(curveU1W0, [0] * shape[1])


    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres)
    curveV0W0 = discretize_edge(curveV0W0, [0] * shape[0])

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres)
    curveV1W0 = discretize_edge(curveV1W0, [0] * shape[0])

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres)
    curveU0W1 = discretize_edge(curveU0W1, [1] * shape[1])

    start = (1, 0, 1)
    end = (1, 1, 1)
    curveU1W1 = edge(start, end, nurbsintres)
    curveU1W1 = discretize_edge(curveU1W1, [1] * shape[1])


    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres)
    curveV0W1 = discretize_edge(curveV0W1, [1] * shape[0])

    start = (0, 1, 1)
    end = (1, 1, 1)
    curveV1W1 = edge(start, end, nurbsintres)
    curveV1W1 = discretize_edge(curveV1W1, [1] * shape[0])

    # ============

    start = (0, 0, 0)
    end = (0, 0, 1)
    curveU0V0 = edge(start, end, nurbsintres)
    curveU0V0 = discretize_edge(curveU0V0, discretizationZ)

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres)
    curveU1V0 = discretize_edge(curveU1V0, discretizationZ)


    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres)
    curveU0V1 = discretize_edge(curveU0V1, discretizationZ)

    start = (1, 1, 0)
    end = (1, 1, 1)
    curveU1V1 = edge(start, end, nurbsintres)
    curveU1V1 = discretize_edge(curveU1V1, discretizationZ)

    # ==============

    print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
                                                       shape[2])
    print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                u = float(i) / (shape[0] - 1.0)
                v = float(j) / (shape[1] - 1.0)
                w = float(k) / (shape[2] - 1.0)
                interpolW = hexahedron(u, v, w,
                                       curveU0V0, curveU0V1, curveU0W0,
                                       curveU0W1, curveU1V0, curveU1V1,
                                       curveU1W0, curveU1W1, curveV0W0,
                                       curveV0W1, curveV1W0, curveV1W1, dim=4)
                # Subtracting 1 to account for adding 1 in nts.discretize_edge!
                gradingBlock[i, j, k, 2] = interpolW - 1.0
    print 'done interpolating for W!'

    # setting all values inside interval [0,1]
    gradingBlock = gradingBlock.clip(0, 1)
    return gradingBlock

def test_hexahedral_interpolation(shape, gradingBlock=None):
    '''Test function for hexahedral interpolation'''
    factor = 0.1
    nurbsintres = 4

    corner = (1, 1, 1)
    # ==============

    start = (0, 0, 0)
    end = (0, 1, 0)
    curveU0W0 = edge(start, end, nurbsintres, parallel='y', factor=factor)

    start = (1, 0, 0)
    end = (1, 1, 0)
    curveU1W0 = edge(start, end, nurbsintres, parallel='y', factor=factor)

    start = (0, 0, 0)
    end = (1, 0, 0)
    curveV0W0 = edge(start, end, nurbsintres, parallel='x', factor=factor)

    start = (0, 1, 0)
    end = (1, 1, 0)
    curveV1W0 = edge(start, end, nurbsintres, parallel='x', factor=factor)

    # ==============

    start = (0, 0, 1)
    end = (0, 1, 1)
    curveU0W1 = edge(start, end, nurbsintres, parallel='y', factor=factor)

    start = (1, 0, 1)
    end = corner
    curveU1W1 = edge(start, end, nurbsintres, parallel='y', factor=factor)

    start = (0, 0, 1)
    end = (1, 0, 1)
    curveV0W1 = edge(start, end, nurbsintres, parallel='x', factor=factor)

    start = (0, 1, 1)
    end = corner
    curveV1W1 = edge(start, end, nurbsintres, parallel='x', factor=factor)

    # ============

    start = (0, 0, -1)
    end = (0, 0, 3)
    curveU0V0 = edge(start, end, nurbsintres, parallel='z', factor=factor)

    start = (1, 0, 0)
    end = (1, 0, 1)
    curveU1V0 = edge(start, end, nurbsintres, parallel='z', factor=factor)

    start = (0, 1, 0)
    end = (0, 1, 1)
    curveU0V1 = edge(start, end, nurbsintres, parallel='z', factor=factor)

    start = (1, 1, 0)
    end = corner
    curveU1V1 = edge(start, end, nurbsintres, parallel='z', factor=factor)

    # ==============

    block0 = np.zeros((shape[0], shape[1], shape[2], 3))
    print 'interpolating %i points for hexahedral.' % (shape[0] * shape[1] *
                                                       shape[2])
    print 'This may take a while...'
    for i in range(shape[0]):
        for j in range(shape[1]):
            for k in range(shape[2]):
                if gradingBlock == None:
                    u = float(i) / (shape[0] - 1.0)
                    v = float(j) / (shape[1] - 1.0)
                    w = float(k) / (shape[2] - 1.0)
                else:
                    u, v, w = gradingBlock[i, j, k]
                xyz = hexahedron(u, v, w, curveU0V0, curveU0V1, curveU0W0,
                                 curveU0W1, curveU1V0, curveU1V1, curveU1W0,
                                 curveU1W1, curveV0W0, curveV0W1, curveV1W0,
                                 curveV1W1, dim=3, mappingDictArg={})
                block0[i, j, k, :] = xyz
    print 'done interpolating!'

    return block0

def root_finding_function(u, normLength, curve):
    '''Function to find the root for'''
    return curve.lengthIn(0, u) - curve.length() * normLength



if __name__ == "__main__":
    t = Topo()
    mainShape = (13, 13, 14)

    #===========================================================================
    # from scipy.optimize import bisect
    # start = (1, 0, 0)
    # end = (1, 0, 1)
    # curve = edge(start, end, 3, parallel='z', factor=0.5)
    # uList = [0, 0.23, 0.51, 0.8, 1.0]
    # for u in uList:
    #    l = bisect(root_finding_function, 0, 1, args=(u, curve))
    #    print l
    #    print curve.lengthIn(0, l) - curve.lengthIn(0, u)
    #    # rint curve.lengthIn(0, u)
    #    print ''
    # block0 = np.zeros((shape[0], shape[1], shape[2], 3))
    # for i in range(shape[0]):
    #    for j in range(shape[1]):
    #        for k in range(shape[2]):
    #            u = float(i)/(shape[0]-1.0)
    #            v = float(j)/(shape[1]-1.0)
    #            w = float(k)/(shape[2]-1.0)
    #            xyz = hexahedron(u, v, w,
    #                             curveU0V0, curveU0V1, curveU0W0, curveU0W1,
    #                             curveU1V0, curveU1V1, curveU1W0, curveU1W1,
    #                             curveV0W0, curveV0W1, curveV1W0, curveV1W1)
    #            block0[i,j,k,:] = xyz
    #===========================================================================

    mainGradingBlock = create_grading_block(mainShape)
    block0 = test_hexahedral_interpolation(mainShape, mainGradingBlock)
    t.blockList = [block0]
    conn0 = create_connection()
    t.blockConnections = [conn0]

#===============================================================================
#    block0 = create_block((3, 3, 2), (1, 1, 1), (0, 0, 0))
#    block1 = create_block((3, 3, 2), (1, 1, 1), (0, 1, 0))
#    block2 = create_block((3, 3, 2), (1, 1, 1), (0, 2, 0))
#    block3 = create_block((3, 3, 2), (1, 1, 1), (1, 0, 0))
#    block5 = create_block((3, 3, 2), (1, 1, 1), (1, 1, 0))
#    for i in range(block5.shape[0]):
#        block5[i, :, :, 1] = block5[i, :, :, 1] + (float(i) /
#                                                   (block5.shape[0] - 1))
#    block4 = create_block((3, 3, 2), (1, 1, 1), (1, 1, 0))
#    for j in range(block4.shape[1]):
#        block4[:, j, :, 0] = block4[:, j, :, 0] + (float(j) /
#                                                   (block4.shape[1] - 1))
# #    #print block0
# #    #print block0.shape
#    conn0 = create_connection()
#    conn0['j+'] = [1, 'i+', 'k+']
#    conn0['i+'] = [3, 'j+', 'k+']
#    conn1 = create_connection()
#    conn1['j-'] = [0, 'i+', 'k+']
#    conn1['j+'] = [2, 'i+', 'k+']
#    conn1['i+'] = [5, 'j+', 'k+']
#    conn2 = create_connection()
#    conn2['j-'] = [1, 'i+', 'k+']
#    conn3 = create_connection()
#    conn3['i-'] = [0, 'j+', 'k+']
#    conn3['j+'] = [4, 'i+', 'k+']
#    conn5 = create_connection()
#    conn5['i-'] = [1, 'j+', 'k+']
#    conn5['j-'] = [4, 'j+', 'k+']
#    conn4 = create_connection()
#    conn4['j-'] = [3, 'i+', 'k+']
#    conn4['i-'] = [5, 'i+', 'k+']
#    t.blockList = [block0, block1, block2, block3, block4, block5]
#    t.blockConnections = [conn0, conn1, conn2, conn3, conn4, conn5]
#===============================================================================
    t.check_connectivity()

    f = FoamMeshWriter()
    f.topo = t
    f.case = '../writerTest'
    f()
    print '\nFinished!'
    # '''
