#!/usr/bin/env python
# -*- coding: utf-8 -*-

#***************************************************************************
#*   Copyright (C) 2013 by Balint Balassa                                  *
#*                                                                         *
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU General Public License as published by  *
#*   the Free Software Foundation; either version 3 of the License, or     *
#*   (at your option) any later version.                                   *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU General Public License for more details.                          *
#*                                                                         *
#*   You should have received a copy of the GNU General Public License     *
#*   along with this program; if not, write to the                         *
#*   Free Software Foundation, Inc.,                                       *
#*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
#***************************************************************************


import os

import turboMachine

from O10H import O10H
from test_bd import create_central_cyclic_surface, \
                    rotate_cylcic_surface_from_center
from grading import test_grading, ht_grading

import time
now = time.time()



workdir = os.path.abspath('/home/belenon/Dokumente/SA1/cax')
#workdir = os.path.abspath('/home/belenon/Dokumente/SA1/cu_Over_r')
turbo = turboMachine.turboMachine(workdir)
turbo.listRows[0].useNurbsXml = True
turbo.listRows[1].useNurbsXml = True
turbo.computeTurboMachine()
row = turbo.listRows[0]



o10h = O10H()
o10h.inletBlockNormLength = 0#.1
o10h.outletBlockNormLength = 0#.05
o10h.spans = [0, 1]
#o10h.relaxLE = [0, 0, 0]
o10h.relaxTE = [-0.1] * 5
o10h.cellsMer0 = 9
o10h.cellsMer1 = 10
o10h.cellsMer2 = 9
o10h.cellsMer3 = 6
o10h.cellsCirc0 = 6
o10h.cellsCirc1 = 7
o10h.cellsCirc2 = 5
o10h.cellsRad = [15] * (len(o10h.spans) - 1)
o10h.cellsOBlock = 5

# set all the o10h parameters before calling any methods!!!
o10h.get_nurbs_from_bd(row)
o10h.cyclicCenterSurfaceNurbs = create_central_cyclic_surface(\
                                    o10h.camberSurfaceNurbs,
                                    o10h.channelSurfaceNurbs,
                                    o10h.interpolationSpans, vRes=20,
                                    relaxLE=o10h.relaxLE,
                                    relaxTE=o10h.relaxTE,
                                    inletBlockNormLength=o10h.inletBlockNormLength,
                                    outletBlockNormLength=o10h.outletBlockNormLength,
                                    uValuesInlet=o10h.inletUvalues,
                                    uValuesOutlet=o10h.outletUvalues)
o10h.cyclicLowerSurfaceNurbs , o10h.cyclicUpperSurfaceNurbs = \
        rotate_cylcic_surface_from_center(o10h.cyclicCenterSurfaceNurbs,
                                          row.numberBlades)



o10h.create_layers()

# ================== Layer 0 ====================
hubLayer = o10h.layers[0]
hubLayer.read_curves_from_surfaces(o10h.bladeSurfaceNurbs,
                                         o10h.camberSurfaceNurbs,
                                         o10h.channelSurfaceNurbs,
                                         o10h.cyclicCenterSurfaceNurbs,
                                         o10h.cyclicLowerSurfaceNurbs ,
                                         o10h.cyclicUpperSurfaceNurbs)
hubLayer.inletDivision = (0.32, 0.75)
hubLayer.outletDivision = (0.36, 0.55, 0.7)
hubLayer.cyclicLowerDivision = (0.25, 0.55, 0.85)
hubLayer.cyclicUpperDivision = (0.25, 0.55, 0.85)
hubLayer.starLowerLE = (0.72, 0.23)
hubLayer.starUpperLE = (0.42, 0.31)
hubLayer.starLowerTE = (0.67, 0.86)#(0.65, 0.8408)
hubLayer.starUpperTE = (0.41, 0.86)#(0.39, 0.79)
hubLayer.OTE = (0.53, 0.91)
hubLayer.oBlockLowerMid = (0.74, 0.5)
hubLayer.oBlockUpperMid = (0.37, 0.61)
hubLayer.profileDivision = [0.01, None, None, None, None, 0.98]

hubLayer.tan_5_8_5 = (-5, -2)
hubLayer.tan_5_8_8 = (-2, 2)
hubLayer.tan_5_11_5 = (0, 5)
hubLayer.tan_8_14_8 = (-2, 2)
hubLayer.tan_11_17_17 = (-1, 3)
hubLayer.tan_14_22_22 = (0, 2)
hubLayer.tan_17_20_17 = (-1, 2)
hubLayer.tan_20_22_22 = (0, -3)

hubLayer.gradingBlade_4 = ht_grading(o10h.cellsMer1, 1.0, reverse=False)
hubLayer.gradingBlade_5 = ht_grading(o10h.cellsCirc1, 1.2, reverse=False)
hubLayer.gradingBlade_9 = ht_grading(o10h.cellsMer2, 0.2, reverse=True)
hubLayer.gradingBlade_10 = ht_grading(o10h.cellsCirc1/2, 1.2, reverse=True)
hubLayer.gradingBlade_11 = ht_grading(o10h.cellsCirc1/2, 1.2, reverse=False)
hubLayer.gradingBlade_12 = ht_grading(o10h.cellsMer2, 0.6, reverse=True)

hubLayer.grading_5_6 = ht_grading(o10h.cellsOBlock, 2.0, reverse=True)
hubLayer.grading_7_8 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)
hubLayer.grading_11_12 = ht_grading(o10h.cellsOBlock, 2.0, reverse=True)
hubLayer.grading_13_14 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)
hubLayer.grading_17_18 = ht_grading(o10h.cellsOBlock, 2.0, reverse=True)
hubLayer.grading_19_20 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)
hubLayer.grading_21_22 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)

hubLayer()



# ================== Layer 1 ====================
shroudLayer = o10h.layers[1]
shroudLayer.read_curves_from_surfaces(o10h.bladeSurfaceNurbs,
                                         o10h.camberSurfaceNurbs,
                                         o10h.channelSurfaceNurbs,
                                         o10h.cyclicCenterSurfaceNurbs,
                                         o10h.cyclicLowerSurfaceNurbs ,
                                         o10h.cyclicUpperSurfaceNurbs)
shroudLayer.inletDivision = (0.32, 0.75)
shroudLayer.outletDivision = (0.36, 0.5, 0.7)
shroudLayer.cyclicLowerDivision = (0.25, 0.55, 0.85)
shroudLayer.cyclicUpperDivision = (0.25, 0.55, 0.85)
shroudLayer.starLowerLE = (0.67, 0.28)
shroudLayer.starUpperLE = (0.4, 0.32)
shroudLayer.starLowerTE = (0.6, 0.81)#(0.65, 0.8408)
shroudLayer.starUpperTE = (0.36, 0.79)#(0.39, 0.79)
shroudLayer.OTE = (0.48, 0.85)
shroudLayer.oBlockLowerMid = (0.66, 0.49)
shroudLayer.oBlockUpperMid = (0.37, 0.6)
shroudLayer.profileDivision = [0.01, None, None, None, None, 0.98]

shroudLayer.tan_5_8_5 = (-5, -2)
shroudLayer.tan_5_8_8 = (-2, 2)
shroudLayer.tan_5_11_5 = (1, 5)
shroudLayer.tan_8_14_8 = (-2, 2)
shroudLayer.tan_11_17_17 = (-2, 2)
shroudLayer.tan_14_22_22 = (-1, 3)
shroudLayer.tan_17_20_17 = (-2, 2)
shroudLayer.tan_20_22_22 = (0, -3)
shroudLayer.tan_22_27_22 = (-1, 2)

shroudLayer.gradingBlade_4 = ht_grading(o10h.cellsMer1, 1.0, reverse=False)
shroudLayer.gradingBlade_5 = ht_grading(o10h.cellsCirc1, 1.2, reverse=False)
shroudLayer.gradingBlade_9 = ht_grading(o10h.cellsMer2, 0.2, reverse=True)
shroudLayer.gradingBlade_10 = ht_grading(o10h.cellsCirc1/2, 1.2, reverse=True)
shroudLayer.gradingBlade_11 = ht_grading(o10h.cellsCirc1/2, 1.2, reverse=False)
shroudLayer.gradingBlade_12 = ht_grading(o10h.cellsMer2, 0.6, reverse=True)

shroudLayer.grading_5_6 = ht_grading(o10h.cellsOBlock, 2.0, reverse=True)
shroudLayer.grading_7_8 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)
shroudLayer.grading_11_12 = ht_grading(o10h.cellsOBlock, 2.0, reverse=True)
shroudLayer.grading_13_14 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)
shroudLayer.grading_17_18 = ht_grading(o10h.cellsOBlock, 2.0, reverse=True)
shroudLayer.grading_19_20 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)
shroudLayer.grading_21_22 = ht_grading(o10h.cellsOBlock, 2.0, reverse=False)

shroudLayer()



o10h(cgns=False)
#o10h(cgns=True)



#    upperCyclicCurve = NurbsCurve.NurbsCurved()
#    o10h.cyclicUpperSurfaceNurbs.isoCurveU(1, upperCyclicCurve)
#    #hub = create_s1_surface(upperCyclicCurve, 28)
#    surface_points_2_vtk(o10h.bladeSurfaceNurbs,
#                         path='bladeCax.vts')
#    control_points_2_vtk(o10h.cyclicCenterSurfaceNurbs,
#                         path='cyclicCenterCtrl.vts')
#curve_points_2_vtk(upperCyclicCurve, path='upperCheck.vtp')


later = time.time()
difference = later - now

print 'all done after %f seconds!' % difference


